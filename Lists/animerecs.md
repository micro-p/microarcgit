---
title: Anime Recommendations
geometry: margin=1cm
output: pdf_document
---


* princess jellyfish
* nichijou
* lucky star
* hyouka
* wolf children ...movie
* dimension w
* land of the lustrous
* girl's last tour
* scum's wish

* kaijuu no kodomo ...movie
* kaguya hime ....movie
* kono sekai no katasumi ni ....movie
* maquia ....movie
* megalo box
* ping pong
* fruits basket ... the new one....manga
* Orange *manga **...** theanimeman **...** bad*
* ao hara ride
* ore monogatari
* Ima, Soko ni Iru Boku
* Re:CREATORS

..................................................

## Romance:

* Golden time
* Toradora
* Suzuka
* Kuzu no honkai
* Tsuki ga keiri
* Oregairu
* Relife
* Oreshura
* Chunibyuu
* Boku wa tomodachi
* Tamako love story
* Nisekoi
* Baka to test
* Ano natsu de matteru
* Yamada kun to 7 nen no majou
* Bokura wa minna kawaisou
* Mayo chiki
* Kimi no iru machi
* Akagame no shirayuki hime

## Drama:

* Hyouka
* *Clannad*
* Nana
* Sakurasou no pet na kanojo
* shuffle
* kanon
* Ano hana
* little busters
* A tale of memories
* Amnesia
* Plastic memories

## Action:

* Medaka box
* Rokka no yuusha
* Mahouka kouko no rettousei
* Tokyo ravens
* To aru no magical index
* Hellsing Ultimate
* Asterisk series
* Shengeki no bahamut Gensis
* Rakudai kish no cavarly
* Accel world(gaming)

## Sports:

* Kuroko no basket series
* Days
* Prince of stride
* Giant killing
* Free
* Mystery:
* Magic kaito

## Comedy:

* Kono Bijutsubu ni wa mondai ga aru
* GTO
* Gabriel dropout
* Himouto umaru chan
* Danshi Koukoousei Nichijou

## Slice of life:

* Natsume series

## Music:

* Nodame cantibale
* kids on the slope
* Haruchika
* Hebike euphonium
