#!/bin/bash

if [[ $(playerctl position) != 0.000000 ]]; then
    sed -e 's/.-.*//' -e 's/\ ~\ /\ /g' -e 's/^/\ \|\ /g' -e 's/^\(.\{4\}\)./\1/' -e 's/^\(.\{0\}\)./\1/' /home/micro-p/.cache/player
else
    echo ""
fi
