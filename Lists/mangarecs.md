---
title: Manga Recommendations
geometry: margin=1cm
fontfamily: FiraCode Nerd Font
output: pdf_document
---


* **liar game**
* **medoka box**
* **denjin N**
* **Coppelion**
* **candy & Cigarettes**
* **blood lad**
* **3d kanojo: Real Girl**
* **uzumaki**
* **tesogare Otame x amnesia**
* **shin kidou senki gundam wing**
* **jinrou game: crazy fox**
* **kuzu no honkai**
* **servamp**
* **Mondaiji-tachi ga Isekai kara Kuru Sou Desu yo?**
* **Hai to Gensou no Grimgar**
* **Outbreak Company: Moeru Shinryakusha**
* **Zero no Tsukaima**
* **garie**
* **gokukoku no brynhildr**
* **usokui**
* **my home hero**
* **machida-kun no sekai**
* **nekota no koto ga ki ni natte shikata ga nai** _>>>_ _shojo light_
* **do you like drinking alcohol with beautiful women**
* **jimi na kensei wa sore demo saikyou desu**
* **my status as an assassin obviously exceeds the brave's**
* **12 beast**
* **knights and magic**
* **nakutemo yokute taemanaku hikaru** _shojo_
* **orc ga okashite kurenai!**
* **maou gaukin no futekigousha**
* **kakushigoto**
* **scum's wish**
* **dog nigga**
* **chijyou 100kai**

