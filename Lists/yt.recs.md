---
title: Youtube Recommendations
geometry: margin=1cm
output: pdf_document
---


001. Ahoy - video game documentaries
002. Wired - "Movie Accent Expert Breaks Down 32 Actors' Accents"
003. Smallant - game challenges
004. Gamechamp 3000 - game challenges
005. Summoning Salt - speedrunning content
006. Frederik Knudsen - documentaries
007. The Right Opinion - documentaries
008. EmpLemon - documentaries
009. 4 hours Smash documentary
010. Atrocity Guide
011. j aubrey
012. Toad McKinley - documentaries
013. Leon Lush - commentaries
014. Drew Gooden
015. Anxiety War
016. Soft White Underbelly - interviews
017. TheReportOfTheWeek
018. buffcorrell
019. Project Farm - reviews
020. Lockpicking Lawyer - reviews
021. Daily Dose of Internet
022. Imaqtpie - LoL content
023. Gbay99 - LoL content
024. Exil - LoL content
025. Cinemassacre (Angry Videogame Nerd) (also Joey's inspiration)
026. ralphthemoviemaker - comedy
027. theneedledrop
028. SpookyRice - movie reviews
029. RedRiotRoss - gaming content
030. Core-A Gaming - fighting game content
031. Nitro Rad - game video essays
032. RelaxAlax
033. MudanTV (editor for Trash Taste and TheAn1meMan)
034. Caddicarus
035. Marshall McGee - sound design content in different mediums
036. MrCreepyPasta - horror content
037. ReignBot - horror content (Mr Anime saga)
038. Chills - horror content etc
039. Barely Sociable - horror content
040. Vsauce - educational content
041. Michael Reeves - engineering content
042. William Osman - engineering content
043. Mark Rober - engineering content
044. Stuff Made Here - engineering content
045. SUSHI RAMEN (Riku) - engineering content
046. CGP Grey - educational content
047. Tom Scott - educational content
048. Kurzgesagt - educational content
049. Veritasium - educational content
050. RealLifeLore
051. Smarter Everyday - educational content
052. Mike Boyd
053. Boyinaband
054. Sam O'Nella Academy
055. TierZoo
056. Jay Foreman - london content
057. Great Big Story - mini documentary
058. Captain Disillusion
059. Corridor
060. Johnny Harris - travel content
061. Philip DeFranco (Gigguk's inspiration)
062. old Smosh (Joey's inspiration)
063. Game Grumps (Joey's inspiration)
064. nigahiga (Joey's inspiration)
065. Kassem G (Connor's inspiration)
066. Rhystic Studies - MTG content
067. Chris Ramsay - Magic content
068. BriTANicK - skits
069. Quackity - discord humour
070. The Hoax Hotel - prank calls
071. Kitboga - prank calls
072. Primitive building channels (there are lots of them)
073. ProZD
074. kiwami japan - knife content
075. VirtuallyVain - prank calls
076. Sorrow TV - reading reddit threads/voice actor content
077. Black Gryph0n - singing/voice impressions
078. Nyanners - vtuber/shitposting
079. CalebCity
080. Jenny Nicholson
081. CallMeCarson
082. Super Eyepatch Wolf - anime content
083. Beyond Ghibli - anime content
084. The Cartoon Cipher - anime content
085. Shaybs - anime content
086. Scamboli Reviews - anime content
087. KrisPNatz
088. arcade craniacs
