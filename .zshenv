export ZSH="/home/micro-p/.oh-my-zsh"
export EDITOR=nvim
export EDITOR=nvim visudo
export VISUAL=nvim
export SXHKD_SHELL=/bin/zsh
export SHELL=/bin/zsh
export MANPAGER='nvim +Man!'
export HISTCONTROL=ignoreboth:erasedups
export PATH=$PATH:/home/micro-p/.bin:/home/micro-p/.local/bin:/home/micro-p/.local/share:~/.npm.g:/usr/lib:~/.gem/ruby/2.7.0/bin:/home/micro-p/.local/share/cargo
export QT_QPA_PLATFORMTHEME="qt5ct"
export WM="bspwm"
export OPENCV_LOG_LEVEL=ERROR
export LESSHISTFILE=-
export GEM_PATH="$XDG_DATA_HOME/ruby/gems"
export GEM_SPEC_CACHE="$XDG_DATA_HOME/ruby/specs"
export GEM_HOME="$XDG_DATA_HOME/ruby/gems"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export GOPATH="$XDG_DATA_HOME/go"
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
export ELINKS_CONFDIR="$XDG_CONFIG_HOME/elinks"
export PYLINTHOME="$XDG_CACHE_HOME/pylint"
export ZSH_COMPDUMP="$XDG_CACHE_HOME/zsh/zcompdump"
export _JAVA_AWT_WM_NONREPARENTING=1
export ANDROID_HOME="$XDG_DATA_HOME"/android
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export DVDCSS_CACHE="$XDG_DATA_HOME"/dvdcss

