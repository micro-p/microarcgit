#! /bin/bash
picom -b &
feh --bg-fill --no-fehbg /wall.png &
kdeconnect-indicator &
udiskie -t &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
flameshot &
