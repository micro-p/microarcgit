#!/bin/zsh
if upower -e | rg headset > /dev/null; then
NAME=$(upower -e | rg headset)
STATUS=$(upower -i $NAME | rg percentage)
echo $STATUS >> /home/micro-p/.cache/budspercentage
bpower=$(tail -n 1 /home/micro-p/.cache/budspercentage | sed -e 's/^.*percentage:\ *//g' -e 's/$//g' -e 's/\%//g')
if [[ $bpower > 70 ]]; then
    echo "󰠐 $bpower"
elif [[ $bpower > 40 ]]; then
    echo "󰠍 $bpower"
else
    echo "󰠋 $bpower"
fi
fi
