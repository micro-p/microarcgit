//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	/* {"", "cat /tmp/recordingicon 2>/dev/null",	0,	9}, */
	/* {"",	"music",	0,	11},*/
	/* {"",	"pacpackages",	0,	8}, */
	/* {"",	"news",		0,	6}, */
	/* {"",	"crypto",	0,	13}, */
	/* {"",	"price bat \"Basic Attention Token\" 🦁",	0,	20}, */
	/* {"",	"price btc Bitcoin 💰",				0,	21}, */
	/* {"",	"price lbc \"LBRY Token\" 📚",			0,	22}, */
	/* {"",	"torrent",	20,	7}, */
	/* {"",	"memory",	10,	14}, */
	/* {"",	"cpu",		10,	18}, */
	/* {"",	"moonphase",	18000,	17}, */
	/* {"",	"weather",	18000,	5}, */
	/* {"",	"mailbox",	180,	12}, */
	/* {"",	"nettraf",	1,	16}, */
	{"",	"/home/micro-p/.config/polybar/music",	10,	10},
	{"",	"/home/micro-p/.local/bin/statusbar/volume",	0,	10},
	{"",	"/home/micro-p/.config/polybar/newnet",	1,	10},
	{"",	"/home/micro-p/.local/bin/statusbar/memory",	10,	14},
    {"",	"/home/micro-p/.local/bin/statusbar/cpuusage",		8,	18},
    {"",	"/home/micro-p/.local/bin/statusbar/cpu",		10,	18},
	{"",	"/home/micro-p/.local/bin/statusbar/battery",	5,	3},
	{"",	"/home/micro-p/.local/bin/statusbar/clock",	1,	1},
	/* {"",	"/home/micro-p/.local/bin/statusbar/internet",	5,	4}, */
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char *delim = " | ";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
