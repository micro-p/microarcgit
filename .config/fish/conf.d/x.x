Time	Sum	Command
1719	16521	> builtin source /usr/share/fish/config.fish
136	136	-> set -g IFS \n\ \t
20	20	-> set -qg __fish_added_user_paths
17	17	-> set -g __fish_added_user_paths
6	18	-> if not set -q __fish_initialized
    set -U __fish_initialized 0
    if set -q __fish_init_2_39_8
        set __fish_initialized 2398
    else if set -q __fish_init_2_3_0
        set __fish_initialized 2300
    end
...
12	12	--> not set -q __fish_initialized
21	21	-> function __fish_default_command_not_found_handler
    printf "fish: Unknown command: %s\n" (string escape -- $argv[1]) >&2
...
13	359	-> if status --is-interactive
    # Enable truecolor/24-bit support for select terminals
    # Ignore Screen and emacs' ansi-term as they swallow the sequences, rendering the text white.
    if not set -q STY
        and not string match -q -- 'eterm*' $TERM
        and begin
            set -q KONSOLE_PROFILE_NAME # KDE's konsole
            or string match -q -- "*:*" $ITERM_SESSION_ID # Supporting versions of iTerm2 will include a colon here
            or string match -q -- "st-*" $TERM # suckless' st
            or test -n "$VTE_VERSION" -a "$VTE_VERSION" -ge 3600 # Should be all gtk3-vte-based terms after version 3.6.0.0
            or test "$COLORTERM" = truecolor -o "$COLORTERM" = 24bit # slang expects this
        end
        # Only set it if it isn't to allow override by setting to 0
        set -q fish_term24bit
        or set -g fish_term24bit 1
    end
else
    # Hook up the default as the principal command_not_found handler
    # in case we are not interactive
    function __fish_command_not_found_handler --on-event fish_command_not_found
        __fish_default_command_not_found_handler $argv
    end
...
20	20	--> status --is-interactive
34	326	--> if not set -q STY
        and not string match -q -- 'eterm*' $TERM
        and begin
            set -q KONSOLE_PROFILE_NAME # KDE's konsole
            or string match -q -- "*:*" $ITERM_SESSION_ID # Supporting versions of iTerm2 will include a colon here
            or string match -q -- "st-*" $TERM # suckless' st
            or test -n "$VTE_VERSION" -a "$VTE_VERSION" -ge 3600 # Should be all gtk3-vte-based terms after version 3.6.0.0
            or test "$COLORTERM" = truecolor -o "$COLORTERM" = 24bit # slang expects this
        end
        # Only set it if it isn't to allow override by setting to 0
        set -q fish_term24bit
        or set -g fish_term24bit 1
    ...
12	12	---> not set -q STY
55	55	---> not string match -q -- 'eterm*' $TERM
24	156	---> begin
            set -q KONSOLE_PROFILE_NAME # KDE's konsole
            or string match -q -- "*:*" $ITERM_SESSION_ID # Supporting versions of iTerm2 will include a colon here
            or string match -q -- "st-*" $TERM # suckless' st
            or test -n "$VTE_VERSION" -a "$VTE_VERSION" -ge 3600 # Should be all gtk3-vte-based terms after version 3.6.0.0
            or test "$COLORTERM" = truecolor -o "$COLORTERM" = 24bit # slang expects this
        ...
14	14	----> set -q KONSOLE_PROFILE_NAME
21	21	----> string match -q -- "*:*" $ITERM_SESSION_ID
17	17	----> string match -q -- "st-*" $TERM
51	51	----> test -n "$VTE_VERSION" -a "$VTE_VERSION" -ge 3600
29	29	----> test "$COLORTERM" = truecolor -o "$COLORTERM" = 24bit
26	26	---> set -q fish_term24bit
43	43	---> set -g fish_term24bit 1
37	37	-> set -l __extra_completionsdir
25	25	-> set -l __extra_functionsdir
22	22	-> set -l __extra_confdir
16	258	-> if test -f $__fish_data_dir/__fish_build_paths.fish
    source $__fish_data_dir/__fish_build_paths.fish
...
45	45	--> test -f $__fish_data_dir/__fish_build_paths.fish
136	197	--> source $__fish_data_dir/__fish_build_paths.fish
27	27	---> set __extra_completionsdir /usr/local/share/fish/vendor_completions.d
18	18	---> set __extra_functionsdir /usr/local/share/fish/vendor_functions.d
16	16	---> set __extra_confdir /usr/local/share/fish/vendor_conf.d
16	16	-> set -l xdg_data_dirs
12	45	-> if set -q XDG_DATA_DIRS
    set --path xdg_data_dirs $XDG_DATA_DIRS
    set xdg_data_dirs (string replace -r '([^/])/$' '$1' -- $xdg_data_dirs)/fish
else
    set xdg_data_dirs $__fish_data_dir
...
11	11	--> set -q XDG_DATA_DIRS
22	22	--> set xdg_data_dirs $__fish_data_dir
23	23	-> set -l vendor_completionsdirs $xdg_data_dirs/vendor_completions.d
22	22	-> set -l vendor_functionsdirs $xdg_data_dirs/vendor_functions.d
20	20	-> set -l vendor_confdirs $xdg_data_dirs/vendor_conf.d
11	62	-> if not contains -- $__extra_completionsdir $vendor_completionsdirs
    set -a vendor_completionsdirs $__extra_completionsdir
...
27	27	--> not contains -- $__extra_completionsdir $vendor_completionsdirs
24	24	--> set -a vendor_completionsdirs $__extra_completionsdir
10	56	-> if not contains -- $__extra_functionsdir $vendor_functionsdirs
    set -a vendor_functionsdirs $__extra_functionsdir
...
24	24	--> not contains -- $__extra_functionsdir $vendor_functionsdirs
22	22	--> set -a vendor_functionsdirs $__extra_functionsdir
9	54	-> if not contains -- $__extra_confdir $vendor_confdirs
    set -a vendor_confdirs $__extra_confdir
...
23	23	--> not contains -- $__extra_confdir $vendor_confdirs
22	22	--> set -a vendor_confdirs $__extra_confdir
9	72	-> if not set -q fish_function_path
    set fish_function_path $__fish_config_dir/functions $__fish_sysconf_dir/functions $vendor_functionsdirs $__fish_data_dir/functions
else if not contains -- $__fish_data_dir/functions $fish_function_path
    set -a fish_function_path $__fish_data_dir/functions
...
11	11	--> not set -q fish_function_path
52	52	--> set fish_function_path $__fish_config_dir/functions $__fish_sysconf_dir/functions $vendor_functionsdirs $__fish_data_dir/functions
14	72	-> if not set -q fish_complete_path
    set fish_complete_path $__fish_config_dir/completions $__fish_sysconf_dir/completions $vendor_completionsdirs $__fish_data_dir/completions $__fish_user_data_dir/generated_completions
else if not contains -- $__fish_data_dir/completions $fish_complete_path
    set -a fish_complete_path $__fish_data_dir/completions
...
12	12	--> not set -q fish_complete_path
46	46	--> set fish_complete_path $__fish_config_dir/completions $__fish_sysconf_dir/completions $vendor_completionsdirs $__fish_data_dir/completions $__fish_user_data_dir/generated_completions
13	13	-> function : -d "no-op function"
    # for compatibility with sh, bash, and others.
    # Often used to insert a comment into a chain of commands without having
    # it eat up the remainder of the line, handy in Makefiles.
    # This command always succeeds
    true
...
8	76	-> if begin; not set -q FISH_UNIT_TESTS_RUNNING; and test -d /usr/xpg4/bin; end
    not contains -- /usr/xpg4/bin $PATH
    and set PATH /usr/xpg4/bin $PATH
...
16	68	--> begin; not set -q FISH_UNIT_TESTS_RUNNING; and test -d /usr/xpg4/bin; ...
16	16	---> not set -q FISH_UNIT_TESTS_RUNNING
36	36	---> test -d /usr/xpg4/bin
48	48	-> function __fish_reconstruct_path -d "Update PATH when fish_user_paths changes" --on-variable fish_user_paths
    set -l local_path $PATH

    for x in $__fish_added_user_paths
        set -l idx (contains --index -- $x $local_path)
        and set -e local_path[$idx]
    end

    set -g __fish_added_user_paths
    if set -q fish_user_paths
        # Explicitly split on ":" because $fish_user_paths might not be a path variable,
        # but $PATH definitely is.
        for x in (string split ":" -- $fish_user_paths[-1..1])
            if set -l idx (contains --index -- $x $local_path)
                set -e local_path[$idx]
            else
                set -ga __fish_added_user_paths $x
            end
            set -p local_path $x
        end
    end

    set -xg PATH $local_path
...
50	50	-> function fish_sigtrap_handler --on-signal TRAP --no-scope-shadowing --description "Signal handler for the TRAP signal. Launches a debug prompt."
    breakpoint
...
13	13	-> function __fish_on_interactive --on-event fish_prompt
    __fish_config_interactive
    functions -e __fish_on_interactive
...
324	1315	-> __fish_set_locale
724	737	--> source /usr/share/fish/functions/__fish_set_locale.fish
13	13	---> function __fish_set_locale
    set -l LOCALE_VARS
    set -a LOCALE_VARS LANG LANGUAGE LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE
    set -a LOCALE_VARS LC_MONETARY LC_MESSAGES LC_PAPER LC_NAME LC_ADDRESS
    set -a LOCALE_VARS LC_TELEPHONE LC_MEASUREMENT LC_IDENTIFICATION

    # We check LC_ALL to figure out if we have a locale but we don't set it later. That is because
    # locale.conf doesn't allow it so we should not set it.
    for locale_var in $LOCALE_VARS LC_ALL
        if set -q $locale_var
            return 0
        end
    end

    # Try to extract the locale from the kernel boot commandline. The splitting here is a bit weird,
    # but we operate under the assumption that the locale can't include whitespace. Other whitespace
    # shouldn't concern us, but a quoted "locale.LANG=SOMETHING" as a value to something else might.
    # Here the last definition of a variable takes precedence.
    if test -r /proc/cmdline
        for var in (string match -ra 'locale.[^=]+=\S+' < /proc/cmdline)
            set -l kv (string replace 'locale.' '' -- $var | string split '=')
            # Only set locale variables, not other stuff contained in these files - this also
            # automatically ignores comments.
            if contains -- $kv[1] $LOCALE_VARS
                and set -q kv[2]
                set -gx $kv[1] (string trim -c '\'"' -- $kv[2])
            end
        end
    end

    # Now read the config files we know are used by various OS distros.
    #
    # /etc/sysconfig/i18n is for old Red Hat derivatives (and possibly of no use anymore).
    #
    # /etc/env.d/02locale is from OpenRC.
    #
    # The rest are systemd inventions but also used elsewhere (e.g. Void Linux). systemd's
    # documentation is a bit unclear on this. We merge all the config files (and the commandline),
    # which seems to be what systemd itself does. (I.e. the value for a variable will be taken from
    # the highest-precedence source) We read the systemd files first since they are a newer
    # invention and therefore the rest are likely to be accumulated cruft.
    #
    # NOTE: Slackware puts the locale in /etc/profile.d/lang.sh, which we can't use because it's a
    # full POSIX-shell script.
    set -l user_cfg_dir (set -q XDG_CONFIG_HOME; and echo $XDG_CONFIG_HOME; or echo ~/.config)
    for f in $user_cfg_dir/locale.conf /etc/locale.conf /etc/env.d/02locale /etc/sysconfig/i18n
        if test -r $f
            while read -l kv
                set kv (string split '=' -- $kv)
                if contains -- $kv[1] $LOCALE_VARS
                    and set -q kv[2]
                    # Do not set already set variables again - this makes the merging happen.
                    if not set -q $kv[1]
                        set -gx $kv[1] (string trim -c '\'"' -- $kv[2])
                    end
                end
            end <$f
        end
    end

    # If we really cannot get anything, at least set character encoding to UTF-8.
    for locale_var in $LOCALE_VARS LC_ALL
        if set -q $locale_var
            return 0
        end
    end
    set -gx LC_CTYPE en_US.UTF-8
...
56	56	--> set -l LOCALE_VARS
43	43	--> set -a LOCALE_VARS LANG LANGUAGE LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE
25	25	--> set -a LOCALE_VARS LC_MONETARY LC_MESSAGES LC_PAPER LC_NAME LC_ADDRESS
18	18	--> set -a LOCALE_VARS LC_TELEPHONE LC_MEASUREMENT LC_IDENTIFICATION
61	112	--> for locale_var in $LOCALE_VARS LC_ALL
        if set -q $locale_var
            return 0
        end
    ...
14	51	---> if set -q $locale_var
            return 0
        ...
23	23	----> set -q $locale_var
14	14	----> return 0
39	39	-> function . -d 'Evaluate a file (deprecated, use "source")' --no-scope-shadowing --wraps source
    if [ (count $argv) -eq 0 ] && isatty 0
        echo "source: using source via '.' is deprecated, and stdin doesn't work."\n"Did you mean 'source' or './'?" >&2
        return 1
    else
        source $argv
    end
...
12	68	-> if test $__fish_initialized -lt 2300
    if set -q fish_user_abbreviations
        set -l fab
        for abbr in $fish_user_abbreviations
            set -a fab (string replace -r '^([^ =]+)=(.*)$' '$1 $2' -- $abbr)
        end
        set fish_user_abbreviations $fab
    end
...
56	56	--> test $__fish_initialized -lt 2300
7	28	-> if status --is-login
    if command -sq /usr/libexec/path_helper
        # Adapt construct_path from the macOS /usr/libexec/path_helper
        # executable for fish; see
        # https://opensource.apple.com/source/shell_cmds/shell_cmds-203/path_helper/path_helper.c.auto.html .
        function __fish_macos_set_env -d "set an environment variable like path_helper does (macOS only)"
            set -l result

            # Populate path according to config files
            for path_file in $argv[2] $argv[3]/*
                if [ -f $path_file ]
                    while read -l entry
                        if not contains -- $entry $result
                            test -n "$entry"
                            and set -a result $entry
                        end
                    end <$path_file
                end
            end

            # Merge in any existing path elements
            for existing_entry in $$argv[1]
                if not contains -- $existing_entry $result
                    set -a result $existing_entry
                end
            end

            set -xg $argv[1] $result
        end

        __fish_macos_set_env 'PATH' '/etc/paths' '/etc/paths.d'
        if [ -n "$MANPATH" ]
            __fish_macos_set_env 'MANPATH' '/etc/manpaths' '/etc/manpaths.d'
        end
        functions -e __fish_macos_set_env
    end

    #
    # Put linux consoles in unicode mode.
    #
    if test "$TERM" = linux
        and string match -qir '\.UTF' -- $LANG
        and command -sq unicode_start
        unicode_start
    end
...
21	21	--> status --is-login
74	348	-> __fish_reconstruct_path
81	81	--> set -l local_path $PATH
56	56	--> for x in $__fish_added_user_paths
        set -l idx (contains --index -- $x $local_path)
        and set -e local_path[$idx]
    ...
22	22	--> set -g __fish_added_user_paths
5	17	--> if set -q fish_user_paths
        # Explicitly split on ":" because $fish_user_paths might not be a path variable,
        # but $PATH definitely is.
        for x in (string split ":" -- $fish_user_paths[-1..1])
            if set -l idx (contains --index -- $x $local_path)
                set -e local_path[$idx]
            else
                set -ga __fish_added_user_paths $x
            end
            set -p local_path $x
        end
    ...
12	12	---> set -q fish_user_paths
98	98	--> set -xg PATH $local_path
15	15	-> function __fish_expand_pid_args
    for arg in $argv
        if string match -qr '^%\d+$' -- $arg
            # set newargv $newargv (jobs -p $arg)
            jobs -p $arg
            if not test $status -eq 0
                return 1
            end
        else
            printf "%s\n" $arg
        end
    end
...
31	71	-> for jobbltn in bg fg wait disown
    function $jobbltn -V jobbltn
        builtin $jobbltn (__fish_expand_pid_args $argv)
    end
...
20	20	--> function $jobbltn -V jobbltn
        builtin $jobbltn (__fish_expand_pid_args $argv)
    ...
7	7	--> function $jobbltn -V jobbltn
        builtin $jobbltn (__fish_expand_pid_args $argv)
    ...
6	6	--> function $jobbltn -V jobbltn
        builtin $jobbltn (__fish_expand_pid_args $argv)
    ...
7	7	--> function $jobbltn -V jobbltn
        builtin $jobbltn (__fish_expand_pid_args $argv)
    ...
3	3	-> function kill
    command kill (__fish_expand_pid_args $argv)
...
25	25	-> set -l sourcelist
440	11335	-> for file in $__fish_config_dir/conf.d/*.fish $__fish_sysconf_dir/conf.d/*.fish $vendor_confdirs/*.fish
    set -l basename (string replace -r '^.*/' '' -- $file)
    contains -- $basename $sourcelist
    and continue
    set sourcelist $sourcelist $basename
    # Also skip non-files or unreadable files.
    # This allows one to use e.g. symlinks to /dev/null to "mask" something (like in systemd).
    [ -f $file -a -r $file ]
    and source $file
...
178	355	--> set -l basename (string replace -r '^.*/' '' -- $file)
177	177	---> string replace -r '^.*/' '' -- $file
39	39	--> contains -- $basename $sourcelist
15	15	--> set sourcelist $sourcelist $basename
28	28	--> [ -f $file -a -r $file ]
91	9961	--> source $file
13	13	---> set -q XDG_DATA_HOME
20	20	---> set -gx OMF_PATH "$HOME/.local/share/omf"
402	9837	---> source $OMF_PATH/init.fish
8	27	----> if not set -q OMF_CONFIG
  set -q XDG_CONFIG_HOME; or set -l XDG_CONFIG_HOME "$HOME/.config"
  set -gx OMF_CONFIG "$XDG_CONFIG_HOME/omf"
...
19	19	-----> not set -q OMF_CONFIG
21	21	----> test -f $OMF_CONFIG/before.init.fish
32	32	----> emit perf:timer:start "Oh My Fish initialisation"
14	14	----> test -f $OMF_CONFIG/theme
39	39	----> read -l theme < $OMF_CONFIG/theme
25	25	----> set -l core_function_path $OMF_PATH/lib{,/git}
246	246	----> set -l theme_function_path {$OMF_CONFIG,$OMF_PATH}/themes*/$theme{,/functions}
46	46	----> set fish_function_path $fish_function_path[1] \
                       $core_function_path \
                       $theme_function_path \
                       $fish_function_path[2..-1]
47	47	----> emit perf:timer:start "Oh My Fish init installed packages"
252	5458	----> require --path {$OMF_PATH,$OMF_CONFIG}/pkg/*
434	443	-----> source /home/micro-p/.local/share/omf/lib/require.fish
9	9	------> function require
  set packages $argv

  if test -z "$packages"
    echo 'usage: require <name>...'
    echo '       require --path <path>...'
    echo '       require --no-bundle --path <path>...'
    return 1
  end

  # If bundle should be
  if set index (contains -i -- --no-bundle $packages)
    set -e packages[$index]
    set ignore_bundle
  end

  # Requiring absolute paths
  if set index (contains -i -- --path $packages)
    set -e packages[$index]
    set package_path $packages

  # Requiring specific packages from default paths
  else
    set package_path {$OMF_PATH,$OMF_CONFIG}/pkg*/$packages

    # Exit with error if no package paths were generated
    test -z "$package_path"
      and return 1
  end

  set function_path $package_path/functions*
  set complete_path $package_path/completions*
  set init_path $package_path/init.fish*
  set conf_path $package_path/conf.d/*.fish

  # Autoload functions
  test -n "$function_path"
    and set fish_function_path $fish_function_path[1] \
                               $function_path \
                               $fish_function_path[2..-1]

  # Autoload completions
  test -n "$complete_path"
    and set fish_complete_path $fish_complete_path[1] \
                               $complete_path \
                               $fish_complete_path[2..-1]

  for init in $init_path
    emit perf:timer:start $init
    set -l IFS '/'
    echo $init | read -la components

    set path (printf '/%s' $components[1..-2])

    contains $path $omf_init_path
      and continue

    set package $components[-2]

    if not set -q ignore_bundle
      set bundle $path/bundle
      set dependencies

      if test -f $bundle
        set -l IFS ' '
        while read -l type dependency
          test "$type" != package
            and continue
          require "$dependency"
          set dependencies $dependencies $dependency
        end < $bundle
      end
    end

    source $init $path

    emit init_$package $path

    set -g omf_init_path $omf_init_path $path
    emit perf:timer:finish $init
  end

  for conf in $conf_path
    source $conf
  end

  return 0
...
26	26	-----> set packages $argv
4	20	-----> if test -z "$packages"
    echo 'usage: require <name>...'
    echo '       require --path <path>...'
    echo '       require --no-bundle --path <path>...'
    return 1
  ...
16	16	------> test -z "$packages"
5	145	-----> if set index (contains -i -- --no-bundle $packages)
    set -e packages[$index]
    set ignore_bundle
  ...
83	140	------> set index (contains -i -- --no-bundle $packages)
57	57	-------> contains -i -- --no-bundle $packages
12	155	-----> if set index (contains -i -- --path $packages)
    set -e packages[$index]
    set package_path $packages

  # Requiring specific packages from default paths
  else
    set package_path {$OMF_PATH,$OMF_CONFIG}/pkg*/$packages

    # Exit with error if no package paths were generated
    test -z "$package_path"
      and return 1
  ...
78	107	------> set index (contains -i -- --path $packages)
29	29	-------> contains -i -- --path $packages
21	21	------> set -e packages[$index]
15	15	------> set package_path $packages
102	102	-----> set function_path $package_path/functions*
84	84	-----> set complete_path $package_path/completions*
99	99	-----> set init_path $package_path/init.fish*
78	78	-----> set conf_path $package_path/conf.d/*.fish
16	16	-----> test -n "$function_path"
34	34	-----> set fish_function_path $fish_function_path[1] \
                               $function_path \
                               $fish_function_path[2..-1]
12	12	-----> test -n "$complete_path"
27	27	-----> set fish_complete_path $fish_complete_path[1] \
                               $complete_path \
                               $fish_complete_path[2..-1]
50	3931	-----> for init in $init_path
    emit perf:timer:start $init
    set -l IFS '/'
    echo $init | read -la components

    set path (printf '/%s' $components[1..-2])

    contains $path $omf_init_path
      and continue

    set package $components[-2]

    if not set -q ignore_bundle
      set bundle $path/bundle
      set dependencies

      if test -f $bundle
        set -l IFS ' '
        while read -l type dependency
          test "$type" != package
            and continue
          require "$dependency"
          set dependencies $dependencies $dependency
        end < $bundle
      end
    end

    source $init $path

    emit init_$package $path

    set -g omf_init_path $omf_init_path $path
    emit perf:timer:finish $init
  ...
54	54	------> emit perf:timer:start $init
15	15	------> set -l IFS '/'
141	141	------> echo $init | read -la components
88	168	------> set path (printf '/%s' $components[1..-2])
80	80	-------> printf '/%s' $components[1..-2]
56	56	------> contains $path $omf_init_path
16	16	------> set package $components[-2]
11	66	------> if not set -q ignore_bundle
      set bundle $path/bundle
      set dependencies

      if test -f $bundle
        set -l IFS ' '
        while read -l type dependency
          test "$type" != package
            and continue
          require "$dependency"
          set dependencies $dependencies $dependency
        end < $bundle
      end
    ...
9	9	-------> not set -q ignore_bundle
15	15	-------> set bundle $path/bundle
10	10	-------> set dependencies
5	21	-------> if test -f $bundle
        set -l IFS ' '
        while read -l type dependency
          test "$type" != package
            and continue
          require "$dependency"
          set dependencies $dependencies $dependency
        end < $bundle
      ...
16	16	--------> test -f $bundle
197	3177	------> source $init $path
19	19	-------> set -g OMF_MISSING_ARG   1
11	11	-------> set -g OMF_UNKNOWN_OPT   2
10	10	-------> set -g OMF_INVALID_ARG   3
9	9	-------> set -g OMF_UNKNOWN_ERR   4
7	7	-------> function omf::em
  set_color cyan 2> /dev/null
...
8	8	-------> function omf::dim
  set_color 555 2> /dev/null
...
3	3	-------> function omf::err
  set_color red --bold 2> /dev/null
...
3	3	-------> function omf::under
  set_color --underline 2> /dev/null
...
3	3	-------> function omf::off
  set_color normal 2> /dev/null
...
147	2907	-------> autoload $path/functions/{compat,core,index,packages,themes,bundle,util,repo,cli,search}
356	371	--------> source /home/micro-p/.local/share/omf/lib/autoload.fish
8	8	---------> function autoload
  switch "$argv[1]"
  case '-e' '--erase'
    test (count $argv) -ge 2
      and __autoload_erase $argv[2..-1]
      or echo "usage: autoload $argv[1] <path>..." >&2
  case "-*" "--*"
    echo "autoload: invalid option $argv[1]"
    return 1
  case '*'
    test (count $argv) -ge 1
      and __autoload_insert $argv
      or echo "usage: autoload <path>..." >&2
  end
...
4	4	---------> function __autoload_insert
  set -l function_path
  set -l complete_path
  for path in $argv
    not test -d "$path"; and continue
    set -l IFS '/'
    echo $path | read -la components
    if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    end;
  end;
  test -n "$function_path"
    and set fish_function_path $fish_function_path[1] $function_path $fish_function_path[2..-1]
  test -n "$complete_path"
    and set fish_complete_path $fish_complete_path[1] $complete_path $fish_complete_path[2..-1]
  return 0
...
3	3	---------> function __autoload_erase
  set -l function_indexes
  set -l complete_indexes
  for path in $argv
    set -l IFS '/'
    echo $path | read -la components
    test "x$components[-1]" = xcompletions
      and set complete_indexes $complete_indexes (contains -i $path $fish_complete_path)
      or  set function_indexes $function_indexes (contains -i $path $fish_function_path)
  end;
  test -n "$function_indexes"
    and set -e fish_function_path["$function_indexes"]
  test -n "$complete_indexes"
    and set -e fish_complete_path["$complete_indexes"]
  return 0
...
25	2389	--------> switch "$argv[1]"
  case '-e' '--erase'
    test (count $argv) -ge 2
      and __autoload_erase $argv[2..-1]
      or echo "usage: autoload $argv[1] <path>..." >&2
  case "-*" "--*"
    echo "autoload: invalid option $argv[1]"
    return 1
  case '*'
    test (count $argv) -ge 1
      and __autoload_insert $argv
      or echo "usage: autoload <path>..." >&2
  ...
84	167	---------> test (count $argv) -ge 1
83	83	----------> count $argv
62	2197	---------> __autoload_insert $argv
16	16	----------> set -l function_path
11	11	----------> set -l complete_path
141	2026	----------> for path in $argv
    not test -d "$path"; and continue
    set -l IFS '/'
    echo $path | read -la components
    if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    end;
  ...
21	21	-----------> not test -d "$path"
14	14	-----------> set -l IFS '/'
93	93	-----------> echo $path | read -la components
13	113	-----------> if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    ...
19	19	------------> test "x$components[-1]" = xcompletions
60	60	------------> contains -- $path $fish_function_path
21	21	------------> set function_path $function_path $path
20	20	-----------> not test -d "$path"
13	13	-----------> set -l IFS '/'
107	107	-----------> echo $path | read -la components
11	80	-----------> if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    ...
19	19	------------> test "x$components[-1]" = xcompletions
32	32	------------> contains -- $path $fish_function_path
18	18	------------> set function_path $function_path $path
18	18	-----------> not test -d "$path"
13	13	-----------> set -l IFS '/'
85	85	-----------> echo $path | read -la components
13	92	-----------> if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    ...
17	17	------------> test "x$components[-1]" = xcompletions
41	41	------------> contains -- $path $fish_function_path
21	21	------------> set function_path $function_path $path
17	17	-----------> not test -d "$path"
12	12	-----------> set -l IFS '/'
83	83	-----------> echo $path | read -la components
13	79	-----------> if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    ...
16	16	------------> test "x$components[-1]" = xcompletions
31	31	------------> contains -- $path $fish_function_path
19	19	------------> set function_path $function_path $path
17	17	-----------> not test -d "$path"
13	13	-----------> set -l IFS '/'
83	83	-----------> echo $path | read -la components
11	79	-----------> if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    ...
17	17	------------> test "x$components[-1]" = xcompletions
30	30	------------> contains -- $path $fish_function_path
21	21	------------> set function_path $function_path $path
19	19	-----------> not test -d "$path"
19	19	-----------> set -l IFS '/'
105	105	-----------> echo $path | read -la components
12	83	-----------> if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    ...
18	18	------------> test "x$components[-1]" = xcompletions
31	31	------------> contains -- $path $fish_function_path
22	22	------------> set function_path $function_path $path
18	18	-----------> not test -d "$path"
12	12	-----------> set -l IFS '/'
83	83	-----------> echo $path | read -la components
12	83	-----------> if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    ...
17	17	------------> test "x$components[-1]" = xcompletions
30	30	------------> contains -- $path $fish_function_path
24	24	------------> set function_path $function_path $path
17	17	-----------> not test -d "$path"
12	12	-----------> set -l IFS '/'
82	82	-----------> echo $path | read -la components
12	83	-----------> if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    ...
17	17	------------> test "x$components[-1]" = xcompletions
30	30	------------> contains -- $path $fish_function_path
24	24	------------> set function_path $function_path $path
17	17	-----------> not test -d "$path"
13	13	-----------> set -l IFS '/'
81	81	-----------> echo $path | read -la components
11	84	-----------> if test "x$components[-1]" = xcompletions
      contains -- $path $fish_complete_path
        or set complete_path $complete_path $path
    else
      contains -- $path $fish_function_path
        or set function_path $function_path $path
    ...
17	17	------------> test "x$components[-1]" = xcompletions
31	31	------------> contains -- $path $fish_function_path
25	25	------------> set function_path $function_path $path
16	16	-----------> not test -d "$path"
6	6	-----------> continue
19	19	----------> test -n "$function_path"
45	45	----------> set fish_function_path $fish_function_path[1] $function_path $fish_function_path[2..-1]
11	11	----------> test -n "$complete_path"
7	7	----------> return 0
151	151	------> emit init_$package $path
21	21	------> set -g omf_init_path $omf_init_path $path
16	16	------> emit perf:timer:finish $init
20	20	-----> for conf in $conf_path
    source $conf
  ...
14	14	-----> return 0
18	18	----> emit perf:timer:finish "Oh My Fish init installed packages"
221	221	----> functions -q fish_user_key_bindings
13	13	----> function fish_user_key_bindings
  test -f $OMF_CONFIG/theme
    and read -l theme < $OMF_CONFIG/theme
    or set -l theme default
  test -e $OMF_CONFIG/key_bindings.fish;
    and source $OMF_CONFIG/key_bindings.fish
  # Prepare packages key bindings paths
  set -l key_bindings {$OMF_CONFIG,$OMF_PATH}/pkg/*/key_bindings.fish \
                      {$OMF_CONFIG,$OMF_PATH}/themes*/$theme/key_bindings.fish
  # Source all keybindings collected
  for file in $key_bindings
    source $file
  end
  # Call original key bindings if existent
  functions -q __original_fish_user_key_bindings
    and __original_fish_user_key_bindings
...
21	21	----> emit perf:timer:start "Oh My Fish init user config path"
379	2961	----> require --no-bundle --path $OMF_CONFIG
813	823	-----> source /home/micro-p/.local/share/omf/lib/require.fish
10	10	------> function require
  set packages $argv

  if test -z "$packages"
    echo 'usage: require <name>...'
    echo '       require --path <path>...'
    echo '       require --no-bundle --path <path>...'
    return 1
  end

  # If bundle should be
  if set index (contains -i -- --no-bundle $packages)
    set -e packages[$index]
    set ignore_bundle
  end

  # Requiring absolute paths
  if set index (contains -i -- --path $packages)
    set -e packages[$index]
    set package_path $packages

  # Requiring specific packages from default paths
  else
    set package_path {$OMF_PATH,$OMF_CONFIG}/pkg*/$packages

    # Exit with error if no package paths were generated
    test -z "$package_path"
      and return 1
  end

  set function_path $package_path/functions*
  set complete_path $package_path/completions*
  set init_path $package_path/init.fish*
  set conf_path $package_path/conf.d/*.fish

  # Autoload functions
  test -n "$function_path"
    and set fish_function_path $fish_function_path[1] \
                               $function_path \
                               $fish_function_path[2..-1]

  # Autoload completions
  test -n "$complete_path"
    and set fish_complete_path $fish_complete_path[1] \
                               $complete_path \
                               $fish_complete_path[2..-1]

  for init in $init_path
    emit perf:timer:start $init
    set -l IFS '/'
    echo $init | read -la components

    set path (printf '/%s' $components[1..-2])

    contains $path $omf_init_path
      and continue

    set package $components[-2]

    if not set -q ignore_bundle
      set bundle $path/bundle
      set dependencies

      if test -f $bundle
        set -l IFS ' '
        while read -l type dependency
          test "$type" != package
            and continue
          require "$dependency"
          set dependencies $dependencies $dependency
        end < $bundle
      end
    end

    source $init $path

    emit init_$package $path

    set -g omf_init_path $omf_init_path $path
    emit perf:timer:finish $init
  end

  for conf in $conf_path
    source $conf
  end

  return 0
...
26	26	-----> set packages $argv
7	32	-----> if test -z "$packages"
    echo 'usage: require <name>...'
    echo '       require --path <path>...'
    echo '       require --no-bundle --path <path>...'
    return 1
  ...
25	25	------> test -z "$packages"
21	282	-----> if set index (contains -i -- --no-bundle $packages)
    set -e packages[$index]
    set ignore_bundle
  ...
86	220	------> set index (contains -i -- --no-bundle $packages)
134	134	-------> contains -i -- --no-bundle $packages
21	21	------> set -e packages[$index]
20	20	------> set ignore_bundle
49	396	-----> if set index (contains -i -- --path $packages)
    set -e packages[$index]
    set package_path $packages

  # Requiring specific packages from default paths
  else
    set package_path {$OMF_PATH,$OMF_CONFIG}/pkg*/$packages

    # Exit with error if no package paths were generated
    test -z "$package_path"
      and return 1
  ...
164	199	------> set index (contains -i -- --path $packages)
35	35	-------> contains -i -- --path $packages
76	76	------> set -e packages[$index]
72	72	------> set package_path $packages
325	325	-----> set function_path $package_path/functions*
184	184	-----> set complete_path $package_path/completions*
175	175	-----> set init_path $package_path/init.fish*
154	154	-----> set conf_path $package_path/conf.d/*.fish
57	57	-----> test -n "$function_path"
38	38	-----> test -n "$complete_path"
38	38	-----> for init in $init_path
    emit perf:timer:start $init
    set -l IFS '/'
    echo $init | read -la components

    set path (printf '/%s' $components[1..-2])

    contains $path $omf_init_path
      and continue

    set package $components[-2]

    if not set -q ignore_bundle
      set bundle $path/bundle
      set dependencies

      if test -f $bundle
        set -l IFS ' '
        while read -l type dependency
          test "$type" != package
            and continue
          require "$dependency"
          set dependencies $dependencies $dependency
        end < $bundle
      end
    end

    source $init $path

    emit init_$package $path

    set -g omf_init_path $omf_init_path $path
    emit perf:timer:finish $init
  ...
40	40	-----> for conf in $conf_path
    source $conf
  ...
12	12	-----> return 0
20	20	----> emit perf:timer:finish "Oh My Fish init user config path"
188	188	----> set -l theme_conf_path {$OMF_CONFIG,$OMF_PATH}/themes*/$theme/conf.d
14	14	----> for conf in $theme_conf_path/*.fish
  source $conf
...
24	24	----> emit perf:timer:finish "Oh My Fish initialisation"
94	245	--> set -l basename (string replace -r '^.*/' '' -- $file)
151	151	---> string replace -r '^.*/' '' -- $file
49	49	--> contains -- $basename $sourcelist
22	22	--> set sourcelist $sourcelist $basename
35	35	--> [ -f $file -a -r $file ]
121	146	--> source $file
9	25	---> if status --is-login
    for perldir in /usr/bin/site_perl /usr/bin/vendor_perl /usr/bin/core_perl
        if test -d $perldir; and not contains $perldir $PATH
            set PATH $PATH $perldir
        end
    end
...
16	16	----> status --is-login
68	68	> builtin source /etc/fish/config.fish
101	111	> source /usr/share/fish/functions/fish_title.fish
10	10	-> function fish_title
    # emacs is basically the only term that can't handle it.
    if not set -q INSIDE_EMACS
        echo (status current-command) (__fish_pwd)
    end
...
26	2256	> fish_title
11	2230	-> if not set -q INSIDE_EMACS
        echo (status current-command) (__fish_pwd)
    ...
15	15	--> not set -q INSIDE_EMACS
352	2204	--> echo (status current-command) (__fish_pwd)
18	18	---> status current-command
213	1834	---> __fish_pwd
126	1532	----> source /usr/share/fish/functions/__fish_pwd.fish
129	1406	-----> switch (uname)
    case 'CYGWIN_*'
        function __fish_pwd --description "Show current path"
            pwd | sed -e 's-^/cygdrive/\(.\)/\?-\u\1:/-'
        end
    case '*'
        function __fish_pwd --description "Show current path"
            pwd
        end
...
1266	1266	------> uname
11	11	------> function __fish_pwd --description "Show current path"
            pwd
        ...
89	89	----> pwd
333	344	> source /home/micro-p/.config/fish/functions/fish_prompt.fish
11	11	-> function fish_prompt
	# Store the exit code of the last command
	set -g sf_exit_code $status
	set -g SPACEFISH_VERSION 2.7.0

	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_PROMPT_ADD_NEWLINE false
	__sf_util_set_default SPACEFISH_PROMPT_FIRST_PREFIX_SHOW false
	__sf_util_set_default SPACEFISH_PROMPT_PREFIXES_SHOW true
	__sf_util_set_default SPACEFISH_PROMPT_SUFFIXES_SHOW true
	__sf_util_set_default SPACEFISH_PROMPT_DEFAULT_PREFIX "via "
	__sf_util_set_default SPACEFISH_PROMPT_DEFAULT_SUFFIX " "
	__sf_util_set_default SPACEFISH_PROMPT_ORDER time user dir host git package node ruby golang php rust haskell julia elixir docker aws venv conda pyenv dotnet kubecontext exec_time line_sep battery vi_mode jobs exit_code char

	# ------------------------------------------------------------------------------
	# Sections
	# ------------------------------------------------------------------------------

	# Keep track of whether the prompt has already been opened
	set -g sf_prompt_opened $SPACEFISH_PROMPT_FIRST_PREFIX_SHOW

	if test "$SPACEFISH_PROMPT_ADD_NEWLINE" = "true"
		echo
	end

	for i in $SPACEFISH_PROMPT_ORDER
		eval __sf_section_$i
	end
	set_color normal
...
165	177	> source /home/micro-p/.local/share/omf/themes/spacefish/fish_right_prompt.fish
12	12	-> function fish_right_prompt

	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_RPROMPT_ORDER ""

	# ------------------------------------------------------------------------------
	# Sections
	# ------------------------------------------------------------------------------

	[ -n "$SPACEFISH_RPROMPT_ORDER" ]; or return

	for i in $SPACEFISH_RPROMPT_ORDER
		eval __sf_section_$i
	end
	set_color normal
...
32	16715	> __fish_on_interactive
405	16664	-> __fish_config_interactive
2104	2124	--> source /usr/share/fish/functions/__fish_config_interactive.fish
20	20	---> function __fish_config_interactive -d "Initializations that should be performed when entering interactive mode"
    if test $__fish_initialized -lt 3000
        # Perform transitions relevant to going from fish 2.x to 3.x.

        # Migrate old universal abbreviations to the new scheme.
        __fish_abbr_old | source
    end

    # Make sure this function is only run once.
    if set -q __fish_config_interactive_done
        return
    end

    set -g __fish_config_interactive_done
    set -g __fish_active_key_bindings

    if not set -q fish_greeting
        set -l line1 (_ 'Welcome to fish, the friendly interactive shell')
        set -l line2 ''
        if test $__fish_initialized -lt 2300
            set line2 \n(_ 'Type `help` for instructions on how to use fish')
        end
        set -U fish_greeting "$line1$line2"
    end

    if set -q fish_private_mode; and string length -q -- $fish_greeting
        set -l line (_ "fish is running in private mode, history will not be persisted.")
        set -g fish_greeting $fish_greeting.\n$line
    end

    # usage: __init_uvar VARIABLE VALUES...
    function __init_uvar -d "Sets a universal variable if it's not already set"
        if not set --query $argv[1]
            set --universal $argv
        end
    end

    #
    # If we are starting up for the first time, set various defaults.
    if test $__fish_initialized -lt 3100

        # Regular syntax highlighting colors
        __init_uvar fish_color_normal normal
        __init_uvar fish_color_command 005fd7
        __init_uvar fish_color_param 00afff
        __init_uvar fish_color_redirection 00afff
        __init_uvar fish_color_comment 990000
        __init_uvar fish_color_error ff0000
        __init_uvar fish_color_escape 00a6b2
        __init_uvar fish_color_operator 00a6b2
        __init_uvar fish_color_end 009900
        __init_uvar fish_color_quote 999900
        __init_uvar fish_color_autosuggestion 555 brblack
        __init_uvar fish_color_user brgreen
        __init_uvar fish_color_host normal
        __init_uvar fish_color_host_remote yellow
        __init_uvar fish_color_valid_path --underline
        __init_uvar fish_color_status red

        __init_uvar fish_color_cwd green
        __init_uvar fish_color_cwd_root red

        # Background color for matching quotes and parenthesis
        __init_uvar fish_color_match --background=brblue

        # Background color for search matches
        __init_uvar fish_color_search_match bryellow --background=brblack

        # Background color for selections
        __init_uvar fish_color_selection white --bold --background=brblack

        # XXX fish_color_cancel was added in 2.6, but this was added to post-2.3 initialization
        # when 2.4 and 2.5 were already released
        __init_uvar fish_color_cancel -r

        # Pager colors
        __init_uvar fish_pager_color_prefix white --bold --underline
        __init_uvar fish_pager_color_completion
        __init_uvar fish_pager_color_description B3A06D yellow
        __init_uvar fish_pager_color_progress brwhite --background=cyan

        #
        # Directory history colors
        #
        __init_uvar fish_color_history_current --bold
    end

    #
    # Generate man page completions if not present.
    #
    # Don't do this if we're being invoked as part of running unit tests.
    if not set -q FISH_UNIT_TESTS_RUNNING
        if not test -d $__fish_user_data_dir/generated_completions
            # Generating completions from man pages needs python (see issue #3588).

            # We cannot simply do `fish_update_completions &` because it is a function.
            # We cannot do `eval` since it is a function.
            # We don't want to call `fish -c` since that is unnecessary and sources config.fish again.
            # Hence we'll call python directly.
            # c_m_p.py should work with any python version.
            set -l update_args -B $__fish_data_dir/tools/create_manpage_completions.py --manpath --cleanup-in '~/.config/fish/completions' --cleanup-in '~/.config/fish/generated_completions'
            for py in python{3,2,}
                if command -sq $py
                    set -l c $py $update_args
                    # Run python directly in the background and swallow all output
                    $c (: fish_update_completions: generating completions from man pages) >/dev/null 2>&1 &
                    # Then disown the job so that it continues to run in case of an early exit (#6269)
                    disown 2>&1 >/dev/null
                    break
                end
            end
        end
    end

    #
    # Print a greeting.
    # fish_greeting can be a function (preferred) or a variable.
    #
    if status --is-interactive
        if functions -q fish_greeting
            fish_greeting
        else
            # The greeting used to be skipped when fish_greeting was empty (not just undefined)
            # Keep it that way to not print superfluous newlines on old configuration
            test -n "$fish_greeting"
            and echo $fish_greeting
        end
    end

    #
    # This event handler makes sure the prompt is repainted when
    # fish_color_cwd{,_root} changes value. Like all event handlers, it can't be
    # autoloaded.
    #
    set -l varargs --on-variable fish_key_bindings
    for var in user host cwd{,_root} status
        set -a varargs --on-variable fish_color_$var
    end
    function __fish_repaint $varargs -d "Event handler, repaints the prompt when fish_color_cwd* changes"
        if status --is-interactive
            set -e __fish_prompt_cwd
            commandline -f repaint 2>/dev/null
        end
    end

    #
    # Completions for SysV startup scripts. These aren't bound to any
    # specific command, so they can't be autoloaded.
    #
    if test -d /etc/init.d
        complete -x -p "/etc/init.d/*" -a start --description 'Start service'
        complete -x -p "/etc/init.d/*" -a stop --description 'Stop service'
        complete -x -p "/etc/init.d/*" -a status --description 'Print service status'
        complete -x -p "/etc/init.d/*" -a restart --description 'Stop and then start service'
        complete -x -p "/etc/init.d/*" -a reload --description 'Reload service configuration'
    end

    #
    # We want to show our completions for the [ (test) builtin, but
    # we don't want to create a [.fish. test.fish will not be loaded until
    # the user tries [ interactively.
    #
    complete -c [ --wraps test
    complete -c ! --wraps not

    #
    # Only a few builtins take filenames; initialize the rest with no file completions
    #
    complete -c(builtin -n | string match -rv '(source|cd|exec|realpath|set|\\[|test|for)') --no-files

    # Reload key bindings when binding variable change
    function __fish_reload_key_bindings -d "Reload key bindings when binding variable change" --on-variable fish_key_bindings
        # Make sure some key bindings are set
        __init_uvar fish_key_bindings fish_default_key_bindings

        # Do nothing if the key bindings didn't actually change.
        # This could be because the variable was set to the existing value
        # or because it was a local variable.
        # If fish_key_bindings is empty on the first run, we still need to set the defaults.
        if test "$fish_key_bindings" = "$__fish_active_key_bindings" -a -n "$fish_key_bindings"
            return
        end
        # Check if fish_key_bindings is a valid function.
        # If not, either keep the previous bindings (if any) or revert to default.
        # Also print an error so the user knows.
        if not functions -q "$fish_key_bindings"
            echo "There is no fish_key_bindings function called: '$fish_key_bindings'" >&2
            # We need to see if this is a defined function, otherwise we'd be in an endless loop.
            if functions -q $__fish_active_key_bindings
                echo "Keeping $__fish_active_key_bindings" >&2
                # Set the variable to the old value so this error doesn't happen again.
                set fish_key_bindings $__fish_active_key_bindings
                return 1
            else if functions -q fish_default_key_bindings
                echo "Reverting to default bindings" >&2
                set fish_key_bindings fish_default_key_bindings
                # Return because we are called again
                return 0
            else
                # If we can't even find the default bindings, something is broken.
                # Without it, we would eventually run into the stack size limit, but that'd print hundreds of duplicate lines
                # so we should give up earlier.
                echo "Cannot find fish_default_key_bindings, falling back to very simple bindings." >&2
                echo "Most likely something is wrong with your installation." >&2
                return 0
            end
        end
        set -g __fish_active_key_bindings "$fish_key_bindings"
        set -g fish_bind_mode default
        if test "$fish_key_bindings" = fish_default_key_bindings
            # Redirect stderr per #1155
            fish_default_key_bindings 2>/dev/null
        else
            $fish_key_bindings 2>/dev/null
        end
        # Load user key bindings if they are defined
        if functions --query fish_user_key_bindings >/dev/null
            fish_user_key_bindings 2>/dev/null
        end
    end

    # Load key bindings
    __fish_reload_key_bindings

    if not set -q FISH_UNIT_TESTS_RUNNING
        # Enable bracketed paste before every prompt (see __fish_shared_bindings for the bindings).
        # Disable it for unit tests so we don't have to add the sequences to bind.expect
        function __fish_enable_bracketed_paste --on-event fish_prompt
            printf "\e[?2004h"
        end

        # Disable BP before every command because that might not support it.
        function __fish_disable_bracketed_paste --on-event fish_preexec --on-event fish_exit
            printf "\e[?2004l"
        end

        # Tell the terminal we support BP. Since we are in __f_c_i, the first fish_prompt
        # has already fired.
        __fish_enable_bracketed_paste
    end

    # Similarly, enable TMUX's focus reporting when in tmux.
    # This will be handled by
    # - The keybindings (reading the sequence and triggering an event)
    # - Any listeners (like the vi-cursor)
    if set -q TMUX
        and not set -q FISH_UNIT_TESTS_RUNNING
        function __fish_enable_focus --on-event fish_postexec
            echo -n \e\[\?1004h
        end
        function __fish_disable_focus --on-event fish_preexec
            echo -n \e\[\?1004l
        end
        # Note: Don't call this initially because, even though we're in a fish_prompt event,
        # tmux reacts sooo quickly that we'll still get a sequence before we're prepared for it.
        # So this means that we won't get focus events until you've run at least one command, but that's preferable
        # to always seeing `^[[I` when starting fish.
        # __fish_enable_focus
    end

    function __fish_winch_handler --on-signal WINCH -d "Repaint screen when window changes size"
        commandline -f repaint >/dev/null 2>/dev/null
    end

    # Notify terminals when $PWD changes (issue #906).
    # VTE based terminals, Terminal.app, and iTerm.app (TODO) support this.
    if test 0"$VTE_VERSION" -ge 3405 -o "$TERM_PROGRAM" = "Apple_Terminal" -a (string match -r '\d+' 0"$TERM_PROGRAM_VERSION") -ge 309
        function __update_cwd_osc --on-variable PWD --description 'Notify capable terminals when $PWD changes'
            if status --is-command-substitution || set -q INSIDE_EMACS
                return
            end
            printf \e\]7\;file://%s%s\a $hostname (string escape --style=url $PWD)
        end
        __update_cwd_osc # Run once because we might have already inherited a PWD from an old tab
    end

    ### Command-not-found handlers
    # This can be overridden by defining a new __fish_command_not_found_handler function
    if not type -q __fish_command_not_found_handler
        # Read the OS/Distro from /etc/os-release.
        # This has a "ID=" line that defines the exact distribution,
        # and an "ID_LIKE=" line that defines what it is derived from or otherwise like.
        # For our purposes, we use both.
        set -l os
        if test -r /etc/os-release
            set os (string match -r '^ID(?:_LIKE)?\s*=.*' < /etc/os-release | \
            string replace -r '^ID(?:_LIKE)?\s*=(.*)' '$1' | string trim -c '\'"' | string split " ")
        end

        # First check if we are on OpenSUSE since SUSE's handler has no options
        # but the same name and path as Ubuntu's.
        if contains -- suse $os || contains -- sles $os && type -q command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /usr/bin/command-not-found $argv[1]
            end
            # Check for Fedora's handler
        else if test -f /usr/libexec/pk-command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /usr/libexec/pk-command-not-found $argv[1]
            end
            # Check in /usr/lib, this is where modern Ubuntus place this command
        else if test -f /usr/lib/command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /usr/lib/command-not-found -- $argv[1]
            end
            # Check for NixOS handler
        else if test -f /run/current-system/sw/bin/command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /run/current-system/sw/bin/command-not-found $argv
            end
            # Ubuntu Feisty places this command in the regular path instead
        else if type -q command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                command-not-found -- $argv[1]
            end
            # pkgfile is an optional, but official, package on Arch Linux
            # it ships with example handlers for bash and zsh, so we'll follow that format
        else if type -p -q pkgfile
            function __fish_command_not_found_handler --on-event fish_command_not_found
                set -l __packages (pkgfile --binaries --verbose -- $argv[1] 2>/dev/null)
                if test $status -eq 0
                    printf "%s may be found in the following packages:\n" "$argv[1]"
                    printf "  %s\n" $__packages
                else
                    __fish_default_command_not_found_handler $argv[1]
                end
            end
            # Use standard fish command not found handler otherwise
        else
            function __fish_command_not_found_handler --on-event fish_command_not_found
                __fish_default_command_not_found_handler $argv[1]
            end
        end
    end

    # Bump this whenever some code below needs to run once when upgrading to a new version.
    # The universal variable __fish_initialized is initialized in share/config.fish.
    set __fish_initialized 3100
...
4	42	--> if test $__fish_initialized -lt 3000
        # Perform transitions relevant to going from fish 2.x to 3.x.

        # Migrate old universal abbreviations to the new scheme.
        __fish_abbr_old | source
    ...
38	38	---> test $__fish_initialized -lt 3000
3	20	--> if set -q __fish_config_interactive_done
        return
    ...
17	17	---> set -q __fish_config_interactive_done
21	21	--> set -g __fish_config_interactive_done
17	17	--> set -g __fish_active_key_bindings
3	16	--> if not set -q fish_greeting
        set -l line1 (_ 'Welcome to fish, the friendly interactive shell')
        set -l line2 ''
        if test $__fish_initialized -lt 2300
            set line2 \n(_ 'Type `help` for instructions on how to use fish')
        end
        set -U fish_greeting "$line1$line2"
    ...
13	13	---> not set -q fish_greeting
3	14	--> if set -q fish_private_mode; and string length -q -- $fish_greeting
        set -l line (_ "fish is running in private mode, history will not be persisted.")
        set -g fish_greeting $fish_greeting.\n$line
    ...
11	11	---> set -q fish_private_mode
15	15	--> function __init_uvar -d "Sets a universal variable if it's not already set"
        if not set --query $argv[1]
            set --universal $argv
        end
    ...
4	25	--> if test $__fish_initialized -lt 3100

        # Regular syntax highlighting colors
        __init_uvar fish_color_normal normal
        __init_uvar fish_color_command 005fd7
        __init_uvar fish_color_param 00afff
        __init_uvar fish_color_redirection 00afff
        __init_uvar fish_color_comment 990000
        __init_uvar fish_color_error ff0000
        __init_uvar fish_color_escape 00a6b2
        __init_uvar fish_color_operator 00a6b2
        __init_uvar fish_color_end 009900
        __init_uvar fish_color_quote 999900
        __init_uvar fish_color_autosuggestion 555 brblack
        __init_uvar fish_color_user brgreen
        __init_uvar fish_color_host normal
        __init_uvar fish_color_host_remote yellow
        __init_uvar fish_color_valid_path --underline
        __init_uvar fish_color_status red

        __init_uvar fish_color_cwd green
        __init_uvar fish_color_cwd_root red

        # Background color for matching quotes and parenthesis
        __init_uvar fish_color_match --background=brblue

        # Background color for search matches
        __init_uvar fish_color_search_match bryellow --background=brblack

        # Background color for selections
        __init_uvar fish_color_selection white --bold --background=brblack

        # XXX fish_color_cancel was added in 2.6, but this was added to post-2.3 initialization
        # when 2.4 and 2.5 were already released
        __init_uvar fish_color_cancel -r

        # Pager colors
        __init_uvar fish_pager_color_prefix white --bold --underline
        __init_uvar fish_pager_color_completion
        __init_uvar fish_pager_color_description B3A06D yellow
        __init_uvar fish_pager_color_progress brwhite --background=cyan

        #
        # Directory history colors
        #
        __init_uvar fish_color_history_current --bold
    ...
21	21	---> test $__fish_initialized -lt 3100
18	70	--> if not set -q FISH_UNIT_TESTS_RUNNING
        if not test -d $__fish_user_data_dir/generated_completions
            # Generating completions from man pages needs python (see issue #3588).

            # We cannot simply do `fish_update_completions &` because it is a function.
            # We cannot do `eval` since it is a function.
            # We don't want to call `fish -c` since that is unnecessary and sources config.fish again.
            # Hence we'll call python directly.
            # c_m_p.py should work with any python version.
            set -l update_args -B $__fish_data_dir/tools/create_manpage_completions.py --manpath --cleanup-in '~/.config/fish/completions' --cleanup-in '~/.config/fish/generated_completions'
            for py in python{3,2,}
                if command -sq $py
                    set -l c $py $update_args
                    # Run python directly in the background and swallow all output
                    $c (: fish_update_completions: generating completions from man pages) >/dev/null 2>&1 &
                    # Then disown the job so that it continues to run in case of an early exit (#6269)
                    disown 2>&1 >/dev/null
                    break
                end
            end
        end
    ...
13	13	---> not set -q FISH_UNIT_TESTS_RUNNING
4	39	---> if not test -d $__fish_user_data_dir/generated_completions
            # Generating completions from man pages needs python (see issue #3588).

            # We cannot simply do `fish_update_completions &` because it is a function.
            # We cannot do `eval` since it is a function.
            # We don't want to call `fish -c` since that is unnecessary and sources config.fish again.
            # Hence we'll call python directly.
            # c_m_p.py should work with any python version.
            set -l update_args -B $__fish_data_dir/tools/create_manpage_completions.py --manpath --cleanup-in '~/.config/fish/completions' --cleanup-in '~/.config/fish/generated_completions'
            for py in python{3,2,}
                if command -sq $py
                    set -l c $py $update_args
                    # Run python directly in the background and swallow all output
                    $c (: fish_update_completions: generating completions from man pages) >/dev/null 2>&1 &
                    # Then disown the job so that it continues to run in case of an early exit (#6269)
                    disown 2>&1 >/dev/null
                    break
                end
            end
        ...
35	35	----> not test -d $__fish_user_data_dir/generated_completions
12	457	--> if status --is-interactive
        if functions -q fish_greeting
            fish_greeting
        else
            # The greeting used to be skipped when fish_greeting was empty (not just undefined)
            # Keep it that way to not print superfluous newlines on old configuration
            test -n "$fish_greeting"
            and echo $fish_greeting
        end
    ...
14	14	---> status --is-interactive
38	431	---> if functions -q fish_greeting
            fish_greeting
        else
            # The greeting used to be skipped when fish_greeting was empty (not just undefined)
            # Keep it that way to not print superfluous newlines on old configuration
            test -n "$fish_greeting"
            and echo $fish_greeting
        ...
225	225	----> functions -q fish_greeting
130	130	----> test -n "$fish_greeting"
38	38	----> echo $fish_greeting
24	24	--> set -l varargs --on-variable fish_key_bindings
42	120	--> for var in user host cwd{,_root} status
        set -a varargs --on-variable fish_color_$var
    ...
21	21	---> set -a varargs --on-variable fish_color_$var
16	16	---> set -a varargs --on-variable fish_color_$var
14	14	---> set -a varargs --on-variable fish_color_$var
14	14	---> set -a varargs --on-variable fish_color_$var
13	13	---> set -a varargs --on-variable fish_color_$var
38	38	--> function __fish_repaint $varargs -d "Event handler, repaints the prompt when fish_color_cwd* changes"
        if status --is-interactive
            set -e __fish_prompt_cwd
            commandline -f repaint 2>/dev/null
        end
    ...
4	18	--> if test -d /etc/init.d
        complete -x -p "/etc/init.d/*" -a start --description 'Start service'
        complete -x -p "/etc/init.d/*" -a stop --description 'Stop service'
        complete -x -p "/etc/init.d/*" -a status --description 'Print service status'
        complete -x -p "/etc/init.d/*" -a restart --description 'Stop and then start service'
        complete -x -p "/etc/init.d/*" -a reload --description 'Reload service configuration'
    ...
14	14	---> test -d /etc/init.d
168	168	--> complete -c [ --wraps test
22	22	--> complete -c ! --wraps not
318	639	--> complete -c(builtin -n | string match -rv '(source|cd|exec|realpath|set|\\[|test|for)') --no-files
321	321	---> builtin -n | string match -rv '(source|cd|exec|realpath|set|\\[|test|for)'
16	16	--> function __fish_reload_key_bindings -d "Reload key bindings when binding variable change" --on-variable fish_key_bindings
        # Make sure some key bindings are set
        __init_uvar fish_key_bindings fish_default_key_bindings

        # Do nothing if the key bindings didn't actually change.
        # This could be because the variable was set to the existing value
        # or because it was a local variable.
        # If fish_key_bindings is empty on the first run, we still need to set the defaults.
        if test "$fish_key_bindings" = "$__fish_active_key_bindings" -a -n "$fish_key_bindings"
            return
        end
        # Check if fish_key_bindings is a valid function.
        # If not, either keep the previous bindings (if any) or revert to default.
        # Also print an error so the user knows.
        if not functions -q "$fish_key_bindings"
            echo "There is no fish_key_bindings function called: '$fish_key_bindings'" >&2
            # We need to see if this is a defined function, otherwise we'd be in an endless loop.
            if functions -q $__fish_active_key_bindings
                echo "Keeping $__fish_active_key_bindings" >&2
                # Set the variable to the old value so this error doesn't happen again.
                set fish_key_bindings $__fish_active_key_bindings
                return 1
            else if functions -q fish_default_key_bindings
                echo "Reverting to default bindings" >&2
                set fish_key_bindings fish_default_key_bindings
                # Return because we are called again
                return 0
            else
                # If we can't even find the default bindings, something is broken.
                # Without it, we would eventually run into the stack size limit, but that'd print hundreds of duplicate lines
                # so we should give up earlier.
                echo "Cannot find fish_default_key_bindings, falling back to very simple bindings." >&2
                echo "Most likely something is wrong with your installation." >&2
                return 0
            end
        end
        set -g __fish_active_key_bindings "$fish_key_bindings"
        set -g fish_bind_mode default
        if test "$fish_key_bindings" = fish_default_key_bindings
            # Redirect stderr per #1155
            fish_default_key_bindings 2>/dev/null
        else
            $fish_key_bindings 2>/dev/null
        end
        # Load user key bindings if they are defined
        if functions --query fish_user_key_bindings >/dev/null
            fish_user_key_bindings 2>/dev/null
        end
    ...
37	6195	--> __fish_reload_key_bindings
29	51	---> __init_uvar fish_key_bindings fish_default_key_bindings
3	22	----> if not set --query $argv[1]
            set --universal $argv
        ...
19	19	-----> not set --query $argv[1]
3	29	---> if test "$fish_key_bindings" = "$__fish_active_key_bindings" -a -n "$fish_key_bindings"
            return
        ...
26	26	----> test "$fish_key_bindings" = "$__fish_active_key_bindings" -a -n "$fish_key_bindings"
3	731	---> if not functions -q "$fish_key_bindings"
            echo "There is no fish_key_bindings function called: '$fish_key_bindings'" >&2
            # We need to see if this is a defined function, otherwise we'd be in an endless loop.
            if functions -q $__fish_active_key_bindings
                echo "Keeping $__fish_active_key_bindings" >&2
                # Set the variable to the old value so this error doesn't happen again.
                set fish_key_bindings $__fish_active_key_bindings
                return 1
            else if functions -q fish_default_key_bindings
                echo "Reverting to default bindings" >&2
                set fish_key_bindings fish_default_key_bindings
                # Return because we are called again
                return 0
            else
                # If we can't even find the default bindings, something is broken.
                # Without it, we would eventually run into the stack size limit, but that'd print hundreds of duplicate lines
                # so we should give up earlier.
                echo "Cannot find fish_default_key_bindings, falling back to very simple bindings." >&2
                echo "Most likely something is wrong with your installation." >&2
                return 0
            end
        ...
189	728	----> not functions -q "$fish_key_bindings"
525	539	-----> source /usr/share/fish/functions/fish_default_key_bindings.fish
14	14	------> function fish_default_key_bindings -d "Default (Emacs-like) key bindings for fish"
    if contains -- -h $argv
        or contains -- --help $argv
        echo "Sorry but this function doesn't support -h or --help"
        return 1
    end

    if not set -q argv[1]
        bind --erase --all --preset # clear earlier bindings, if any
        if test "$fish_key_bindings" != "fish_default_key_bindings"
            # Allow the user to set the variable universally
            set -q fish_key_bindings
            or set -g fish_key_bindings
            # This triggers the handler, which calls us again and ensures the user_key_bindings
            # are executed.
            set fish_key_bindings fish_default_key_bindings
            return
        end
    end

    # Silence warnings about unavailable keys. See #4431, 4188
    if not contains -- -s $argv
        set argv "-s" $argv
    end

    # These are shell-specific bindings that we share with vi mode.
    __fish_shared_key_bindings $argv
    or return # protect against invalid $argv

    # This is the default binding, i.e. the one used if no other binding matches
    bind --preset $argv "" self-insert
    or exit # protect against invalid $argv

    # Space expands abbrs _and_ inserts itself.
    bind --preset $argv " " self-insert expand-abbr

    bind --preset $argv \n execute
    bind --preset $argv \r execute

    bind --preset $argv \ck kill-line

    bind --preset $argv \eOC forward-char
    bind --preset $argv \eOD backward-char
    bind --preset $argv \e\[C forward-char
    bind --preset $argv \e\[D backward-char
    bind --preset $argv -k right forward-char
    bind --preset $argv -k left backward-char

    bind --preset $argv -k dc delete-char
    bind --preset $argv -k backspace backward-delete-char
    bind --preset $argv \x7f backward-delete-char

    # for PuTTY
    # https://github.com/fish-shell/fish-shell/issues/180
    bind --preset $argv \e\[1~ beginning-of-line
    bind --preset $argv \e\[3~ delete-char
    bind --preset $argv \e\[4~ end-of-line

    # OS X SnowLeopard doesn't have these keys. Don't show an annoying error message.
    bind --preset $argv -k home beginning-of-line 2>/dev/null
    bind --preset $argv -k end end-of-line 2>/dev/null
    bind --preset $argv \e\[3\;2~ backward-delete-char # Mavericks Terminal.app shift-ctrl-delete

    bind --preset $argv \ca beginning-of-line
    bind --preset $argv \ce end-of-line
    bind --preset $argv \ch backward-delete-char
    bind --preset $argv \cp up-or-search
    bind --preset $argv \cn down-or-search
    bind --preset $argv \cf forward-char
    bind --preset $argv \cb backward-char
    bind --preset $argv \ct transpose-chars
    bind --preset $argv \et transpose-words
    bind --preset $argv \eu upcase-word

    # This clashes with __fish_list_current_token
    # bind --preset $argv \el downcase-word
    bind --preset $argv \ec capitalize-word
    # One of these is alt+backspace.
    bind --preset $argv \e\x7f backward-kill-word
    bind --preset $argv \e\b backward-kill-word
    bind --preset $argv \eb backward-word
    bind --preset $argv \ef forward-word
    bind --preset $argv \e\[1\;5C forward-word
    bind --preset $argv \e\[1\;5D backward-word
    bind --preset $argv \e\< beginning-of-buffer
    bind --preset $argv \e\> end-of-buffer

    bind --preset $argv \ed kill-word

    # term-specific special bindings
    switch "$TERM"
        case 'rxvt*'
            bind --preset $argv \e\[8~ end-of-line
            bind --preset $argv \eOc forward-word
            bind --preset $argv \eOd backward-word
        case 'xterm-256color'
            # Microsoft's conemu uses xterm-256color plus
            # the following to tell a console to paste:
            bind --preset $argv \e\x20ep fish_clipboard_paste
    end
...
29	29	---> set -g __fish_active_key_bindings "$fish_key_bindings"
12	12	---> set -g fish_bind_mode default
8	4460	---> if test "$fish_key_bindings" = fish_default_key_bindings
            # Redirect stderr per #1155
            fish_default_key_bindings 2>/dev/null
        else
            $fish_key_bindings 2>/dev/null
        ...
14	14	----> test "$fish_key_bindings" = fish_default_key_bindings
121	4438	----> fish_default_key_bindings 2>/dev/null
5	35	-----> if contains -- -h $argv
        or contains -- --help $argv
        echo "Sorry but this function doesn't support -h or --help"
        return 1
    ...
19	19	------> contains -- -h $argv
11	11	------> contains -- --help $argv
10	123	-----> if not set -q argv[1]
        bind --erase --all --preset # clear earlier bindings, if any
        if test "$fish_key_bindings" != "fish_default_key_bindings"
            # Allow the user to set the variable universally
            set -q fish_key_bindings
            or set -g fish_key_bindings
            # This triggers the handler, which calls us again and ensures the user_key_bindings
            # are executed.
            set fish_key_bindings fish_default_key_bindings
            return
        end
    ...
9	9	------> not set -q argv[1]
87	87	------> bind --erase --all --preset
2	17	------> if test "$fish_key_bindings" != "fish_default_key_bindings"
            # Allow the user to set the variable universally
            set -q fish_key_bindings
            or set -g fish_key_bindings
            # This triggers the handler, which calls us again and ensures the user_key_bindings
            # are executed.
            set fish_key_bindings fish_default_key_bindings
            return
        ...
15	15	-------> test "$fish_key_bindings" != "fish_default_key_bindings"
6	34	-----> if not contains -- -s $argv
        set argv "-s" $argv
    ...
13	13	------> not contains -- -s $argv
15	15	------> set argv "-s" $argv
282	3126	-----> __fish_shared_key_bindings $argv
1011	1039	------> source /usr/share/fish/functions/__fish_shared_key_bindings.fish
13	13	-------> function __fish_shared_key_bindings -d "Bindings shared between emacs and vi mode"
    # These are some bindings that are supposed to be shared between vi mode and default mode.
    # They are supposed to be unrelated to text-editing (or movement).
    # This takes $argv so the vi-bindings can pass the mode they are valid in.

    if contains -- -h $argv
        or contains -- --help $argv
        echo "Sorry but this function doesn't support -h or --help"
        return 1
    end

    bind --preset $argv \cy yank
    or return # protect against invalid $argv
    bind --preset $argv \ey yank-pop

    # Left/Right arrow
    bind --preset $argv -k right forward-char
    bind --preset $argv -k left backward-char
    bind --preset $argv \e\[C forward-char
    bind --preset $argv \e\[D backward-char
    # Some terminals output these when they're in in keypad mode.
    bind --preset $argv \eOC forward-char
    bind --preset $argv \eOD backward-char

    bind --preset $argv -k ppage beginning-of-history
    bind --preset $argv -k npage end-of-history

    # Interaction with the system clipboard.
    bind --preset $argv \cx fish_clipboard_copy
    bind --preset $argv \cv fish_clipboard_paste

    bind --preset $argv \e cancel
    bind --preset $argv \t complete
    bind --preset $argv \cs pager-toggle-search
    # shift-tab does a tab complete followed by a search.
    bind --preset $argv --key btab complete-and-search

    bind --preset $argv \e\n "commandline -i \n"
    bind --preset $argv \e\r "commandline -i \n"

    bind --preset $argv -k down down-or-search
    bind --preset $argv -k up up-or-search
    bind --preset $argv \e\[A up-or-search
    bind --preset $argv \e\[B down-or-search
    bind --preset $argv \eOA up-or-search
    bind --preset $argv \eOB down-or-search

    bind --preset $argv -k sright forward-bigword
    bind --preset $argv -k sleft backward-bigword

    # Alt-left/Alt-right
    bind --preset $argv \e\eOC nextd-or-forward-word
    bind --preset $argv \e\eOD prevd-or-backward-word
    bind --preset $argv \e\e\[C nextd-or-forward-word
    bind --preset $argv \e\e\[D prevd-or-backward-word
    bind --preset $argv \eO3C nextd-or-forward-word
    bind --preset $argv \eO3D prevd-or-backward-word
    bind --preset $argv \e\[3C nextd-or-forward-word
    bind --preset $argv \e\[3D prevd-or-backward-word
    bind --preset $argv \e\[1\;3C nextd-or-forward-word
    bind --preset $argv \e\[1\;3D prevd-or-backward-word
    bind --preset $argv \e\[1\;9C nextd-or-forward-word #iTerm2
    bind --preset $argv \e\[1\;9D prevd-or-backward-word #iTerm2

    # Alt-up/Alt-down
    bind --preset $argv \e\eOA history-token-search-backward
    bind --preset $argv \e\eOB history-token-search-forward
    bind --preset $argv \e\e\[A history-token-search-backward
    bind --preset $argv \e\e\[B history-token-search-forward
    bind --preset $argv \eO3A history-token-search-backward
    bind --preset $argv \eO3B history-token-search-forward
    bind --preset $argv \e\[3A history-token-search-backward
    bind --preset $argv \e\[3B history-token-search-forward
    bind --preset $argv \e\[1\;3A history-token-search-backward
    bind --preset $argv \e\[1\;3B history-token-search-forward
    bind --preset $argv \e\[1\;9A history-token-search-backward # iTerm2
    bind --preset $argv \e\[1\;9B history-token-search-forward # iTerm2
    # Bash compatibility
    # https://github.com/fish-shell/fish-shell/issues/89
    bind --preset $argv \e. history-token-search-backward

    bind --preset $argv \el __fish_list_current_token
    bind --preset $argv \ew __fish_whatis_current_token
    # ncurses > 6.0 sends a "delete scrollback" sequence along with clear.
    # This string replace removes it.
    bind --preset $argv \cl 'echo -n (clear | string replace \e\[3J ""); commandline -f repaint'
    bind --preset $argv \cc __fish_cancel_commandline
    bind --preset $argv \cu backward-kill-line
    bind --preset $argv \cw backward-kill-path-component
    bind --preset $argv \e\[F end-of-line
    bind --preset $argv \e\[H beginning-of-line

    bind --preset $argv \ed 'set -l cmd (commandline); if test -z "$cmd"; echo; dirh; commandline -f repaint; else; commandline -f kill-word; end'
    bind --preset $argv \cd delete-or-exit

    # Prepend 'sudo ' to the current commandline
    bind --preset $argv \es __fish_prepend_sudo

    # Allow reading manpages by pressing F1 (many GUI applications) or Alt+h (like in zsh).
    bind --preset $argv -k f1 __fish_man_page
    bind --preset $argv \eh __fish_man_page

    # This will make sure the output of the current command is paged using the default pager when
    # you press Meta-p.
    # If none is set, less will be used.
    bind --preset $argv \ep '__fish_paginate'

    # Make it easy to turn an unexecuted command into a comment in the shell history. Also,
    # remove the commenting chars so the command can be further edited then executed.
    bind --preset $argv \e\# __fish_toggle_comment_commandline

    # The [meta-e] and [meta-v] keystrokes invoke an external editor on the command buffer.
    bind --preset $argv \ee edit_command_buffer
    bind --preset $argv \ev edit_command_buffer

    # Tmux' focus events.
    # Exclude paste mode because that should get _everything_ literally.
    for mode in (bind --list-modes | string match -v paste)
        # We only need the in-focus event currently (to redraw the vi-cursor).
        bind --preset -M $mode \e\[I 'emit fish_focus_in'
        bind --preset -M $mode \e\[O false
        bind --preset -M $mode \e\[\?1004h false
    end

    # Support for "bracketed paste"
    # The way it works is that we acknowledge our support by printing
    # \e\[?2004h
    # then the terminal will "bracket" every paste in
    # \e\[200~ and \e\[201~
    # Every character in between those two will be part of the paste and should not cause a binding to execute (like \n executing commands).
    #
    # We enable it after every command and disable it before (in __fish_config_interactive.fish)
    #
    # Support for this seems to be ubiquitous - emacs enables it unconditionally (!) since 25.1
    # (though it only supports it since then, it seems to be the last term to gain support).
    #
    # NOTE: This is more of a "security" measure than a proper feature.
    # The better way to paste remains the `fish_clipboard_paste` function (bound to \cv by default).
    # We don't disable highlighting here, so it will be redone after every character (which can be slow),
    # and it doesn't handle "paste-stop" sequences in the paste (which the terminal needs to strip).
    #
    # See http://thejh.net/misc/website-terminal-copy-paste.

    # Bind the starting sequence in every bind mode, even user-defined ones.
    # Exclude paste mode or there'll be an additional binding after switching between emacs and vi
    for mode in (bind --list-modes | string match -v paste)
        bind --preset -M $mode -m paste \e\[200~ '__fish_start_bracketed_paste'
    end
    # This sequence ends paste-mode and returns to the previous mode we have saved before.
    bind --preset -M paste \e\[201~ '__fish_stop_bracketed_paste'
    # In paste-mode, everything self-inserts except for the sequence to get out of it
    bind --preset -M paste "" self-insert
    # Without this, a \r will overwrite the other text, rendering it invisible - which makes the exercise kinda pointless.
    bind --preset -M paste \r "commandline -i \n"

    # We usually just pass the text through as-is to facilitate pasting code,
    # but when the current token contains an unbalanced single-quote (`'`),
    # we escape all single-quotes and backslashes, effectively turning the paste
    # into one literal token, to facilitate pasting non-code (e.g. markdown or git commitishes)
    bind --preset -M paste "'" "__fish_commandline_insert_escaped \' \$__fish_paste_quoted"
    bind --preset -M paste \\ "__fish_commandline_insert_escaped \\\ \$__fish_paste_quoted"
    # Only insert spaces if we're either quoted or not at the beginning of the commandline
    # - this strips leading spaces if they would trigger histignore.
    bind --preset -M paste " " self-insert-notfirst
...
8	8	-------> function __fish_commandline_insert_escaped --description 'Insert the first arg escaped if a second arg is given'
    if set -q argv[2]
        commandline -i \\$argv[1]
    else
        commandline -i $argv[1]
    end
...
4	4	-------> function __fish_start_bracketed_paste
    # Save the last bind mode so we can restore it.
    set -g __fish_last_bind_mode $fish_bind_mode
    # If the token is currently single-quoted,
    # we escape single-quotes (and backslashes).
    __fish_commandline_is_singlequoted
    and set -g __fish_paste_quoted 1
...
3	3	-------> function __fish_stop_bracketed_paste
    # Restore the last bind mode.
    set fish_bind_mode $__fish_last_bind_mode
    set -e __fish_paste_quoted
    commandline -f force-repaint
...
5	39	------> if contains -- -h $argv
        or contains -- --help $argv
        echo "Sorry but this function doesn't support -h or --help"
        return 1
    ...
21	21	-------> contains -- -h $argv
13	13	-------> contains -- --help $argv
19	19	------> bind --preset $argv \cy yank
16	16	------> bind --preset $argv \ey yank-pop
19	19	------> bind --preset $argv -k right forward-char
16	16	------> bind --preset $argv -k left backward-char
15	15	------> bind --preset $argv \e\[C forward-char
15	15	------> bind --preset $argv \e\[D backward-char
14	14	------> bind --preset $argv \eOC forward-char
14	14	------> bind --preset $argv \eOD backward-char
15	15	------> bind --preset $argv -k ppage beginning-of-history
16	16	------> bind --preset $argv -k npage end-of-history
14	14	------> bind --preset $argv \cx fish_clipboard_copy
14	14	------> bind --preset $argv \cv fish_clipboard_paste
14	14	------> bind --preset $argv \e cancel
14	14	------> bind --preset $argv \t complete
14	14	------> bind --preset $argv \cs pager-toggle-search
14	14	------> bind --preset $argv --key btab complete-and-search
16	16	------> bind --preset $argv \e\n "commandline -i \n"
15	15	------> bind --preset $argv \e\r "commandline -i \n"
16	16	------> bind --preset $argv -k down down-or-search
17	17	------> bind --preset $argv -k up up-or-search
15	15	------> bind --preset $argv \e\[A up-or-search
14	14	------> bind --preset $argv \e\[B down-or-search
14	14	------> bind --preset $argv \eOA up-or-search
13	13	------> bind --preset $argv \eOB down-or-search
29	29	------> bind --preset $argv -k sright forward-bigword
16	16	------> bind --preset $argv -k sleft backward-bigword
15	15	------> bind --preset $argv \e\eOC nextd-or-forward-word
15	15	------> bind --preset $argv \e\eOD prevd-or-backward-word
14	14	------> bind --preset $argv \e\e\[C nextd-or-forward-word
15	15	------> bind --preset $argv \e\e\[D prevd-or-backward-word
14	14	------> bind --preset $argv \eO3C nextd-or-forward-word
14	14	------> bind --preset $argv \eO3D prevd-or-backward-word
14	14	------> bind --preset $argv \e\[3C nextd-or-forward-word
14	14	------> bind --preset $argv \e\[3D prevd-or-backward-word
15	15	------> bind --preset $argv \e\[1\;3C nextd-or-forward-word
14	14	------> bind --preset $argv \e\[1\;3D prevd-or-backward-word
15	15	------> bind --preset $argv \e\[1\;9C nextd-or-forward-word
15	15	------> bind --preset $argv \e\[1\;9D prevd-or-backward-word
15	15	------> bind --preset $argv \e\eOA history-token-search-backward
14	14	------> bind --preset $argv \e\eOB history-token-search-forward
14	14	------> bind --preset $argv \e\e\[A history-token-search-backward
14	14	------> bind --preset $argv \e\e\[B history-token-search-forward
14	14	------> bind --preset $argv \eO3A history-token-search-backward
14	14	------> bind --preset $argv \eO3B history-token-search-forward
15	15	------> bind --preset $argv \e\[3A history-token-search-backward
14	14	------> bind --preset $argv \e\[3B history-token-search-forward
15	15	------> bind --preset $argv \e\[1\;3A history-token-search-backward
14	14	------> bind --preset $argv \e\[1\;3B history-token-search-forward
14	14	------> bind --preset $argv \e\[1\;9A history-token-search-backward
14	14	------> bind --preset $argv \e\[1\;9B history-token-search-forward
15	15	------> bind --preset $argv \e. history-token-search-backward
14	14	------> bind --preset $argv \el __fish_list_current_token
14	14	------> bind --preset $argv \ew __fish_whatis_current_token
16	16	------> bind --preset $argv \cl 'echo -n (clear | string replace \e\[3J ""); commandline -f repaint'
14	14	------> bind --preset $argv \cc __fish_cancel_commandline
15	15	------> bind --preset $argv \cu backward-kill-line
18	18	------> bind --preset $argv \cw backward-kill-path-component
17	17	------> bind --preset $argv \e\[F end-of-line
16	16	------> bind --preset $argv \e\[H beginning-of-line
19	19	------> bind --preset $argv \ed 'set -l cmd (commandline); if test -z "$cmd"; echo; dirh; commandline -f repaint; else; commandline -f kill-word; end'
16	16	------> bind --preset $argv \cd delete-or-exit
16	16	------> bind --preset $argv \es __fish_prepend_sudo
18	18	------> bind --preset $argv -k f1 __fish_man_page
16	16	------> bind --preset $argv \eh __fish_man_page
17	17	------> bind --preset $argv \ep '__fish_paginate'
16	16	------> bind --preset $argv \e\# __fish_toggle_comment_commandline
16	16	------> bind --preset $argv \ee edit_command_buffer
15	15	------> bind --preset $argv \ev edit_command_buffer
107	357	------> for mode in (bind --list-modes | string match -v paste)
        # We only need the in-focus event currently (to redraw the vi-cursor).
        bind --preset -M $mode \e\[I 'emit fish_focus_in'
        bind --preset -M $mode \e\[O false
        bind --preset -M $mode \e\[\?1004h false
    ...
149	149	-------> bind --list-modes | string match -v paste
56	56	-------> bind --preset -M $mode \e\[I 'emit fish_focus_in'
27	27	-------> bind --preset -M $mode \e\[O false
18	18	-------> bind --preset -M $mode \e\[\?1004h false
95	275	------> for mode in (bind --list-modes | string match -v paste)
        bind --preset -M $mode -m paste \e\[200~ '__fish_start_bracketed_paste'
    ...
146	146	-------> bind --list-modes | string match -v paste
34	34	-------> bind --preset -M $mode -m paste \e\[200~ '__fish_start_bracketed_paste'
18	18	------> bind --preset -M paste \e\[201~ '__fish_stop_bracketed_paste'
15	15	------> bind --preset -M paste "" self-insert
15	15	------> bind --preset -M paste \r "commandline -i \n"
16	16	------> bind --preset -M paste "'" "__fish_commandline_insert_escaped \' \$__fish_paste_quoted"
15	15	------> bind --preset -M paste \\ "__fish_commandline_insert_escaped \\\ \$__fish_paste_quoted"
14	14	------> bind --preset -M paste " " self-insert-notfirst
18	18	-----> bind --preset $argv "" self-insert
16	16	-----> bind --preset $argv " " self-insert expand-abbr
15	15	-----> bind --preset $argv \n execute
15	15	-----> bind --preset $argv \r execute
15	15	-----> bind --preset $argv \ck kill-line
20	20	-----> bind --preset $argv \eOC forward-char
26	26	-----> bind --preset $argv \eOD backward-char
16	16	-----> bind --preset $argv \e\[C forward-char
14	14	-----> bind --preset $argv \e\[D backward-char
17	17	-----> bind --preset $argv -k right forward-char
15	15	-----> bind --preset $argv -k left backward-char
17	17	-----> bind --preset $argv -k dc delete-char
27	27	-----> bind --preset $argv -k backspace backward-delete-char
26	26	-----> bind --preset $argv \x7f backward-delete-char
40	40	-----> bind --preset $argv \e\[1~ beginning-of-line
26	26	-----> bind --preset $argv \e\[3~ delete-char
26	26	-----> bind --preset $argv \e\[4~ end-of-line
51	51	-----> bind --preset $argv -k home beginning-of-line 2>/dev/null
41	41	-----> bind --preset $argv -k end end-of-line 2>/dev/null
30	30	-----> bind --preset $argv \e\[3\;2~ backward-delete-char
38	38	-----> bind --preset $argv \ca beginning-of-line
28	28	-----> bind --preset $argv \ce end-of-line
25	25	-----> bind --preset $argv \ch backward-delete-char
25	25	-----> bind --preset $argv \cp up-or-search
26	26	-----> bind --preset $argv \cn down-or-search
28	28	-----> bind --preset $argv \cf forward-char
27	27	-----> bind --preset $argv \cb backward-char
27	27	-----> bind --preset $argv \ct transpose-chars
28	28	-----> bind --preset $argv \et transpose-words
29	29	-----> bind --preset $argv \eu upcase-word
28	28	-----> bind --preset $argv \ec capitalize-word
29	29	-----> bind --preset $argv \e\x7f backward-kill-word
27	27	-----> bind --preset $argv \e\b backward-kill-word
29	29	-----> bind --preset $argv \eb backward-word
27	27	-----> bind --preset $argv \ef forward-word
29	29	-----> bind --preset $argv \e\[1\;5C forward-word
17	17	-----> bind --preset $argv \e\[1\;5D backward-word
15	15	-----> bind --preset $argv \e\< beginning-of-buffer
15	15	-----> bind --preset $argv \e\> end-of-buffer
14	14	-----> bind --preset $argv \ed kill-word
17	17	-----> switch "$TERM"
        case 'rxvt*'
            bind --preset $argv \e\[8~ end-of-line
            bind --preset $argv \eOc forward-word
            bind --preset $argv \eOd backward-word
        case 'xterm-256color'
            # Microsoft's conemu uses xterm-256color plus
            # the following to tell a console to paste:
            bind --preset $argv \e\x20ep fish_clipboard_paste
    ...
6	846	---> if functions --query fish_user_key_bindings >/dev/null
            fish_user_key_bindings 2>/dev/null
        ...
28	28	----> functions --query fish_user_key_bindings >/dev/null
52	812	----> fish_user_key_bindings 2>/dev/null
50	50	-----> test -f $OMF_CONFIG/theme
71	71	-----> read -l theme < $OMF_CONFIG/theme
32	32	-----> test -e $OMF_CONFIG/key_bindings.fish
478	478	-----> set -l key_bindings {$OMF_CONFIG,$OMF_PATH}/pkg/*/key_bindings.fish \
                      {$OMF_CONFIG,$OMF_PATH}/themes*/$theme/key_bindings.fish
17	17	-----> for file in $key_bindings
    source $file
  ...
112	112	-----> functions -q __original_fish_user_key_bindings
9	157	--> if not set -q FISH_UNIT_TESTS_RUNNING
        # Enable bracketed paste before every prompt (see __fish_shared_bindings for the bindings).
        # Disable it for unit tests so we don't have to add the sequences to bind.expect
        function __fish_enable_bracketed_paste --on-event fish_prompt
            printf "\e[?2004h"
        end

        # Disable BP before every command because that might not support it.
        function __fish_disable_bracketed_paste --on-event fish_preexec --on-event fish_exit
            printf "\e[?2004l"
        end

        # Tell the terminal we support BP. Since we are in __f_c_i, the first fish_prompt
        # has already fired.
        __fish_enable_bracketed_paste
    ...
12	12	---> not set -q FISH_UNIT_TESTS_RUNNING
11	11	---> function __fish_enable_bracketed_paste --on-event fish_prompt
            printf "\e[?2004h"
        ...
9	9	---> function __fish_disable_bracketed_paste --on-event fish_preexec --on-event fish_exit
            printf "\e[?2004l"
        ...
19	116	---> __fish_enable_bracketed_paste
97	97	----> printf "\e[?2004h"
4	18	--> if set -q TMUX
        and not set -q FISH_UNIT_TESTS_RUNNING
        function __fish_enable_focus --on-event fish_postexec
            echo -n \e\[\?1004h
        end
        function __fish_disable_focus --on-event fish_preexec
            echo -n \e\[\?1004l
        end
        # Note: Don't call this initially because, even though we're in a fish_prompt event,
        # tmux reacts sooo quickly that we'll still get a sequence before we're prepared for it.
        # So this means that we won't get focus events until you've run at least one command, but that's preferable
        # to always seeing `^[[I` when starting fish.
        # __fish_enable_focus
    ...
14	14	---> set -q TMUX
22	22	--> function __fish_winch_handler --on-signal WINCH -d "Repaint screen when window changes size"
        commandline -f repaint >/dev/null 2>/dev/null
    ...
8	183	--> if test 0"$VTE_VERSION" -ge 3405 -o "$TERM_PROGRAM" = "Apple_Terminal" -a (string match -r '\d+' 0"$TERM_PROGRAM_VERSION") -ge 309
        function __update_cwd_osc --on-variable PWD --description 'Notify capable terminals when $PWD changes'
            if status --is-command-substitution || set -q INSIDE_EMACS
                return
            end
            printf \e\]7\;file://%s%s\a $hostname (string escape --style=url $PWD)
        end
        __update_cwd_osc # Run once because we might have already inherited a PWD from an old tab
    ...
125	175	---> test 0"$VTE_VERSION" -ge 3405 -o "$TERM_PROGRAM" = "Apple_Terminal" -a (string match -r '\d+' 0"$TERM_PROGRAM_VERSION") -ge 309
50	50	----> string match -r '\d+' 0"$TERM_PROGRAM_VERSION"
26	5795	--> if not type -q __fish_command_not_found_handler
        # Read the OS/Distro from /etc/os-release.
        # This has a "ID=" line that defines the exact distribution,
        # and an "ID_LIKE=" line that defines what it is derived from or otherwise like.
        # For our purposes, we use both.
        set -l os
        if test -r /etc/os-release
            set os (string match -r '^ID(?:_LIKE)?\s*=.*' < /etc/os-release | \
            string replace -r '^ID(?:_LIKE)?\s*=(.*)' '$1' | string trim -c '\'"' | string split " ")
        end

        # First check if we are on OpenSUSE since SUSE's handler has no options
        # but the same name and path as Ubuntu's.
        if contains -- suse $os || contains -- sles $os && type -q command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /usr/bin/command-not-found $argv[1]
            end
            # Check for Fedora's handler
        else if test -f /usr/libexec/pk-command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /usr/libexec/pk-command-not-found $argv[1]
            end
            # Check in /usr/lib, this is where modern Ubuntus place this command
        else if test -f /usr/lib/command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /usr/lib/command-not-found -- $argv[1]
            end
            # Check for NixOS handler
        else if test -f /run/current-system/sw/bin/command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /run/current-system/sw/bin/command-not-found $argv
            end
            # Ubuntu Feisty places this command in the regular path instead
        else if type -q command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                command-not-found -- $argv[1]
            end
            # pkgfile is an optional, but official, package on Arch Linux
            # it ships with example handlers for bash and zsh, so we'll follow that format
        else if type -p -q pkgfile
            function __fish_command_not_found_handler --on-event fish_command_not_found
                set -l __packages (pkgfile --binaries --verbose -- $argv[1] 2>/dev/null)
                if test $status -eq 0
                    printf "%s may be found in the following packages:\n" "$argv[1]"
                    printf "  %s\n" $__packages
                else
                    __fish_default_command_not_found_handler $argv[1]
                end
            end
            # Use standard fish command not found handler otherwise
        else
            function __fish_command_not_found_handler --on-event fish_command_not_found
                __fish_default_command_not_found_handler $argv[1]
            end
        end
    ...
388	2904	---> not type -q __fish_command_not_found_handler
1332	1346	----> source /usr/share/fish/functions/type.fish
14	14	-----> function type --description 'Print the type of a command'
    # For legacy reasons, no argument simply causes an unsuccessful return.
    set -q argv[1]
    or return 1

    set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
    argparse -n type -x t,p,P $options -- $argv
    or return

    if set -q _flag_help
        __fish_print_help type
        return 0
    end

    set -l res 1
    set -l mode normal
    set -l multi no
    set -l selection all
    set -l short no

    # Technically all four of these flags are mutually exclusive. However, we allow -q to be used
    # with the other three because old versions of this function explicitly allowed it by making
    # --quiet have precedence.
    if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    end

    set -q _flag_all
    and set multi yes

    set -q _flag_short
    and set short yes

    set -q _flag_no_functions
    and set selection files

    # Check all possible types for the remaining arguments.
    for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    end

    return $res
...
18	18	----> set -q argv[1]
39	39	----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
62	62	----> argparse -n type -x t,p,P $options -- $argv
4	15	----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
11	11	-----> set -q _flag_help
11	11	----> set -l res 1
14	14	----> set -l mode normal
17	17	----> set -l multi no
10	10	----> set -l selection all
8	8	----> set -l short no
7	25	----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
8	8	-----> set -q _flag_quiet
10	10	-----> set mode quiet
12	12	----> set -q _flag_all
10	10	----> set -q _flag_short
6	6	----> set -q _flag_no_functions
34	905	----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
10	10	-----> set -l found 0
11	498	-----> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
20	20	------> test $selection != files
4	163	------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
159	159	-------> functions -q -- $i
6	304	------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
246	298	-------> contains -- $i (builtin -n)
52	52	--------> builtin -n
24	24	-----> set -l paths
11	236	-----> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
25	25	------> test $multi != yes
70	200	------> set paths (command -s -- $i)
130	130	-------> command -s -- $i
59	59	-----> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
6	44	-----> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
25	25	------> test $found = 0
13	13	------> test $mode != quiet
18	18	----> return $res
14	14	---> set -l os
8	479	---> if test -r /etc/os-release
            set os (string match -r '^ID(?:_LIKE)?\s*=.*' < /etc/os-release | \
            string replace -r '^ID(?:_LIKE)?\s*=(.*)' '$1' | string trim -c '\'"' | string split " ")
        ...
16	16	----> test -r /etc/os-release
110	455	----> set os (string match -r '^ID(?:_LIKE)?\s*=.*' < /etc/os-release | \
            string replace -r '^ID(?:_LIKE)?\s*=(.*)' '$1' | string trim -c '\'"' | string split " ")
345	345	-----> string match -r '^ID(?:_LIKE)?\s*=.*' < /etc/os-release | \
            string replace -r '^ID(?:_LIKE)?\s*=(.*)' '$1' | string trim -c '\'"' | string split " "
21	2372	---> if contains -- suse $os || contains -- sles $os && type -q command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /usr/bin/command-not-found $argv[1]
            end
            # Check for Fedora's handler
        else if test -f /usr/libexec/pk-command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /usr/libexec/pk-command-not-found $argv[1]
            end
            # Check in /usr/lib, this is where modern Ubuntus place this command
        else if test -f /usr/lib/command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /usr/lib/command-not-found -- $argv[1]
            end
            # Check for NixOS handler
        else if test -f /run/current-system/sw/bin/command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                /run/current-system/sw/bin/command-not-found $argv
            end
            # Ubuntu Feisty places this command in the regular path instead
        else if type -q command-not-found
            function __fish_command_not_found_handler --on-event fish_command_not_found
                command-not-found -- $argv[1]
            end
            # pkgfile is an optional, but official, package on Arch Linux
            # it ships with example handlers for bash and zsh, so we'll follow that format
        else if type -p -q pkgfile
            function __fish_command_not_found_handler --on-event fish_command_not_found
                set -l __packages (pkgfile --binaries --verbose -- $argv[1] 2>/dev/null)
                if test $status -eq 0
                    printf "%s may be found in the following packages:\n" "$argv[1]"
                    printf "  %s\n" $__packages
                else
                    __fish_default_command_not_found_handler $argv[1]
                end
            end
            # Use standard fish command not found handler otherwise
        else
            function __fish_command_not_found_handler --on-event fish_command_not_found
                __fish_default_command_not_found_handler $argv[1]
            end
        ...
26	26	----> contains -- suse $os
22	22	----> contains -- sles $os
14	14	----> test -f /usr/libexec/pk-command-not-found
9	9	----> test -f /usr/lib/command-not-found
15	15	----> test -f /run/current-system/sw/bin/command-not-found
84	1099	----> type -q command-not-found
13	13	-----> set -q argv[1]
28	28	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
56	56	-----> argparse -n type -x t,p,P $options -- $argv
4	17	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
13	13	------> set -q _flag_help
17	17	-----> set -l res 1
10	10	-----> set -l mode normal
9	9	-----> set -l multi no
9	9	-----> set -l selection all
9	9	-----> set -l short no
10	33	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
11	11	------> set -q _flag_quiet
12	12	------> set mode quiet
7	7	-----> set -q _flag_all
6	6	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
39	778	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
19	19	------> set -l found 0
18	438	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
14	14	-------> test $selection != files
4	131	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
127	127	--------> functions -q -- $i
4	275	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
224	271	--------> contains -- $i (builtin -n)
47	47	---------> builtin -n
16	16	------> set -l paths
10	213	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
21	21	-------> test $multi != yes
81	182	-------> set paths (command -s -- $i)
101	101	--------> command -s -- $i
18	18	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
4	35	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
19	19	-------> test $found = 0
12	12	-------> test $mode != quiet
17	17	-----> return $res
76	1155	----> type -p -q pkgfile
11	11	-----> set -q argv[1]
37	37	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
61	61	-----> argparse -n type -x t,p,P $options -- $argv
3	15	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
12	12	------> set -q _flag_help
11	11	-----> set -l res 1
9	9	-----> set -l mode normal
9	9	-----> set -l multi no
16	16	-----> set -l selection all
13	13	-----> set -l short no
6	22	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	------> set -q _flag_quiet
9	9	------> set mode quiet
7	7	-----> set -q _flag_all
7	7	-----> set -q _flag_short
10	10	-----> set -q _flag_no_functions
38	841	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
11	11	------> set -l found 0
17	467	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
13	13	-------> test $selection != files
3	121	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
118	118	--------> functions -q -- $i
5	316	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
266	311	--------> contains -- $i (builtin -n)
45	45	---------> builtin -n
18	18	------> set -l paths
9	193	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	-------> test $multi != yes
106	170	-------> set paths (command -s -- $i)
64	64	--------> command -s -- $i
29	102	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
20	20	-------> set res 0
14	14	-------> set found 1
15	15	-------> switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            ...
5	24	-------> if test $multi != yes
                continue
            ...
13	13	--------> test $multi != yes
6	6	--------> continue
3	12	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
9	9	-------> test $found = 0
10	10	-----> return $res
11	11	----> function __fish_command_not_found_handler --on-event fish_command_not_found
                set -l __packages (pkgfile --binaries --verbose -- $argv[1] 2>/dev/null)
                if test $status -eq 0
                    printf "%s may be found in the following packages:\n" "$argv[1]"
                    printf "  %s\n" $__packages
                else
                    __fish_default_command_not_found_handler $argv[1]
                end
            ...
23	23	--> set __fish_initialized 3100
19	19	-> functions -e __fish_on_interactive
104	111	> source /home/micro-p/.local/share/omf/themes/spacefish/fish_mode_prompt.fish
7	7	-> function fish_mode_prompt
	# Overriden by Spacefish fishshell theme
	# To see vi mode in prompt add 'vi_mode' to SPACEFISH_PROMPT_ORDER
...
25	25	> fish_mode_prompt
43	107864	> fish_prompt
24	24	-> set -g sf_exit_code $status
13	13	-> set -g SPACEFISH_VERSION 2.7.0
193	345	-> __sf_util_set_default SPACEFISH_PROMPT_ADD_NEWLINE false
89	103	--> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_util_set_default.fish
14	14	---> function __sf_util_set_default -a var -d "Set the default value for a global variable"
	if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	end
...
9	49	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	---> not set -q $var
21	21	---> set -g $var $argv[2..-1]
25	62	-> __sf_util_set_default SPACEFISH_PROMPT_FIRST_PREFIX_SHOW false
7	37	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	---> not set -q $var
18	18	---> set -g $var $argv[2..-1]
23	56	-> __sf_util_set_default SPACEFISH_PROMPT_PREFIXES_SHOW true
6	33	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	---> not set -q $var
16	16	---> set -g $var $argv[2..-1]
20	51	-> __sf_util_set_default SPACEFISH_PROMPT_SUFFIXES_SHOW true
6	31	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	---> not set -q $var
15	15	---> set -g $var $argv[2..-1]
22	52	-> __sf_util_set_default SPACEFISH_PROMPT_DEFAULT_PREFIX "via "
6	30	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	---> not set -q $var
15	15	---> set -g $var $argv[2..-1]
23	53	-> __sf_util_set_default SPACEFISH_PROMPT_DEFAULT_SUFFIX " "
4	30	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	---> not set -q $var
16	16	---> set -g $var $argv[2..-1]
47	116	-> __sf_util_set_default SPACEFISH_PROMPT_ORDER time user dir host git package node ruby golang php rust haskell julia elixir docker aws venv conda pyenv dotnet kubecontext exec_time line_sep battery vi_mode jobs exit_code char
7	69	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	---> not set -q $var
51	51	---> set -g $var $argv[2..-1]
17	17	-> set -g sf_prompt_opened $SPACEFISH_PROMPT_FIRST_PREFIX_SHOW
3	19	-> if test "$SPACEFISH_PROMPT_ADD_NEWLINE" = "true"
		echo
	...
16	16	--> test "$SPACEFISH_PROMPT_ADD_NEWLINE" = "true"
236	106998	-> for i in $SPACEFISH_PROMPT_ORDER
		eval __sf_section_$i
	...
100	973	--> eval __sf_section_$i
203	873	---> __sf_section_time
241	253	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_time.fish
12	12	-----> function __sf_section_time -d "Display the current time!"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_TIME_SHOW false
	__sf_util_set_default SPACEFISH_DATE_SHOW false
	__sf_util_set_default SPACEFISH_TIME_PREFIX "at "
	__sf_util_set_default SPACEFISH_TIME_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_TIME_FORMAT false
	__sf_util_set_default SPACEFISH_TIME_12HR false
	__sf_util_set_default SPACEFISH_TIME_COLOR "yellow"

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_TIME_SHOW = false ]; and return

	set -l time_str

	if test $SPACEFISH_DATE_SHOW = true
		set time_str (date '+%Y-%m-%d')" "
	end

	if not test $SPACEFISH_TIME_FORMAT = false
		set time_str "$time_str"(date '+'$SPACEFISH_TIME_FORMAT)
	else if test $SPACEFISH_TIME_12HR = true
		set time_str "$time_str"(date '+%I:%M:%S') # Fish doesn't seem to have date/time formatting.
	else
		set time_str "$time_str"(date '+%H:%M:%S')
	end

	__sf_lib_section \
		$SPACEFISH_TIME_COLOR \
		$SPACEFISH_TIME_PREFIX \
		$time_str \
		$SPACEFISH_TIME_SUFFIX
...
30	78	----> __sf_util_set_default SPACEFISH_TIME_SHOW false
9	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
22	22	------> set -g $var $argv[2..-1]
24	59	----> __sf_util_set_default SPACEFISH_DATE_SHOW false
6	35	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
24	56	----> __sf_util_set_default SPACEFISH_TIME_PREFIX "at "
5	32	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
25	56	----> __sf_util_set_default SPACEFISH_TIME_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
20	50	----> __sf_util_set_default SPACEFISH_TIME_FORMAT false
5	30	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
19	48	----> __sf_util_set_default SPACEFISH_TIME_12HR false
6	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
14	14	------> set -g $var $argv[2..-1]
21	50	----> __sf_util_set_default SPACEFISH_TIME_COLOR "yellow"
6	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
14	14	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_TIME_SHOW = false ]
6	6	----> return
27	865	--> eval __sf_section_$i
220	838	---> __sf_section_user
241	254	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_user.fish
13	13	-----> function __sf_section_user -d "Display the username"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	# --------------------------------------------------------------------------
	# | SPACEFISH_USER_SHOW | show username on local | show username on remote |
	# |---------------------+------------------------+-------------------------|
	# | false               | never                  | never                   |
	# | always              | always                 | always                  |
	# | true                | if needed              | always                  |
	# | needed              | if needed              | if needed               |
	# --------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_USER_SHOW true
	__sf_util_set_default SPACEFISH_USER_PREFIX "with "
	__sf_util_set_default SPACEFISH_USER_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_USER_COLOR yellow
	__sf_util_set_default SPACEFISH_USER_COLOR_ROOT red

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_USER_SHOW = false ]; and return

	if test "$SPACEFISH_USER_SHOW" = "always" \
	-o "$LOGNAME" != "$USER" \
	-o "$UID" = "0" \
	-o \( "$SPACEFISH_USER_SHOW" = "true" -a -n "$SSH_CONNECTION" \)

		set -l user_color
		if test "$USER" = "root"
			set user_color $SPACEFISH_USER_COLOR_ROOT
		else
			set user_color $SPACEFISH_USER_COLOR
		end

		__sf_lib_section \
			$user_color \
			$SPACEFISH_USER_PREFIX \
			$USER \
			$SPACEFISH_USER_SUFFIX
	end
...
31	78	----> __sf_util_set_default SPACEFISH_USER_SHOW true
9	47	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
24	58	----> __sf_util_set_default SPACEFISH_USER_PREFIX "with "
5	34	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
24	56	----> __sf_util_set_default SPACEFISH_USER_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
6	32	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
20	50	----> __sf_util_set_default SPACEFISH_USER_COLOR yellow
6	30	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
21	52	----> __sf_util_set_default SPACEFISH_USER_COLOR_ROOT red
5	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_USER_SHOW = false ]
3	56	----> if test "$SPACEFISH_USER_SHOW" = "always" \
	-o "$LOGNAME" != "$USER" \
	-o "$UID" = "0" \
	-o \( "$SPACEFISH_USER_SHOW" = "true" -a -n "$SSH_CONNECTION" \)

		set -l user_color
		if test "$USER" = "root"
			set user_color $SPACEFISH_USER_COLOR_ROOT
		else
			set user_color $SPACEFISH_USER_COLOR
		end

		__sf_lib_section \
			$user_color \
			$SPACEFISH_USER_PREFIX \
			$USER \
			$SPACEFISH_USER_SUFFIX
	...
53	53	-----> test "$SPACEFISH_USER_SHOW" = "always" \
	-o "$LOGNAME" != "$USER" \
	-o "$UID" = "0" \
	-o \( "$SPACEFISH_USER_SHOW" = "true" -a -n "$SSH_CONNECTION" \)
32	5344	--> eval __sf_section_$i
221	5312	---> __sf_section_dir
346	359	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_dir.fish
13	13	-----> function __sf_section_dir -d "Display the current truncated directory"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_DIR_SHOW true
	__sf_util_set_default SPACEFISH_DIR_PREFIX "in "
	__sf_util_set_default SPACEFISH_DIR_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_DIR_TRUNC 3
	__sf_util_set_default SPACEFISH_DIR_TRUNC_REPO true
	__sf_util_set_default SPACEFISH_DIR_COLOR cyan

	# Write Permissions lock symbol
	__sf_util_set_default SPACEFISH_DIR_LOCK_SHOW true
	__sf_util_set_default SPACEFISH_DIR_LOCK_SYMBOL ""
	__sf_util_set_default SPACEFISH_DIR_LOCK_COLOR red

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_DIR_SHOW = false ]; and return

	set -l dir
	set -l tmp
	set -l git_root (command git rev-parse --show-toplevel 2>/dev/null)

	if test "$SPACEFISH_DIR_TRUNC_REPO" = "true" -a -n "$git_root"
		# Resolve to physical PWD instead of logical
		set -l resolvedPWD (pwd -P 2>/dev/null; or pwd)
		# Treat repo root as top level directory
		set tmp (string replace $git_root (basename $git_root) $resolvedPWD)
	else
		set -l realhome ~
		set tmp (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD)
	end

	# Truncate the path to have a limited number of dirs
	set dir (__sf_util_truncate_dir $tmp $SPACEFISH_DIR_TRUNC)

	if [ $SPACEFISH_DIR_LOCK_SHOW = true -a ! -w . ]
		set DIR_LOCK_SYMBOL (set_color $SPACEFISH_DIR_LOCK_COLOR)" $SPACEFISH_DIR_LOCK_SYMBOL"(set_color --bold)
	end

	__sf_lib_section \
		$SPACEFISH_DIR_COLOR \
		$SPACEFISH_DIR_PREFIX \
		$dir \
		"$DIR_LOCK_SYMBOL""$SPACEFISH_DIR_SUFFIX"
...
31	78	----> __sf_util_set_default SPACEFISH_DIR_SHOW true
8	47	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
21	21	------> set -g $var $argv[2..-1]
26	61	----> __sf_util_set_default SPACEFISH_DIR_PREFIX "in "
5	35	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
26	57	----> __sf_util_set_default SPACEFISH_DIR_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
20	50	----> __sf_util_set_default SPACEFISH_DIR_TRUNC 3
5	30	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
19	51	----> __sf_util_set_default SPACEFISH_DIR_TRUNC_REPO true
6	32	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
19	48	----> __sf_util_set_default SPACEFISH_DIR_COLOR cyan
5	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
14	14	------> set -g $var $argv[2..-1]
19	48	----> __sf_util_set_default SPACEFISH_DIR_LOCK_SHOW true
5	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
14	14	------> set -g $var $argv[2..-1]
22	51	----> __sf_util_set_default SPACEFISH_DIR_LOCK_SYMBOL ""
6	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
14	14	------> set -g $var $argv[2..-1]
19	48	----> __sf_util_set_default SPACEFISH_DIR_LOCK_COLOR red
6	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
14	14	------> set -g $var $argv[2..-1]
13	13	----> [ $SPACEFISH_DIR_SHOW = false ]
9	9	----> set -l dir
8	8	----> set -l tmp
116	1510	----> set -l git_root (command git rev-parse --show-toplevel 2>/dev/null)
1394	1394	-----> command git rev-parse --show-toplevel 2>/dev/null
17	256	----> if test "$SPACEFISH_DIR_TRUNC_REPO" = "true" -a -n "$git_root"
		# Resolve to physical PWD instead of logical
		set -l resolvedPWD (pwd -P 2>/dev/null; or pwd)
		# Treat repo root as top level directory
		set tmp (string replace $git_root (basename $git_root) $resolvedPWD)
	else
		set -l realhome ~
		set tmp (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD)
	...
34	34	-----> test "$SPACEFISH_DIR_TRUNC_REPO" = "true" -a -n "$git_root"
26	26	-----> set -l realhome ~
116	179	-----> set tmp (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD)
63	63	------> string replace -r '^'"$realhome"'($|/)' '~$1' $PWD
117	1391	----> set dir (__sf_util_truncate_dir $tmp $SPACEFISH_DIR_TRUNC)
242	1274	-----> __sf_util_truncate_dir $tmp $SPACEFISH_DIR_TRUNC
198	219	------> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_util_truncate_dir.fish
21	21	-------> function __sf_util_truncate_dir -a path truncate_to -d "Truncate a directory path"
	if test "$truncate_to" -eq 0
		echo $path
	else
		set -l folders (string split / $path)

		if test (count $folders) -le "$truncate_to"
			echo $path
		else
			echo (string join / $folders[(math 0 - $truncate_to)..-1])
		end
	end
...
17	813	------> if test "$truncate_to" -eq 0
		echo $path
	else
		set -l folders (string split / $path)

		if test (count $folders) -le "$truncate_to"
			echo $path
		else
			echo (string join / $folders[(math 0 - $truncate_to)..-1])
		end
	...
23	23	-------> test "$truncate_to" -eq 0
112	154	-------> set -l folders (string split / $path)
42	42	--------> string split / $path
11	619	-------> if test (count $folders) -le "$truncate_to"
			echo $path
		else
			echo (string join / $folders[(math 0 - $truncate_to)..-1])
		...
104	236	--------> test (count $folders) -le "$truncate_to"
132	132	---------> count $folders
111	372	--------> echo (string join / $folders[(math 0 - $truncate_to)..-1])
125	261	---------> string join / $folders[(math 0 - $truncate_to)..-1]
136	136	----------> math 0 - $truncate_to
5	41	----> if [ $SPACEFISH_DIR_LOCK_SHOW = true -a ! -w . ]
		set DIR_LOCK_SYMBOL (set_color $SPACEFISH_DIR_LOCK_COLOR)" $SPACEFISH_DIR_LOCK_SYMBOL"(set_color --bold)
	...
36	36	-----> [ $SPACEFISH_DIR_LOCK_SHOW = true -a ! -w . ]
281	1012	----> __sf_lib_section \
		$SPACEFISH_DIR_COLOR \
		$SPACEFISH_DIR_PREFIX \
		$dir \
		"$DIR_LOCK_SYMBOL""$SPACEFISH_DIR_SUFFIX"
218	230	-----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_lib_section.fish
12	12	------> function __sf_lib_section -a color prefix content suffix
	# If there are only 2 args, they are $content and $prefix
	if test (count $argv) -eq 2
		set content $argv[2]
		set prefix
	end

	if test "$sf_prompt_opened" = "true" -a "$SPACEFISH_PROMPT_PREFIXES_SHOW" = "true"
		# Echo prefixes in bold white
		set_color --bold
		echo -e -n -s $prefix
		set_color normal
	end

	# Set the prompt as having been opened
	set -g sf_prompt_opened true

	set_color --bold $color
	echo -e -n $content
	set_color normal

	if test "$SPACEFISH_PROMPT_SUFFIXES_SHOW" = "true"
		# Echo suffixes in bold white
		set_color --bold
		echo -e -n -s $suffix
		set_color normal
	end
...
5	155	-----> if test (count $argv) -eq 2
		set content $argv[2]
		set prefix
	...
106	150	------> test (count $argv) -eq 2
44	44	-------> count $argv
5	34	-----> if test "$sf_prompt_opened" = "true" -a "$SPACEFISH_PROMPT_PREFIXES_SHOW" = "true"
		# Echo prefixes in bold white
		set_color --bold
		echo -e -n -s $prefix
		set_color normal
	...
29	29	------> test "$sf_prompt_opened" = "true" -a "$SPACEFISH_PROMPT_PREFIXES_SHOW" = "true"
24	24	-----> set -g sf_prompt_opened true
132	132	-----> set_color --bold $color
25	25	-----> echo -e -n $content
16	16	-----> set_color normal
18	115	-----> if test "$SPACEFISH_PROMPT_SUFFIXES_SHOW" = "true"
		# Echo suffixes in bold white
		set_color --bold
		echo -e -n -s $suffix
		set_color normal
	...
24	24	------> test "$SPACEFISH_PROMPT_SUFFIXES_SHOW" = "true"
32	32	------> set_color --bold
27	27	------> echo -e -n -s $suffix
14	14	------> set_color normal
54	1496	--> eval __sf_section_$i
634	1442	---> __sf_section_host
288	303	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_host.fish
15	15	-----> function __sf_section_host -d "Display the current hostname if connected over SSH"

	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_HOST_SHOW true
	__sf_util_set_default SPACEFISH_HOST_PREFIX "at "
	__sf_util_set_default SPACEFISH_HOST_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_HOST_COLOR blue
	__sf_util_set_default SPACEFISH_HOST_COLOR_SSH green

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ "$SPACEFISH_HOST_SHOW" = false ]; and return

	if test "$SPACEFISH_HOST_SHOW" = "always"; or set -q SSH_CONNECTION;

		# Determination of what color should be used
		set -l host_color
		if set -q SSH_CONNECTION;
			set host_color $SPACEFISH_HOST_COLOR_SSH
		else
			set host_color $SPACEFISH_HOST_COLOR
		end

		__sf_lib_section \
			$host_color \
			$SPACEFISH_HOST_PREFIX \
			(hostname) \
			$SPACEFISH_HOST_SUFFIX
		end
...
48	108	----> __sf_util_set_default SPACEFISH_HOST_SHOW true
11	60	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
24	24	------> not set -q $var
25	25	------> set -g $var $argv[2..-1]
32	76	----> __sf_util_set_default SPACEFISH_HOST_PREFIX "at "
8	44	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
24	24	------> set -g $var $argv[2..-1]
42	91	----> __sf_util_set_default SPACEFISH_HOST_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
10	49	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
24	24	------> set -g $var $argv[2..-1]
43	92	----> __sf_util_set_default SPACEFISH_HOST_COLOR blue
13	49	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
34	82	----> __sf_util_set_default SPACEFISH_HOST_COLOR_SSH green
12	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
24	24	----> [ "$SPACEFISH_HOST_SHOW" = false ]
5	32	----> if test "$SPACEFISH_HOST_SHOW" = "always"; or set -q SSH_CONNECTION;

		# Determination of what color should be used
		set -l host_color
		if set -q SSH_CONNECTION;
			set host_color $SPACEFISH_HOST_COLOR_SSH
		else
			set host_color $SPACEFISH_HOST_COLOR
		end

		__sf_lib_section \
			$host_color \
			$SPACEFISH_HOST_PREFIX \
			(hostname) \
			$SPACEFISH_HOST_SUFFIX
		...
16	16	-----> test "$SPACEFISH_HOST_SHOW" = "always"
11	11	-----> set -q SSH_CONNECTION
56	13224	--> eval __sf_section_$i
254	13168	---> __sf_section_git
188	214	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_git.fish
26	26	-----> function __sf_section_git -d "Display the git branch and status"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_GIT_SHOW true
	__sf_util_set_default SPACEFISH_GIT_PREFIX "on "
	__sf_util_set_default SPACEFISH_GIT_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_GIT_SYMBOL " "

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show both git branch and git status:
	#   spacefish_git_branch
	#   spacefish_git_status

	[ $SPACEFISH_GIT_SHOW = false ]; and return

	set -l git_branch (__sf_section_git_branch)
	set -l git_status (__sf_section_git_status)

	[ -z $git_branch ]; and return

	__sf_lib_section \
		fff \
		$SPACEFISH_GIT_PREFIX \
		"$git_branch$git_status" \
		$SPACEFISH_GIT_SUFFIX
...
37	88	----> __sf_util_set_default SPACEFISH_GIT_SHOW true
10	51	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
22	22	------> set -g $var $argv[2..-1]
27	62	----> __sf_util_set_default SPACEFISH_GIT_PREFIX "on "
6	35	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
27	59	----> __sf_util_set_default SPACEFISH_GIT_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
6	32	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
22	53	----> __sf_util_set_default SPACEFISH_GIT_SYMBOL " "
5	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_GIT_SHOW = false ]
99	3938	----> set -l git_branch (__sf_section_git_branch)
410	3839	-----> __sf_section_git_branch
430	456	------> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_git_branch.fish
26	26	-------> function __sf_section_git_branch -d "Format the displayed branch name"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_GIT_BRANCH_SHOW true
	__sf_util_set_default SPACEFISH_GIT_BRANCH_PREFIX $SPACEFISH_GIT_SYMBOL
	__sf_util_set_default SPACEFISH_GIT_BRANCH_SUFFIX ""
	__sf_util_set_default SPACEFISH_GIT_BRANCH_COLOR magenta

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_GIT_BRANCH_SHOW = false ]; and return

	set -l git_branch (__sf_util_git_branch)

	[ -z $git_branch ]; and return

	__sf_lib_section \
		$SPACEFISH_GIT_BRANCH_COLOR \
		$SPACEFISH_GIT_BRANCH_PREFIX$git_branch$SPACEFISH_GIT_BRANCH_SUFFIX
...
68	174	------> __sf_util_set_default SPACEFISH_GIT_BRANCH_SHOW true
22	106	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
47	47	--------> not set -q $var
37	37	--------> set -g $var $argv[2..-1]
59	119	------> __sf_util_set_default SPACEFISH_GIT_BRANCH_PREFIX $SPACEFISH_GIT_SYMBOL
14	60	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	--------> not set -q $var
26	26	--------> set -g $var $argv[2..-1]
45	102	------> __sf_util_set_default SPACEFISH_GIT_BRANCH_SUFFIX ""
11	57	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	--------> not set -q $var
26	26	--------> set -g $var $argv[2..-1]
47	102	------> __sf_util_set_default SPACEFISH_GIT_BRANCH_COLOR magenta
12	55	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	--------> not set -q $var
24	24	--------> set -g $var $argv[2..-1]
22	22	------> [ $SPACEFISH_GIT_BRANCH_SHOW = false ]
220	2428	------> set -l git_branch (__sf_util_git_branch)
222	2208	-------> __sf_util_git_branch
92	105	--------> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_util_git_branch.fish
13	13	---------> function __sf_util_git_branch -d "Display the current branch name"
	echo (command git rev-parse --abbrev-ref HEAD 2>/dev/null)
...
140	1881	--------> echo (command git rev-parse --abbrev-ref HEAD 2>/dev/null)
1741	1741	---------> command git rev-parse --abbrev-ref HEAD 2>/dev/null
19	19	------> [ -z $git_branch ]
7	7	------> return
120	8462	----> set -l git_status (__sf_section_git_status)
246	8342	-----> __sf_section_git_status
661	676	------> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_git_status.fish
15	15	-------> function __sf_section_git_status -d "Display the current git status"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_GIT_STATUS_SHOW true
	__sf_util_set_default SPACEFISH_GIT_STATUS_PREFIX " ["
	__sf_util_set_default SPACEFISH_GIT_STATUS_SUFFIX ]
	__sf_util_set_default SPACEFISH_GIT_STATUS_COLOR red
	__sf_util_set_default SPACEFISH_GIT_STATUS_UNTRACKED \?
	__sf_util_set_default SPACEFISH_GIT_STATUS_ADDED +
	__sf_util_set_default SPACEFISH_GIT_STATUS_MODIFIED !
	__sf_util_set_default SPACEFISH_GIT_STATUS_RENAMED »
	__sf_util_set_default SPACEFISH_GIT_STATUS_DELETED ✘
	__sf_util_set_default SPACEFISH_GIT_STATUS_STASHED \$
	__sf_util_set_default SPACEFISH_GIT_STATUS_UNMERGED =
	__sf_util_set_default SPACEFISH_GIT_STATUS_AHEAD ⇡
	__sf_util_set_default SPACEFISH_GIT_STATUS_BEHIND ⇣
	__sf_util_set_default SPACEFISH_GIT_STATUS_DIVERGED ⇕
	__sf_util_set_default SPACEFISH_GIT_PROMPT_ORDER untracked added modified renamed deleted stashed unmerged diverged ahead behind

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_GIT_STATUS_SHOW = false ]; and return

	set -l git_status
	set -l is_ahead
	set -l is_behind

	set -l index (command git status --porcelain 2>/dev/null -b)
	set -l trimmed_index (string split \n $index | string sub --start 1 --length 2)

	for i in $trimmed_index
		if test (string match '\?\?' $i)
			set git_status untracked $git_status
		end
		if test (string match '*A*' $i)
			set git_status added $git_status
		end
		if test (string match '*M*' $i)
			set git_status modified $git_status
		end
		if test (string match '*R*' $i)
			set git_status renamed $git_status
		end
		if test (string match '*D*' $i)
			set git_status deleted $git_status
		end
		if test (string match '*U*' $i)
			set git_status unmerged $git_status
		end
	end

	# Check for stashes
	if test -n (echo (command git rev-parse --verify refs/stash 2>/dev/null))
		set git_status stashed $git_status
	end

	# Check whether the branch is ahead
	if test (string match '*ahead*' $index)
		set is_ahead true
	end

	# Check whether the branch is behind
	if test (string match '*behind*' $index)
		set is_behind true
	end

	# Check whether the branch has diverged
	if test "$is_ahead" = "true" -a "$is_behind" = "true"
		set git_status diverged $git_status
	else if test "$is_ahead" = "true"
		set git_status ahead $git_status
	else if test "$is_behind" = "true"
		set git_status behind $git_status
	end

	set -l full_git_status
	for i in $SPACEFISH_GIT_PROMPT_ORDER
		set i (string upper $i)
		set git_status (string upper $git_status)
		if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		end
	end

	# Check if git status
	if test -n "$full_git_status"
		__sf_lib_section \
			$SPACEFISH_GIT_STATUS_COLOR \
			"$SPACEFISH_GIT_STATUS_PREFIX$full_git_status$SPACEFISH_GIT_STATUS_SUFFIX"
	end
...
33	85	------> __sf_util_set_default SPACEFISH_GIT_STATUS_SHOW true
10	52	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	--------> not set -q $var
22	22	--------> set -g $var $argv[2..-1]
28	66	------> __sf_util_set_default SPACEFISH_GIT_STATUS_PREFIX " ["
8	38	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	--------> not set -q $var
18	18	--------> set -g $var $argv[2..-1]
23	57	------> __sf_util_set_default SPACEFISH_GIT_STATUS_SUFFIX ]
6	34	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	--------> not set -q $var
17	17	--------> set -g $var $argv[2..-1]
20	52	------> __sf_util_set_default SPACEFISH_GIT_STATUS_COLOR red
6	32	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	--------> not set -q $var
16	16	--------> set -g $var $argv[2..-1]
23	53	------> __sf_util_set_default SPACEFISH_GIT_STATUS_UNTRACKED \?
6	30	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	--------> not set -q $var
14	14	--------> set -g $var $argv[2..-1]
21	50	------> __sf_util_set_default SPACEFISH_GIT_STATUS_ADDED +
5	29	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	--------> not set -q $var
15	15	--------> set -g $var $argv[2..-1]
19	49	------> __sf_util_set_default SPACEFISH_GIT_STATUS_MODIFIED !
6	30	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	--------> not set -q $var
15	15	--------> set -g $var $argv[2..-1]
18	47	------> __sf_util_set_default SPACEFISH_GIT_STATUS_RENAMED »
6	29	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	--------> not set -q $var
14	14	--------> set -g $var $argv[2..-1]
19	46	------> __sf_util_set_default SPACEFISH_GIT_STATUS_DELETED ✘
4	27	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	--------> not set -q $var
14	14	--------> set -g $var $argv[2..-1]
21	49	------> __sf_util_set_default SPACEFISH_GIT_STATUS_STASHED \$
5	28	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	--------> not set -q $var
14	14	--------> set -g $var $argv[2..-1]
19	47	------> __sf_util_set_default SPACEFISH_GIT_STATUS_UNMERGED =
5	28	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	--------> not set -q $var
14	14	--------> set -g $var $argv[2..-1]
19	48	------> __sf_util_set_default SPACEFISH_GIT_STATUS_AHEAD ⇡
5	29	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	--------> not set -q $var
14	14	--------> set -g $var $argv[2..-1]
20	49	------> __sf_util_set_default SPACEFISH_GIT_STATUS_BEHIND ⇣
5	29	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	--------> not set -q $var
14	14	--------> set -g $var $argv[2..-1]
19	70	------> __sf_util_set_default SPACEFISH_GIT_STATUS_DIVERGED ⇕
7	51	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	--------> not set -q $var
35	35	--------> set -g $var $argv[2..-1]
34	80	------> __sf_util_set_default SPACEFISH_GIT_PROMPT_ORDER untracked added modified renamed deleted stashed unmerged diverged ahead behind
5	46	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	--------> not set -q $var
30	30	--------> set -g $var $argv[2..-1]
15	15	------> [ $SPACEFISH_GIT_STATUS_SHOW = false ]
10	10	------> set -l git_status
8	8	------> set -l is_ahead
8	8	------> set -l is_behind
133	1493	------> set -l index (command git status --porcelain 2>/dev/null -b)
1360	1360	-------> command git status --porcelain 2>/dev/null -b
116	180	------> set -l trimmed_index (string split \n $index | string sub --start 1 --length 2)
64	64	-------> string split \n $index | string sub --start 1 --length 2
15	15	------> for i in $trimmed_index
		if test (string match '\?\?' $i)
			set git_status untracked $git_status
		end
		if test (string match '*A*' $i)
			set git_status added $git_status
		end
		if test (string match '*M*' $i)
			set git_status modified $git_status
		end
		if test (string match '*R*' $i)
			set git_status renamed $git_status
		end
		if test (string match '*D*' $i)
			set git_status deleted $git_status
		end
		if test (string match '*U*' $i)
			set git_status unmerged $git_status
		end
	...
6	1527	------> if test -n (echo (command git rev-parse --verify refs/stash 2>/dev/null))
		set git_status stashed $git_status
	...
119	1521	-------> test -n (echo (command git rev-parse --verify refs/stash 2>/dev/null))
126	1402	--------> echo (command git rev-parse --verify refs/stash 2>/dev/null)
1276	1276	---------> command git rev-parse --verify refs/stash 2>/dev/null
6	135	------> if test (string match '*ahead*' $index)
		set is_ahead true
	...
99	129	-------> test (string match '*ahead*' $index)
30	30	--------> string match '*ahead*' $index
6	134	------> if test (string match '*behind*' $index)
		set is_behind true
	...
96	128	-------> test (string match '*behind*' $index)
32	32	--------> string match '*behind*' $index
9	70	------> if test "$is_ahead" = "true" -a "$is_behind" = "true"
		set git_status diverged $git_status
	else if test "$is_ahead" = "true"
		set git_status ahead $git_status
	else if test "$is_behind" = "true"
		set git_status behind $git_status
	...
27	27	-------> test "$is_ahead" = "true" -a "$is_behind" = "true"
20	20	-------> test "$is_ahead" = "true"
14	14	-------> test "$is_behind" = "true"
16	16	------> set -l full_git_status
170	2935	------> for i in $SPACEFISH_GIT_PROMPT_ORDER
		set i (string upper $i)
		set git_status (string upper $git_status)
		if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		end
	...
103	142	-------> set i (string upper $i)
39	39	--------> string upper $i
113	156	-------> set git_status (string upper $git_status)
43	43	--------> string upper $git_status
4	34	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
30	30	--------> contains $i in $git_status
101	129	-------> set i (string upper $i)
28	28	--------> string upper $i
83	106	-------> set git_status (string upper $git_status)
23	23	--------> string upper $git_status
4	25	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
21	21	--------> contains $i in $git_status
93	119	-------> set i (string upper $i)
26	26	--------> string upper $i
80	103	-------> set git_status (string upper $git_status)
23	23	--------> string upper $git_status
3	22	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
19	19	--------> contains $i in $git_status
81	105	-------> set i (string upper $i)
24	24	--------> string upper $i
88	110	-------> set git_status (string upper $git_status)
22	22	--------> string upper $git_status
4	22	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
18	18	--------> contains $i in $git_status
105	128	-------> set i (string upper $i)
23	23	--------> string upper $i
88	120	-------> set git_status (string upper $git_status)
32	32	--------> string upper $git_status
4	36	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
32	32	--------> contains $i in $git_status
97	128	-------> set i (string upper $i)
31	31	--------> string upper $i
85	118	-------> set git_status (string upper $git_status)
33	33	--------> string upper $git_status
6	35	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
29	29	--------> contains $i in $git_status
91	125	-------> set i (string upper $i)
34	34	--------> string upper $i
92	121	-------> set git_status (string upper $git_status)
29	29	--------> string upper $git_status
6	29	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
23	23	--------> contains $i in $git_status
93	129	-------> set i (string upper $i)
36	36	--------> string upper $i
97	147	-------> set git_status (string upper $git_status)
50	50	--------> string upper $git_status
5	37	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
32	32	--------> contains $i in $git_status
94	124	-------> set i (string upper $i)
30	30	--------> string upper $i
85	116	-------> set git_status (string upper $git_status)
31	31	--------> string upper $git_status
5	31	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
26	26	--------> contains $i in $git_status
85	119	-------> set i (string upper $i)
34	34	--------> string upper $i
96	126	-------> set git_status (string upper $git_status)
30	30	--------> string upper $git_status
4	23	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
19	19	--------> contains $i in $git_status
5	26	------> if test -n "$full_git_status"
		__sf_lib_section \
			$SPACEFISH_GIT_STATUS_COLOR \
			"$SPACEFISH_GIT_STATUS_PREFIX$full_git_status$SPACEFISH_GIT_STATUS_SUFFIX"
	...
21	21	-------> test -n "$full_git_status"
18	18	----> [ -z $git_branch ]
6	6	----> return
32	976	--> eval __sf_section_$i
187	944	---> __sf_section_package
382	396	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_package.fish
14	14	-----> function __sf_section_package -d "Display the local package version"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_PACKAGE_SHOW true
	__sf_util_set_default SPACEFISH_PACKAGE_PREFIX "is "
	__sf_util_set_default SPACEFISH_PACKAGE_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_PACKAGE_SYMBOL "📦 "
	__sf_util_set_default SPACEFISH_PACKAGE_COLOR red

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_PACKAGE_SHOW = false ]; and return

	# Exit if there is no package.json or Cargo.toml
	if not test -e ./package.json; and not test -e ./Cargo.toml
		return
	end

	set -l package_version

	# Check if package.json exists AND npm exists locally while supressing output to just exit code (-q)
	if type -q npm; and test -f ./package.json
		# Check if jq (json handler) exists locally. If yes, check in package.json version
		if type -q jq
			set package_version (jq -r '.version' package.json 2>/dev/null)
		# Check if python exists locally, use json to check version in package.json
		else if type -q python
			set package_version (python -c "import json; print(json.load(open('package.json'))['version'])" 2>/dev/null)
		# Check if node exists locally, use it to check version of package.json
		else if type -q node
			set package_version (node -p "require('./package.json').version" 2>/dev/null)
		end
	end

	# Check if Cargo.toml exists and cargo command exists
	# and use cargo pkgid to figure out the package
	if type -q cargo; and test -f ./Cargo.toml
		# Handle missing field `version` in Cargo.toml.
		# `cargo pkgid` needs Cargo.lock to exists too. If
		# it doesn't, do not show package version
		set -l pkgid (cargo pkgid 2>&1)
		# Early return on error
		echo $pkgid | grep -q "error:"; and return

		# Example input: abc#1.0.0. Example output: 1.0.1
		set package_version (string match -r '#(.*)' $pkgid)[2]
	end

	if test -z "$package_version"
		set package_version ⚠
	else
		set package_version "v$package_version"
	end

	__sf_lib_section \
		$SPACEFISH_PACKAGE_COLOR \
		$SPACEFISH_PACKAGE_PREFIX \
		"$SPACEFISH_PACKAGE_SYMBOL$package_version" \
		$SPACEFISH_PACKAGE_SUFFIX
...
33	82	----> __sf_util_set_default SPACEFISH_PACKAGE_SHOW true
8	49	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
23	23	------> set -g $var $argv[2..-1]
28	64	----> __sf_util_set_default SPACEFISH_PACKAGE_PREFIX "is "
6	36	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
27	60	----> __sf_util_set_default SPACEFISH_PACKAGE_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
6	33	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
24	55	----> __sf_util_set_default SPACEFISH_PACKAGE_SYMBOL "📦 "
6	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
22	52	----> __sf_util_set_default SPACEFISH_PACKAGE_COLOR red
5	30	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_PACKAGE_SHOW = false ]
9	34	----> if not test -e ./package.json; and not test -e ./Cargo.toml
		return
	...
12	12	-----> not test -e ./package.json
8	8	-----> not test -e ./Cargo.toml
5	5	-----> return
31	1506	--> eval __sf_section_$i
221	1475	---> __sf_section_node
331	345	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_node.fish
14	14	-----> function __sf_section_node -d "Display the local node version"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_NODE_SHOW true
	__sf_util_set_default SPACEFISH_NODE_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_NODE_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_NODE_SYMBOL "⬢ "
	__sf_util_set_default SPACEFISH_NODE_DEFAULT_VERSION ""
	__sf_util_set_default SPACEFISH_NODE_COLOR green

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show the current version of Node
	[ $SPACEFISH_NODE_SHOW = false ]; and return

	# Show versions only for Node-specific folders
	if not test -f ./package.json \
		-o -d ./node_modules \
		-o (count *.js) -gt 0
		return
	end

	if type -q nvm
		# Only recheck the node version if the nvm bin has changed
		if test "$NVM_BIN" != "$sf_last_nvm_bin" -o -z "$sf_node_version"
			set -g sf_node_version (nvm current 2>/dev/null)
			set -g sf_last_nvm_bin $NVM_BIN
		end
	else if type -q nodenv
		set -g sf_node_version (nodenv version-name 2>/dev/null)
	else if type -q node
		set -g sf_node_version (node -v 2>/dev/null)
	else
		return
	end

	# Don't echo section if the system verison of node is being used
	[ "$sf_node_version" = "system" -o "$sf_node_version" = "node" ]; and return

	# Don't echo section if the node version matches the default version
	[ "$sf_node_version" = "$SPACEFISH_NODE_DEFAULT_VERSION" ]; and return

	__sf_lib_section \
		$SPACEFISH_NODE_COLOR \
		$SPACEFISH_NODE_PREFIX \
		"$SPACEFISH_NODE_SYMBOL$sf_node_version" \
		$SPACEFISH_NODE_SUFFIX
...
43	111	----> __sf_util_set_default SPACEFISH_NODE_SHOW true
13	68	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	------> not set -q $var
34	34	------> set -g $var $argv[2..-1]
38	87	----> __sf_util_set_default SPACEFISH_NODE_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
8	49	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
44	84	----> __sf_util_set_default SPACEFISH_NODE_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
7	40	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
38	77	----> __sf_util_set_default SPACEFISH_NODE_SYMBOL "⬢ "
9	39	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
42	98	----> __sf_util_set_default SPACEFISH_NODE_DEFAULT_VERSION ""
11	56	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
26	26	------> set -g $var $argv[2..-1]
37	91	----> __sf_util_set_default SPACEFISH_NODE_COLOR green
10	54	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
26	26	------> set -g $var $argv[2..-1]
23	23	----> [ $SPACEFISH_NODE_SHOW = false ]
16	338	----> if not test -f ./package.json \
		-o -d ./node_modules \
		-o (count *.js) -gt 0
		return
	...
197	308	-----> not test -f ./package.json \
		-o -d ./node_modules \
		-o (count *.js) -gt 0
111	111	------> count *.js
14	14	-----> return
48	1504	--> eval __sf_section_$i
369	1456	---> __sf_section_ruby
365	391	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_ruby.fish
26	26	-----> function __sf_section_ruby -d "Show current version of Ruby"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_RUBY_SHOW true
	__sf_util_set_default SPACEFISH_RUBY_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_RUBY_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_RUBY_SYMBOL "💎 "
	__sf_util_set_default SPACEFISH_RUBY_COLOR red

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Check if that user wants to show ruby version
	[ $SPACEFISH_RUBY_SHOW = false ]; and return

	# Show versions only for Ruby-specific folders
	if not test -f Gemfile \
		-o -f Rakefile \
		-o (count *.rb) -gt 0
		return
	end

	set -l ruby_version

	if type -q rvm-prompt
		set ruby_version (rvm-prompt i v g)
	else if type -q rbenv
		set ruby_version (rbenv version-name)
	else if type -q chruby
		set ruby_version $RUBY_AUTO_VERSION
	else if type -q asdf
		set ruby_version (asdf current ruby | awk '{print $1}')
	else
		return
	end

	[ -z "$ruby_version" -o "$ruby_version" = "system" ]; and return

	# Add 'v' before ruby version that starts with a number
	if test -n (echo (string match -r "^[0-9].+\$" "$ruby_version"))
		set ruby_version "v$ruby_version"
	end

	__sf_lib_section \
		$SPACEFISH_RUBY_COLOR \
		$SPACEFISH_RUBY_PREFIX \
		"$SPACEFISH_RUBY_SYMBOL""$ruby_version" \
		$SPACEFISH_RUBY_SUFFIX
...
57	146	----> __sf_util_set_default SPACEFISH_RUBY_SHOW true
20	89	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
32	32	------> not set -q $var
37	37	------> set -g $var $argv[2..-1]
46	104	----> __sf_util_set_default SPACEFISH_RUBY_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
10	58	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
29	29	------> set -g $var $argv[2..-1]
38	79	----> __sf_util_set_default SPACEFISH_RUBY_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
7	41	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
24	55	----> __sf_util_set_default SPACEFISH_RUBY_SYMBOL "💎 "
6	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
21	50	----> __sf_util_set_default SPACEFISH_RUBY_COLOR red
5	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
14	14	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_RUBY_SHOW = false ]
8	248	----> if not test -f Gemfile \
		-o -f Rakefile \
		-o (count *.rb) -gt 0
		return
	...
147	231	-----> not test -f Gemfile \
		-o -f Rakefile \
		-o (count *.rb) -gt 0
84	84	------> count *.rb
9	9	-----> return
32	3550	--> eval __sf_section_$i
368	3518	---> __sf_section_golang
331	361	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_golang.fish
30	30	-----> function __sf_section_golang -d "Display the current go version if you're inside GOPATH"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_GOLANG_SHOW true
	__sf_util_set_default SPACEFISH_GOLANG_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_GOLANG_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_GOLANG_SYMBOL "🐹 "
	__sf_util_set_default SPACEFISH_GOLANG_COLOR cyan

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show the current version of Golang
	[ $SPACEFISH_GOLANG_SHOW = false ]; and return

	# Ensure the go command is available
	type -q go; or return

	if not test -f go.mod \
		-o -d Godeps \
		-o -f glide.yaml \
		-o (count *.go) -gt 0 \
		-o -f Gopkg.yml \
		-o -f Gopkg.lock \
		-o ([ (count $GOPATH) -gt 0 ]; and string match $GOPATH $PWD)
		return
	end

	set -l go_version (go version | string split ' ')

	# Go version is either the commit hash and date (devel +5efe9a8f11 Web Jan 9 07:21:16 2019 +0000)
	# at the time of the build or a release tag (go1.11.4)
	# https://github.com/matchai/spacefish/issues/137
	if test (string match 'devel*' $go_version[3])
		set go_version $go_version[3]":"(string sub -s 2 $go_version[4])
	else
		set go_version "v"(string sub -s 3 $go_version[3])
	end

	__sf_lib_section \
		$SPACEFISH_GOLANG_COLOR \
		$SPACEFISH_GOLANG_PREFIX \
		"$SPACEFISH_GOLANG_SYMBOL""$go_version" \
		$SPACEFISH_GOLANG_SUFFIX
...
95	207	----> __sf_util_set_default SPACEFISH_GOLANG_SHOW true
24	112	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
49	49	------> not set -q $var
39	39	------> set -g $var $argv[2..-1]
62	126	----> __sf_util_set_default SPACEFISH_GOLANG_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
15	64	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
25	25	------> not set -q $var
24	24	------> set -g $var $argv[2..-1]
53	192	----> __sf_util_set_default SPACEFISH_GOLANG_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
15	139	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
104	104	------> set -g $var $argv[2..-1]
87	185	----> __sf_util_set_default SPACEFISH_GOLANG_SYMBOL "🐹 "
18	98	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
25	25	------> not set -q $var
55	55	------> set -g $var $argv[2..-1]
56	126	----> __sf_util_set_default SPACEFISH_GOLANG_COLOR cyan
14	70	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
28	28	------> not set -q $var
28	28	------> set -g $var $argv[2..-1]
26	26	----> [ $SPACEFISH_GOLANG_SHOW = false ]
119	1387	----> type -q go
19	19	-----> set -q argv[1]
37	37	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
94	94	-----> argparse -n type -x t,p,P $options -- $argv
7	22	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
15	15	------> set -q _flag_help
14	14	-----> set -l res 1
13	13	-----> set -l mode normal
12	12	-----> set -l multi no
12	12	-----> set -l selection all
8	8	-----> set -l short no
8	23	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	------> set -q _flag_quiet
9	9	------> set mode quiet
7	7	-----> set -q _flag_all
7	7	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
37	975	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
11	11	------> set -l found 0
19	500	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
13	13	-------> test $selection != files
3	95	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
92	92	--------> functions -q -- $i
7	373	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
318	366	--------> contains -- $i (builtin -n)
48	48	---------> builtin -n
30	30	------> set -l paths
16	233	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
26	26	-------> test $multi != yes
134	191	-------> set paths (command -s -- $i)
57	57	--------> command -s -- $i
40	139	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
22	22	-------> set res 0
17	17	-------> set found 1
16	16	-------> switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            ...
11	44	-------> if test $multi != yes
                continue
            ...
21	21	--------> test $multi != yes
12	12	--------> continue
7	25	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
18	18	-------> test $found = 0
19	19	-----> return $res
9	540	----> if not test -f go.mod \
		-o -d Godeps \
		-o -f glide.yaml \
		-o (count *.go) -gt 0 \
		-o -f Gopkg.yml \
		-o -f Gopkg.lock \
		-o ([ (count $GOPATH) -gt 0 ]; and string match $GOPATH $PWD)
		return
	...
263	521	-----> not test -f go.mod \
		-o -d Godeps \
		-o -f glide.yaml \
		-o (count *.go) -gt 0 \
		-o -f Gopkg.yml \
		-o -f Gopkg.lock \
		-o ([ (count $GOPATH) -gt 0 ]; and string match $GOPATH $PWD)
124	124	------> count *.go
102	134	------> [ (count $GOPATH) -gt 0 ]
32	32	-------> count $GOPATH
10	10	-----> return
45	2214	--> eval __sf_section_$i
222	2169	---> __sf_section_php
265	284	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_php.fish
19	19	-----> function __sf_section_php -d "Display the current php version"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_PHP_SHOW true
	__sf_util_set_default SPACEFISH_PHP_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_PHP_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_PHP_SYMBOL "🐘 "
	__sf_util_set_default SPACEFISH_PHP_COLOR blue

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show current version of PHP
	[ $SPACEFISH_PHP_SHOW = false ]; and return

	# Ensure the php command is available
	type -q php; or return

	if not test -f composer.json \
		-o (count *.php) -gt 0
		return
	end

	set -l php_version (php -v | string match -r 'PHP\s*[0-9.]+' | string split ' ')[2]

	__sf_lib_section \
		$SPACEFISH_PHP_COLOR \
		$SPACEFISH_PHP_PREFIX \
		"$SPACEFISH_PHP_SYMBOL"v"$php_version" \
		$SPACEFISH_PHP_SUFFIX
...
40	121	----> __sf_util_set_default SPACEFISH_PHP_SHOW true
11	81	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
50	50	------> set -g $var $argv[2..-1]
37	90	----> __sf_util_set_default SPACEFISH_PHP_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
10	53	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
22	22	------> not set -q $var
21	21	------> set -g $var $argv[2..-1]
41	89	----> __sf_util_set_default SPACEFISH_PHP_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
14	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
19	19	------> set -g $var $argv[2..-1]
40	78	----> __sf_util_set_default SPACEFISH_PHP_SYMBOL "🐘 "
7	38	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
35	73	----> __sf_util_set_default SPACEFISH_PHP_COLOR blue
7	38	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
22	22	----> [ $SPACEFISH_PHP_SHOW = false ]
78	1180	----> type -q php
10	10	-----> set -q argv[1]
40	40	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
56	56	-----> argparse -n type -x t,p,P $options -- $argv
5	16	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
11	11	------> set -q _flag_help
11	11	-----> set -l res 1
10	10	-----> set -l mode normal
12	12	-----> set -l multi no
17	17	-----> set -l selection all
10	10	-----> set -l short no
8	24	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	------> set -q _flag_quiet
9	9	------> set mode quiet
9	9	-----> set -q _flag_all
12	12	-----> set -q _flag_short
10	10	-----> set -q _flag_no_functions
35	852	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
11	11	------> set -l found 0
19	505	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
15	15	-------> test $selection != files
5	120	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
115	115	--------> functions -q -- $i
9	351	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
265	342	--------> contains -- $i (builtin -n)
77	77	---------> builtin -n
21	21	------> set -l paths
10	220	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	-------> test $multi != yes
103	196	-------> set paths (command -s -- $i)
93	93	--------> command -s -- $i
14	14	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
9	46	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
16	16	-------> test $found = 0
21	21	-------> test $mode != quiet
13	13	-----> return $res
10	10	----> return
35	2293	--> eval __sf_section_$i
223	2258	---> __sf_section_rust
326	341	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_rust.fish
15	15	-----> function __sf_section_rust -d "Display the current Rust version"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_RUST_SHOW true
	__sf_util_set_default SPACEFISH_RUST_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_RUST_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_RUST_SYMBOL "𝗥 "
	__sf_util_set_default SPACEFISH_RUST_COLOR red
	__sf_util_set_default SPACEFISH_RUST_VERBOSE_VERSION false

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show current version of Rust
	[ $SPACEFISH_RUST_SHOW = false ]; and return

	# Ensure the rustc command is available
	type -q rustc; or return

	if not test -f Cargo.toml \
		-o (count *.rs) -gt 0
		return
	end

	set -l rust_version (rustc --version | string split ' ')[2]

	if test $SPACEFISH_RUST_VERBOSE_VERSION = false
        set rust_version (string split '-' $rust_version)[1] # Cut off -suffixes from version. "v1.30.0-beta" vs "v1.30.0"
	end

	__sf_lib_section \
		$SPACEFISH_RUST_COLOR \
		$SPACEFISH_RUST_PREFIX \
		"$SPACEFISH_RUST_SYMBOL"v"$rust_version" \
		$SPACEFISH_RUST_SUFFIX
...
35	96	----> __sf_util_set_default SPACEFISH_RUST_SHOW true
10	61	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
27	27	------> not set -q $var
24	24	------> set -g $var $argv[2..-1]
46	92	----> __sf_util_set_default SPACEFISH_RUST_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
9	46	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
22	22	------> set -g $var $argv[2..-1]
37	85	----> __sf_util_set_default SPACEFISH_RUST_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
8	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
27	27	------> set -g $var $argv[2..-1]
29	77	----> __sf_util_set_default SPACEFISH_RUST_SYMBOL "𝗥 "
7	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
29	29	------> set -g $var $argv[2..-1]
25	73	----> __sf_util_set_default SPACEFISH_RUST_COLOR red
10	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
27	27	------> set -g $var $argv[2..-1]
26	74	----> __sf_util_set_default SPACEFISH_RUST_VERBOSE_VERSION false
9	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
28	28	------> set -g $var $argv[2..-1]
16	16	----> [ $SPACEFISH_RUST_SHOW = false ]
91	1171	----> type -q rustc
15	15	-----> set -q argv[1]
28	28	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
55	55	-----> argparse -n type -x t,p,P $options -- $argv
3	14	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
11	11	------> set -q _flag_help
11	11	-----> set -l res 1
14	14	-----> set -l mode normal
16	16	-----> set -l multi no
11	11	-----> set -l selection all
8	8	-----> set -l short no
9	26	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	------> set -q _flag_quiet
11	11	------> set mode quiet
14	14	-----> set -q _flag_all
9	9	-----> set -q _flag_short
26	26	-----> set -q _flag_no_functions
39	820	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
13	13	------> set -l found 0
21	467	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
14	14	-------> test $selection != files
4	122	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
118	118	--------> functions -q -- $i
7	310	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
241	303	--------> contains -- $i (builtin -n)
62	62	---------> builtin -n
18	18	------> set -l paths
9	223	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
15	15	-------> test $multi != yes
107	199	-------> set paths (command -s -- $i)
92	92	--------> command -s -- $i
14	14	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
8	46	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
20	20	-------> test $found = 0
18	18	-------> test $mode != quiet
13	13	-----> return $res
10	10	----> return
34	2147	--> eval __sf_section_$i
245	2113	---> __sf_section_haskell
239	255	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_haskell.fish
16	16	-----> function __sf_section_haskell -d "Show current version of Haskell Tool Stack"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_HASKELL_SHOW true
	__sf_util_set_default SPACEFISH_HASKELL_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_HASKELL_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_HASKELL_SYMBOL "λ "
	__sf_util_set_default SPACEFISH_HASKELL_COLOR red

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show current version of Haskell Tool Stack.
	[ $SPACEFISH_HASKELL_SHOW = false ]; and return

	# Ensure the stack command is available
	type -q stack; or return

	# If there are stack files in current directory
	[ -f ./stack.yaml ]; or return

	set -l haskell_version (stack ghc -- --numeric-version --no-install-ghc)

	__sf_lib_section \
		$SPACEFISH_HASKELL_COLOR \
		$SPACEFISH_HASKELL_PREFIX \
		"$SPACEFISH_HASKELL_SYMBOL"v"$haskell_version" \
		$SPACEFISH_HASKELL_SUFFIX
...
46	103	----> __sf_util_set_default SPACEFISH_HASKELL_SHOW true
11	57	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	------> not set -q $var
25	25	------> set -g $var $argv[2..-1]
35	88	----> __sf_util_set_default SPACEFISH_HASKELL_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
9	53	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
30	30	------> set -g $var $argv[2..-1]
31	84	----> __sf_util_set_default SPACEFISH_HASKELL_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
10	53	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
26	26	------> set -g $var $argv[2..-1]
28	76	----> __sf_util_set_default SPACEFISH_HASKELL_SYMBOL "λ "
9	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
26	73	----> __sf_util_set_default SPACEFISH_HASKELL_COLOR red
9	47	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
19	19	------> set -g $var $argv[2..-1]
13	13	----> [ $SPACEFISH_HASKELL_SHOW = false ]
111	1170	----> type -q stack
11	11	-----> set -q argv[1]
25	25	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
81	81	-----> argparse -n type -x t,p,P $options -- $argv
4	16	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
12	12	------> set -q _flag_help
11	11	-----> set -l res 1
11	11	-----> set -l mode normal
16	16	-----> set -l multi no
14	14	-----> set -l selection all
9	9	-----> set -l short no
10	25	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	------> set -q _flag_quiet
9	9	------> set mode quiet
12	12	-----> set -q _flag_all
12	12	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
44	791	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
11	11	------> set -l found 0
24	446	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
21	21	-------> test $selection != files
5	117	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
112	112	--------> functions -q -- $i
6	284	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
226	278	--------> contains -- $i (builtin -n)
52	52	---------> builtin -n
20	20	------> set -l paths
10	216	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	-------> test $multi != yes
101	192	-------> set paths (command -s -- $i)
91	91	--------> command -s -- $i
13	13	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
9	41	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
13	13	-------> test $found = 0
19	19	-------> test $mode != quiet
19	19	-----> return $res
6	6	----> return
45	2146	--> eval __sf_section_$i
216	2101	---> __sf_section_julia
249	263	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_julia.fish
14	14	-----> function __sf_section_julia -d "Display julia version"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_JULIA_SHOW true
	__sf_util_set_default SPACEFISH_JULIA_PREFIX "is "
	__sf_util_set_default SPACEFISH_JULIA_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_JULIA_SYMBOL "ஃ "
	__sf_util_set_default SPACEFISH_JULIA_COLOR green

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_JULIA_SHOW = false ]; and return

	# Show Julia version only if julia is installed
	type -q julia; or return

	# Show julia version only when pwd has *.jl file(s)
	[ (count *.jl) -gt 0 ]; or return

	set -l julia_version (julia --version | grep --color=never -oE '[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]')

	__sf_lib_section \
	$SPACEFISH_JULIA_COLOR \
	$SPACEFISH_JULIA_PREFIX \
	"$SPACEFISH_JULIA_SYMBOL"v"$julia_version" \
	$SPACEFISH_JULIA_SUFFIX
...
46	102	----> __sf_util_set_default SPACEFISH_JULIA_SHOW true
11	56	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
25	25	------> set -g $var $argv[2..-1]
35	87	----> __sf_util_set_default SPACEFISH_JULIA_PREFIX "is "
10	52	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
29	29	------> set -g $var $argv[2..-1]
31	82	----> __sf_util_set_default SPACEFISH_JULIA_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
9	51	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
29	29	------> set -g $var $argv[2..-1]
26	77	----> __sf_util_set_default SPACEFISH_JULIA_SYMBOL "ஃ "
9	51	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
27	27	------> set -g $var $argv[2..-1]
28	94	----> __sf_util_set_default SPACEFISH_JULIA_COLOR green
10	66	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
38	38	------> set -g $var $argv[2..-1]
20	20	----> [ $SPACEFISH_JULIA_SHOW = false ]
85	1154	----> type -q julia
13	13	-----> set -q argv[1]
34	34	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
55	55	-----> argparse -n type -x t,p,P $options -- $argv
3	14	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
11	11	------> set -q _flag_help
11	11	-----> set -l res 1
10	10	-----> set -l mode normal
15	15	-----> set -l multi no
15	15	-----> set -l selection all
9	9	-----> set -l short no
9	25	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	------> set -q _flag_quiet
9	9	------> set mode quiet
12	12	-----> set -q _flag_all
11	11	-----> set -q _flag_short
7	7	-----> set -q _flag_no_functions
33	824	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
10	10	------> set -l found 0
26	455	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
20	20	-------> test $selection != files
5	116	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
111	111	--------> functions -q -- $i
9	293	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
229	284	--------> contains -- $i (builtin -n)
55	55	---------> builtin -n
19	19	------> set -l paths
10	255	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	-------> test $multi != yes
118	231	-------> set paths (command -s -- $i)
113	113	--------> command -s -- $i
14	14	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
8	38	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
15	15	-------> test $found = 0
15	15	-------> test $mode != quiet
14	14	-----> return $res
6	6	----> return
39	1577	--> eval __sf_section_$i
230	1538	---> __sf_section_elixir
385	403	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_elixir.fish
18	18	-----> function __sf_section_elixir -d "Show current version of Elixir"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_ELIXIR_SHOW true
	__sf_util_set_default SPACEFISH_ELIXIR_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_ELIXIR_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_ELIXIR_SYMBOL "💧 "
	__sf_util_set_default SPACEFISH_ELIXIR_DEFAULT_VERSION $SPACEFISH_ELIXIR_DEFAULT_VERSION
	__sf_util_set_default SPACEFISH_ELIXIR_COLOR magenta

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Check if that user wants to show elixir version
	[ $SPACEFISH_ELIXIR_SHOW = false ]; and return

	# Show versions only for Elixir-specific folders
	if not test -f mix.exs \
		-o (count *.ex) -gt 0 \
		-o (count *.exs) -gt 0
		return
	end

	set -l elixir_version

	if type -q kiex
		set elixir_version $ELIXIR_VERSION
	else if type -q exenv
		set elixir_version (exenv version-name)
	else if type -q elixir
		set elixir_version (elixir -v 2>/dev/null | string match -r "Elixir.*" | string split " ")[2]
	else
		return
	end

	[ -z "$elixir_version" -o "$elixir_version" = "system" ]; and return

	# Add 'v' before elixir version that starts with a number
	if test -n (echo (string match -r "^[0-9].+\$" "$elixir_version"))
		set elixir_version "v$elixir_version"
	end

	__sf_lib_section \
		$SPACEFISH_ELIXIR_COLOR \
		$SPACEFISH_ELIXIR_PREFIX \
		"$SPACEFISH_ELIXIR_SYMBOL""$elixir_version" \
		$SPACEFISH_ELIXIR_SUFFIX
...
38	101	----> __sf_util_set_default SPACEFISH_ELIXIR_SHOW true
11	63	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
33	33	------> set -g $var $argv[2..-1]
49	104	----> __sf_util_set_default SPACEFISH_ELIXIR_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
10	55	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
24	24	------> not set -q $var
21	21	------> set -g $var $argv[2..-1]
46	87	----> __sf_util_set_default SPACEFISH_ELIXIR_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
8	41	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
19	19	------> set -g $var $argv[2..-1]
36	76	----> __sf_util_set_default SPACEFISH_ELIXIR_SYMBOL "💧 "
7	40	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
38	79	----> __sf_util_set_default SPACEFISH_ELIXIR_DEFAULT_VERSION $SPACEFISH_ELIXIR_DEFAULT_VERSION
7	41	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
21	21	------> set -g $var $argv[2..-1]
31	75	----> __sf_util_set_default SPACEFISH_ELIXIR_COLOR magenta
8	44	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
23	23	------> set -g $var $argv[2..-1]
17	17	----> [ $SPACEFISH_ELIXIR_SHOW = false ]
12	366	----> if not test -f mix.exs \
		-o (count *.ex) -gt 0 \
		-o (count *.exs) -gt 0
		return
	...
201	343	-----> not test -f mix.exs \
		-o (count *.ex) -gt 0 \
		-o (count *.exs) -gt 0
77	77	------> count *.ex
65	65	------> count *.exs
11	11	-----> return
38	2724	--> eval __sf_section_$i
228	2686	---> __sf_section_docker
360	380	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_docker.fish
20	20	-----> function __sf_section_docker -d "Display docker version and machine name"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_DOCKER_SHOW true
	__sf_util_set_default SPACEFISH_DOCKER_PREFIX "is "
	__sf_util_set_default SPACEFISH_DOCKER_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_DOCKER_SYMBOL "🐳 "
	__sf_util_set_default SPACEFISH_DOCKER_COLOR cyan
	__sf_util_set_default SPACEFISH_DOCKER_VERBOSE_VERSION false

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_DOCKER_SHOW = false ]; and return

	# Show Docker version only if docker is installed
	type -q docker; or return

	# Show docker version only when pwd has Dockerfile, docker-compose.yml, .dockerenv in root or COMPOSE_FILE
	if not test -f Dockerfile \
		-o -f docker-compose.yml \
		-o -f /.dockerenv \
		-o -f "$COMPOSE_FILE"
		return
	end

	set -l docker_version (docker version -f "{{.Server.Version}}" 2>/dev/null)
	# if docker daemon isn't running you'll get an error like 'Bad response from Docker engine'
	[ -z $docker_version ]; and return

	if test "$SPACEFISH_DOCKER_VERBOSE_VERSION" = "false"
			set docker_version (string split - $docker_version)[1]
	end

	if test -n "$DOCKER_MACHINE_NAME"
			set docker_version $docker_version via $DOCKER_MACHINE_NAME
	end

	__sf_lib_section \
	$SPACEFISH_DOCKER_COLOR \
	$SPACEFISH_DOCKER_PREFIX \
	"$SPACEFISH_DOCKER_SYMBOL"v"$docker_version" \
	$SPACEFISH_DOCKER_SUFFIX
...
51	125	----> __sf_util_set_default SPACEFISH_DOCKER_SHOW true
13	74	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
26	26	------> not set -q $var
35	35	------> set -g $var $argv[2..-1]
102	208	----> __sf_util_set_default SPACEFISH_DOCKER_PREFIX "is "
22	106	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
42	42	------> not set -q $var
42	42	------> set -g $var $argv[2..-1]
107	202	----> __sf_util_set_default SPACEFISH_DOCKER_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
24	95	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
34	34	------> not set -q $var
37	37	------> set -g $var $argv[2..-1]
140	223	----> __sf_util_set_default SPACEFISH_DOCKER_SYMBOL "🐳 "
16	83	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
37	37	------> not set -q $var
30	30	------> set -g $var $argv[2..-1]
42	90	----> __sf_util_set_default SPACEFISH_DOCKER_COLOR cyan
9	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
30	68	----> __sf_util_set_default SPACEFISH_DOCKER_VERBOSE_VERSION false
8	38	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
19	19	------> set -g $var $argv[2..-1]
21	21	----> [ $SPACEFISH_DOCKER_SHOW = false ]
128	1132	----> type -q docker
11	11	-----> set -q argv[1]
24	24	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
62	62	-----> argparse -n type -x t,p,P $options -- $argv
5	20	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
15	15	------> set -q _flag_help
16	16	-----> set -l res 1
12	12	-----> set -l mode normal
17	17	-----> set -l multi no
12	12	-----> set -l selection all
8	8	-----> set -l short no
13	39	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
11	11	------> set -q _flag_quiet
15	15	------> set mode quiet
13	13	-----> set -q _flag_all
13	13	-----> set -q _flag_short
13	13	-----> set -q _flag_no_functions
35	723	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
12	12	------> set -l found 0
23	414	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
13	13	-------> test $selection != files
2	98	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
96	96	--------> functions -q -- $i
5	280	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
229	275	--------> contains -- $i (builtin -n)
46	46	---------> builtin -n
16	16	------> set -l paths
7	198	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	-------> test $multi != yes
100	177	-------> set paths (command -s -- $i)
77	77	--------> command -s -- $i
13	13	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
8	35	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
13	13	-------> test $found = 0
14	14	-------> test $mode != quiet
21	21	-----> return $res
9	9	----> return
65	2422	--> eval __sf_section_$i
429	2357	---> __sf_section_aws
475	506	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_aws.fish
31	31	-----> function __sf_section_aws -d "Display the selected aws profile"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_AWS_SHOW true
	__sf_util_set_default SPACEFISH_AWS_PREFIX "using "
	__sf_util_set_default SPACEFISH_AWS_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_AWS_SYMBOL "☁️ "
	__sf_util_set_default SPACEFISH_AWS_COLOR ff8700

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show the selected AWS-cli profile
	[ $SPACEFISH_AWS_SHOW = false ]; and return

	# Ensure the aws command is available
	type -q aws; or return

  set -l PROFILE_NAME
	
  # if aws-vault is in use, override profile with that
  if test -n "$AWS_VAULT"
    set PROFILE_NAME "$AWS_VAULT"
  else
    set PROFILE_NAME "$AWS_PROFILE"
  end

	# Early return if there's no named profile, or it's set to default
	if test -z "$PROFILE_NAME" \
		-o "$PROFILE_NAME" = "default"
		return
	end

	__sf_lib_section \
		$SPACEFISH_AWS_COLOR \
		$SPACEFISH_AWS_PREFIX \
		"$SPACEFISH_AWS_SYMBOL""$PROFILE_NAME" \
		$SPACEFISH_AWS_SUFFIX
...
59	150	----> __sf_util_set_default SPACEFISH_AWS_SHOW true
17	91	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
38	38	------> not set -q $var
36	36	------> set -g $var $argv[2..-1]
54	115	----> __sf_util_set_default SPACEFISH_AWS_PREFIX "using "
12	61	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
23	23	------> not set -q $var
26	26	------> set -g $var $argv[2..-1]
49	104	----> __sf_util_set_default SPACEFISH_AWS_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
11	55	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
26	26	------> set -g $var $argv[2..-1]
23	54	----> __sf_util_set_default SPACEFISH_AWS_SYMBOL "☁️ "
6	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
21	66	----> __sf_util_set_default SPACEFISH_AWS_COLOR ff8700
7	45	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
29	29	------> set -g $var $argv[2..-1]
16	16	----> [ $SPACEFISH_AWS_SHOW = false ]
70	911	----> type -q aws
9	9	-----> set -q argv[1]
23	23	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
44	44	-----> argparse -n type -x t,p,P $options -- $argv
3	11	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
8	8	------> set -q _flag_help
10	10	-----> set -l res 1
9	9	-----> set -l mode normal
8	8	-----> set -l multi no
9	9	-----> set -l selection all
8	8	-----> set -l short no
7	23	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	------> set -q _flag_quiet
10	10	------> set mode quiet
7	7	-----> set -q _flag_all
6	6	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
29	658	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
10	10	------> set -l found 0
16	366	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
12	12	-------> test $selection != files
3	94	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
91	91	--------> functions -q -- $i
5	244	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
194	239	--------> contains -- $i (builtin -n)
45	45	---------> builtin -n
15	15	------> set -l paths
7	195	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	-------> test $multi != yes
92	174	-------> set paths (command -s -- $i)
82	82	--------> command -s -- $i
12	12	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
6	31	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
13	13	-------> test $found = 0
12	12	-------> test $mode != quiet
10	10	-----> return $res
6	6	----> return
41	884	--> eval __sf_section_$i
212	843	---> __sf_section_venv
219	232	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_venv.fish
13	13	-----> function __sf_section_venv -d "Show current virtual Python environment"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_VENV_SHOW true
	__sf_util_set_default SPACEFISH_VENV_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_VENV_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_VENV_SYMBOL "·"
	__sf_util_set_default SPACEFISH_VENV_GENERIC_NAMES virtualenv venv .venv
	__sf_util_set_default SPACEFISH_VENV_COLOR blue

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show venv python version
	 test $SPACEFISH_VENV_SHOW = false; and return

	# Check if the current directory running via Virtualenv
	test -n "$VIRTUAL_ENV"; or return

	set -l venv (basename $VIRTUAL_ENV)
	if contains $venv $SPACEFISH_VENV_GENERIC_NAMES
		set venv (basename (dirname $VIRTUAL_ENV))
	end

	__sf_lib_section \
		$SPACEFISH_VENV_COLOR \
		$SPACEFISH_VENV_PREFIX \
		"$SPACEFISH_VENV_SYMBOL""$venv" \
		$SPACEFISH_VENV_SUFFIX
...
35	84	----> __sf_util_set_default SPACEFISH_VENV_SHOW true
9	49	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
22	22	------> set -g $var $argv[2..-1]
29	64	----> __sf_util_set_default SPACEFISH_VENV_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
6	35	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
26	58	----> __sf_util_set_default SPACEFISH_VENV_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	32	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
23	53	----> __sf_util_set_default SPACEFISH_VENV_SYMBOL "·"
6	30	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
23	59	----> __sf_util_set_default SPACEFISH_VENV_GENERIC_NAMES virtualenv venv .venv
6	36	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
21	51	----> __sf_util_set_default SPACEFISH_VENV_COLOR blue
6	30	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
14	14	------> set -g $var $argv[2..-1]
13	13	----> test $SPACEFISH_VENV_SHOW = false
11	11	----> test -n "$VIRTUAL_ENV"
6	6	----> return
29	1734	--> eval __sf_section_$i
225	1705	---> __sf_section_conda
206	218	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_conda.fish
12	12	-----> function __sf_section_conda -d "Display current Conda version"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_CONDA_SHOW true
	__sf_util_set_default SPACEFISH_CONDA_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_CONDA_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_CONDA_SYMBOL "🅒 "
	__sf_util_set_default SPACEFISH_CONDA_COLOR blue

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_CONDA_SHOW = false ]; and return

	# Show Conda version only if conda is installed and CONDA_DEFAULT_ENV is set
	if not type -q conda; \
		or test -z "$CONDA_DEFAULT_ENV";
		return
	end

	set -l conda_version (conda -V | string split ' ')[2]

	__sf_lib_section \
		$SPACEFISH_CONDA_COLOR \
		$SPACEFISH_CONDA_PREFIX \
		"$SPACEFISH_CONDA_SYMBOL"v"$conda_version" \
		$SPACEFISH_CONDA_SUFFIX
...
36	84	----> __sf_util_set_default SPACEFISH_CONDA_SHOW true
9	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
21	21	------> set -g $var $argv[2..-1]
28	63	----> __sf_util_set_default SPACEFISH_CONDA_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
7	35	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
25	58	----> __sf_util_set_default SPACEFISH_CONDA_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
6	33	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
22	53	----> __sf_util_set_default SPACEFISH_CONDA_SYMBOL "🅒 "
6	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
20	50	----> __sf_util_set_default SPACEFISH_CONDA_COLOR blue
5	30	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
14	14	------> set -g $var $argv[2..-1]
13	13	----> [ $SPACEFISH_CONDA_SHOW = false ]
7	941	----> if not type -q conda; \
		or test -z "$CONDA_DEFAULT_ENV";
		return
	...
67	928	-----> not type -q conda
9	9	------> set -q argv[1]
23	23	------> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
44	44	------> argparse -n type -x t,p,P $options -- $argv
3	11	------> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
8	8	-------> set -q _flag_help
10	10	------> set -l res 1
10	10	------> set -l mode normal
9	9	------> set -l multi no
9	9	------> set -l selection all
8	8	------> set -l short no
7	22	------> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	-------> set -q _flag_quiet
9	9	-------> set mode quiet
6	6	------> set -q _flag_all
6	6	------> set -q _flag_short
6	6	------> set -q _flag_no_functions
30	677	------> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
10	10	-------> set -l found 0
23	395	-------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
13	13	--------> test $selection != files
4	108	--------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
104	104	---------> functions -q -- $i
6	251	--------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
199	245	---------> contains -- $i (builtin -n)
46	46	----------> builtin -n
15	15	-------> set -l paths
7	185	-------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	--------> test $multi != yes
88	164	--------> set paths (command -s -- $i)
76	76	---------> command -s -- $i
12	12	-------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
6	30	-------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
13	13	--------> test $found = 0
11	11	--------> test $mode != quiet
11	11	------> return $res
6	6	-----> return
31	1809	--> eval __sf_section_$i
206	1778	---> __sf_section_pyenv
242	254	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_pyenv.fish
12	12	-----> function __sf_section_pyenv -d "Show current version of pyenv Python, including system."
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_PYENV_SHOW true
	__sf_util_set_default SPACEFISH_PYENV_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_PYENV_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_PYENV_SYMBOL "🐍 "
	__sf_util_set_default SPACEFISH_PYENV_COLOR yellow

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show pyenv python version
	[ $SPACEFISH_PYENV_SHOW = false ]; and return

	# Ensure the pyenv command is available
	type -q pyenv; or return

	# Show pyenv python version only for Python-specific folders
	if not test -n "$PYENV_VERSION" \
		-o -f .python-version \
		-o -f requirements.txt \
		-o -f pyproject.toml \
		-o (count *.py) -gt 0
		return
	end

	set -l pyenv_status (pyenv version-name 2>/dev/null) # This line needs explicit testing in an enviroment that has pyenv.

	__sf_lib_section \
		$SPACEFISH_PYENV_COLOR \
		$SPACEFISH_PYENV_PREFIX \
		"$SPACEFISH_PYENV_SYMBOL""$pyenv_status" \
		$SPACEFISH_PYENV_SUFFIX
...
35	84	----> __sf_util_set_default SPACEFISH_PYENV_SHOW true
9	49	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
21	21	------> set -g $var $argv[2..-1]
28	64	----> __sf_util_set_default SPACEFISH_PYENV_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
6	36	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
26	58	----> __sf_util_set_default SPACEFISH_PYENV_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
6	32	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
22	53	----> __sf_util_set_default SPACEFISH_PYENV_SYMBOL "🐍 "
6	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
21	52	----> __sf_util_set_default SPACEFISH_PYENV_COLOR yellow
6	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_PYENV_SHOW = false ]
70	987	----> type -q pyenv
9	9	-----> set -q argv[1]
23	23	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
46	46	-----> argparse -n type -x t,p,P $options -- $argv
3	11	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
8	8	------> set -q _flag_help
10	10	-----> set -l res 1
9	9	-----> set -l mode normal
9	9	-----> set -l multi no
9	9	-----> set -l selection all
8	8	-----> set -l short no
8	23	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	------> set -q _flag_quiet
9	9	------> set mode quiet
6	6	-----> set -q _flag_all
6	6	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
31	730	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
9	9	------> set -l found 0
17	404	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
13	13	-------> test $selection != files
2	98	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
96	96	--------> functions -q -- $i
6	276	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
226	270	--------> contains -- $i (builtin -n)
44	44	---------> builtin -n
16	16	------> set -l paths
11	212	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
15	15	-------> test $multi != yes
110	186	-------> set paths (command -s -- $i)
76	76	--------> command -s -- $i
18	18	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
7	40	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
19	19	-------> test $found = 0
14	14	-------> test $mode != quiet
12	12	-----> return $res
6	6	----> return
36	2635	--> eval __sf_section_$i
197	2599	---> __sf_section_dotnet
249	261	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_dotnet.fish
12	12	-----> function __sf_section_dotnet -d "Display the .NET SDK version"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_DOTNET_SHOW true
	__sf_util_set_default SPACEFISH_DOTNET_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
	__sf_util_set_default SPACEFISH_DOTNET_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_DOTNET_SYMBOL ".NET "
	__sf_util_set_default SPACEFISH_DOTNET_COLOR "af00d7" # 128 in the original version, but renders as blue in iTerm2?

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show current version of .NET SDK
	[ $SPACEFISH_DOTNET_SHOW = false ]; and return

	# Ensure the dotnet command is available
	type -q dotnet; or return

	if not test -f project.json \
		-o -f global.json \
		-o -f paket.dependencies \
		-o (count *.csproj) -gt 0 \
		-o (count *.fsproj) -gt 0 \
		-o (count *.xproj) -gt 0 \
		-o (count *.sln) -gt 0
		return
	end

	# From the
	# dotnet-cli automatically handles SDK pinning (specified in a global.json file)
	# therefore, this already returns the expected version for the current directory
	set -l dotnet_version (dotnet --version 2>/dev/null)

	__sf_lib_section \
		$SPACEFISH_DOTNET_COLOR \
		$SPACEFISH_DOTNET_PREFIX \
		"$SPACEFISH_DOTNET_SYMBOL""$dotnet_version" \
		$SPACEFISH_DOTNET_SUFFIX
...
32	81	----> __sf_util_set_default SPACEFISH_DOTNET_SHOW true
9	49	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
22	22	------> set -g $var $argv[2..-1]
57	103	----> __sf_util_set_default SPACEFISH_DOTNET_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
8	46	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
19	19	------> set -g $var $argv[2..-1]
30	63	----> __sf_util_set_default SPACEFISH_DOTNET_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	33	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
22	55	----> __sf_util_set_default SPACEFISH_DOTNET_SYMBOL ".NET "
7	33	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
22	53	----> __sf_util_set_default SPACEFISH_DOTNET_COLOR "af00d7"
6	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_DOTNET_SHOW = false ]
70	1093	----> type -q dotnet
10	10	-----> set -q argv[1]
23	23	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
45	45	-----> argparse -n type -x t,p,P $options -- $argv
3	12	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
9	9	------> set -q _flag_help
10	10	-----> set -l res 1
10	10	-----> set -l mode normal
9	9	-----> set -l multi no
9	9	-----> set -l selection all
8	8	-----> set -l short no
7	22	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	------> set -q _flag_quiet
9	9	------> set mode quiet
6	6	-----> set -q _flag_all
6	6	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
30	833	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
11	11	------> set -l found 0
20	443	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
12	12	-------> test $selection != files
4	97	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
93	93	--------> functions -q -- $i
15	314	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
250	299	--------> contains -- $i (builtin -n)
49	49	---------> builtin -n
23	23	------> set -l paths
12	209	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
39	39	-------> test $multi != yes
114	158	-------> set paths (command -s -- $i)
44	44	--------> command -s -- $i
27	97	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
15	15	-------> set res 0
17	17	-------> set found 1
12	12	-------> switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            ...
5	26	-------> if test $multi != yes
                continue
            ...
14	14	--------> test $multi != yes
7	7	--------> continue
5	20	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
15	15	-------> test $found = 0
14	14	-----> return $res
10	679	----> if not test -f project.json \
		-o -f global.json \
		-o -f paket.dependencies \
		-o (count *.csproj) -gt 0 \
		-o (count *.fsproj) -gt 0 \
		-o (count *.xproj) -gt 0 \
		-o (count *.sln) -gt 0
		return
	...
378	659	-----> not test -f project.json \
		-o -f global.json \
		-o -f paket.dependencies \
		-o (count *.csproj) -gt 0 \
		-o (count *.fsproj) -gt 0 \
		-o (count *.xproj) -gt 0 \
		-o (count *.sln) -gt 0
82	82	------> count *.csproj
67	67	------> count *.fsproj
67	67	------> count *.xproj
65	65	------> count *.sln
10	10	-----> return
43	2309	--> eval __sf_section_$i
222	2266	---> __sf_section_kubecontext
312	326	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_kubecontext.fish
14	14	-----> function __sf_section_kubecontext -d "Display the kubernetes context"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_KUBECONTEXT_SHOW true
	__sf_util_set_default SPACEFISH_KUBECONTEXT_NAMESPACE_SHOW true
	__sf_util_set_default SPACEFISH_KUBECONTEXT_PREFIX "at "
	__sf_util_set_default SPACEFISH_KUBECONTEXT_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	# Additional space is added because ☸️ is wider than other symbols
	# See: https://github.com/denysdovhan/spaceship-prompt/pull/432
	__sf_util_set_default SPACEFISH_KUBECONTEXT_SYMBOL "☸️  "
	__sf_util_set_default SPACEFISH_KUBECONTEXT_COLOR cyan


	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show current kubecontext
	[ $SPACEFISH_KUBECONTEXT_SHOW = false ]; and return
	# Ensure the kubectl command is available
	type -q kubectl; or return

	set -l kube_context (kubectl config current-context 2>/dev/null)
	[ -z $kube_context ]; and return

	if test "$SPACEFISH_KUBECONTEXT_NAMESPACE_SHOW" = "true" -a "$kube_context" != "default"
		set kube_namespace (kubectl config view --minify --output 'jsonpath={..namespace}' 2>/dev/null)
		set kube_context "$kube_context ($kube_namespace)"
	end

	__sf_lib_section \
		$SPACEFISH_KUBECONTEXT_COLOR \
		$SPACEFISH_KUBECONTEXT_PREFIX \
		"$SPACEFISH_KUBECONTEXT_SYMBOL""$kube_context" \
		$SPACEFISH_KUBECONTEXT_SUFFIX
...
42	100	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_SHOW true
11	58	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
24	24	------> not set -q $var
23	23	------> set -g $var $argv[2..-1]
37	88	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_NAMESPACE_SHOW true
10	51	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
26	26	------> set -g $var $argv[2..-1]
30	82	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_PREFIX "at "
10	52	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
30	30	------> set -g $var $argv[2..-1]
32	83	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
9	51	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
27	27	------> set -g $var $argv[2..-1]
51	102	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_SYMBOL "☸️  "
9	51	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
22	22	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
37	74	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_COLOR cyan
6	37	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
23	23	----> [ $SPACEFISH_KUBECONTEXT_SHOW = false ]
78	1161	----> type -q kubectl
10	10	-----> set -q argv[1]
35	35	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
50	50	-----> argparse -n type -x t,p,P $options -- $argv
5	20	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
15	15	------> set -q _flag_help
14	14	-----> set -l res 1
11	11	-----> set -l mode normal
9	9	-----> set -l multi no
10	10	-----> set -l selection all
16	16	-----> set -l short no
8	26	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
8	8	------> set -q _flag_quiet
10	10	------> set mode quiet
7	7	-----> set -q _flag_all
7	7	-----> set -q _flag_short
10	10	-----> set -q _flag_no_functions
59	847	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
14	14	------> set -l found 0
24	485	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
14	14	-------> test $selection != files
4	125	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
121	121	--------> functions -q -- $i
6	322	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
259	316	--------> contains -- $i (builtin -n)
57	57	---------> builtin -n
24	24	------> set -l paths
10	219	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
16	16	-------> test $multi != yes
113	193	-------> set paths (command -s -- $i)
80	80	--------> command -s -- $i
14	14	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
5	32	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
15	15	-------> test $found = 0
12	12	-------> test $mode != quiet
11	11	-----> return $res
5	5	----> return
32	959	--> eval __sf_section_$i
189	927	---> __sf_section_exec_time
203	216	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_exec_time.fish
13	13	-----> function __sf_section_exec_time -d "Display the execution time of the last command"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_EXEC_TIME_SHOW true
	__sf_util_set_default SPACEFISH_EXEC_TIME_PREFIX "took "
	__sf_util_set_default SPACEFISH_EXEC_TIME_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_EXEC_TIME_COLOR yellow
	__sf_util_set_default SPACEFISH_EXEC_TIME_ELAPSED 5

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_EXEC_TIME_SHOW = false ]; and return

	# Allow for compatibility between fish 2.7 and 3.0
	set -l command_duration "$CMD_DURATION$cmd_duration"

	if test -n "$command_duration" -a "$command_duration" -gt (math "$SPACEFISH_EXEC_TIME_ELAPSED * 1000")
		set -l human_command_duration (echo $command_duration | __sf_util_human_time)
		__sf_lib_section \
			$SPACEFISH_EXEC_TIME_COLOR \
			$SPACEFISH_EXEC_TIME_PREFIX \
			$human_command_duration \
			$SPACEFISH_EXEC_TIME_SUFFIX
	end
...
31	80	----> __sf_util_set_default SPACEFISH_EXEC_TIME_SHOW true
8	49	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
22	22	------> set -g $var $argv[2..-1]
25	62	----> __sf_util_set_default SPACEFISH_EXEC_TIME_PREFIX "took "
6	37	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
19	19	------> set -g $var $argv[2..-1]
40	74	----> __sf_util_set_default SPACEFISH_EXEC_TIME_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
6	34	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
25	62	----> __sf_util_set_default SPACEFISH_EXEC_TIME_COLOR yellow
6	37	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
21	53	----> __sf_util_set_default SPACEFISH_EXEC_TIME_ELAPSED 5
6	32	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_EXEC_TIME_SHOW = false ]
17	17	----> set -l command_duration "$CMD_DURATION$cmd_duration"
4	160	----> if test -n "$command_duration" -a "$command_duration" -gt (math "$SPACEFISH_EXEC_TIME_ELAPSED * 1000")
		set -l human_command_duration (echo $command_duration | __sf_util_human_time)
		__sf_lib_section \
			$SPACEFISH_EXEC_TIME_COLOR \
			$SPACEFISH_EXEC_TIME_PREFIX \
			$human_command_duration \
			$SPACEFISH_EXEC_TIME_SUFFIX
	...
110	156	-----> test -n "$command_duration" -a "$command_duration" -gt (math "$SPACEFISH_EXEC_TIME_ELAPSED * 1000")
46	46	------> math "$SPACEFISH_EXEC_TIME_ELAPSED * 1000"
32	456	--> eval __sf_section_$i
173	424	---> __sf_section_line_sep
123	135	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_line_sep.fish
12	12	-----> function __sf_section_line_sep -d "Separate the prompt into two lines"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_PROMPT_SEPARATE_LINE true

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	if test "$SPACEFISH_PROMPT_SEPARATE_LINE" = "true"
		echo -e -n \n
	end
...
30	76	----> __sf_util_set_default SPACEFISH_PROMPT_SEPARATE_LINE true
9	46	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
7	40	----> if test "$SPACEFISH_PROMPT_SEPARATE_LINE" = "true"
		echo -e -n \n
	...
16	16	-----> test "$SPACEFISH_PROMPT_SEPARATE_LINE" = "true"
17	17	-----> echo -e -n \n
28	44260	--> eval __sf_section_$i
253	44232	---> __sf_section_battery
634	648	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_battery.fish
14	14	-----> function __sf_section_battery -d "Displays battery symbol and charge"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	# ------------------------------------------------------------------------------
	# | SPACEFISH_BATTERY_SHOW | below threshold | above threshold | fully charged |
	# |------------------------+-----------------+-----------------+---------------|
	# | false                  | hidden          | hidden          | hidden        |
	# | always                 | shown           | shown           | shown         |
	# | true                   | shown           | hidden          | hidden        |
	# | charged                | shown           | hidden          | shown         |
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_BATTERY_SHOW true
	__sf_util_set_default SPACEFISH_BATTERY_PREFIX ""
	__sf_util_set_default SPACEFISH_BATTERY_SUFFIX " "
	__sf_util_set_default SPACEFISH_BATTERY_SYMBOL_CHARGING ⇡
	__sf_util_set_default SPACEFISH_BATTERY_SYMBOL_DISCHARGING ⇣
	__sf_util_set_default SPACEFISH_BATTERY_SYMBOL_FULL •
	__sf_util_set_default SPACEFISH_BATTERY_THRESHOLD 10

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Show section only if any of the following is true
	# - SPACEFISH_BATTERY_SHOW = "always"
	# - SPACEFISH_BATTERY_SHOW = "true" and
	#	- battery percentage is below the given limit (default: 10%)
	# - SPACEFISH_BATTERY_SHOW = "charged" and
	#	- Battery is fully charged

	# Check that user wants to show battery levels
	[ $SPACEFISH_BATTERY_SHOW = false ]; and return

	set -l battery_data
	set -l battery_percent
	set -l battery_status
	set -l battery_color
	set -l battery_symbol

	# Darwin and macOS machines
	if type -q pmset
		set battery_data (pmset -g batt | grep "InternalBattery")

		# Return if no internal battery
		if test -z (echo $battery_data)
			return
		end

		set battery_percent (echo $battery_data | grep -oE "[0-9]{1,3}%")
		# spaceship has echo $battery_data | awk -F '; *' 'NR==2 { print $2 }', but NR==2 did not return anything.
		set battery_status (echo $battery_data | awk -F '; *' '{ print $2 }')

	# Linux machines
	else if type -q upower
		set -l battery (upower -e | grep battery | head -1)

		[ -z $battery ]; and return

		set -l IFS # Clear IFS to allow for multi-line variables
		set battery_data (upower -i $battery)
		set battery_percent (echo $battery_data | grep percentage | awk '{print $2}')
		set battery_status (echo $battery_data | grep state | awk '{print $2}')

	# Windows machines.
	else if type -q acpi
		set -l battery_data (acpi -b 2>/dev/null | head -1)

		# Return if no battery
		[ -z $battery_data ]; and return

		set battery_percent ( echo $battery_data | awk '{print $4}' )
		set battery_status ( echo $battery_data | awk '{print tolower($3)}' )
	else
		return
	end

	 # Remove trailing % and symbols for comparison
	set battery_percent (echo $battery_percent | string trim --chars=%[,;])

	if test "$battery_percent" -eq 100 -o -n (echo (string match -r "(charged|full)" $battery_status))
		set battery_color green
	else if test "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD"
		set battery_color red
	else
		set battery_color yellow
	end

	# Battery indicator based on current status of battery
	if test "$battery_status" = "charging"
		set battery_symbol $SPACEFISH_BATTERY_SYMBOL_CHARGING
	else if test -n (echo (string match -r "^[dD]ischarg.*" $battery_status))
		set battery_symbol $SPACEFISH_BATTERY_SYMBOL_DISCHARGING
	else
		set battery_symbol $SPACEFISH_BATTERY_SYMBOL_FULL
	end

	if test "$SPACEFISH_BATTERY_SHOW" = "always" \
	-o "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD" \
	-o "$SPACEFISH_BATTERY_SHOW" = "charged" \
	-a -n (echo (string match -r "(charged|full)" $battery_status))
		__sf_lib_section \
			$battery_color \
			$SPACEFISH_BATTERY_PREFIX \
			"$battery_symbol$battery_percent%" \
			$SPACEFISH_BATTERY_SUFFIX
	end
...
36	86	----> __sf_util_set_default SPACEFISH_BATTERY_SHOW true
10	50	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
21	21	------> set -g $var $argv[2..-1]
27	63	----> __sf_util_set_default SPACEFISH_BATTERY_PREFIX ""
6	36	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
18	18	------> set -g $var $argv[2..-1]
25	58	----> __sf_util_set_default SPACEFISH_BATTERY_SUFFIX " "
6	33	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
21	54	----> __sf_util_set_default SPACEFISH_BATTERY_SYMBOL_CHARGING ⇡
7	33	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
21	52	----> __sf_util_set_default SPACEFISH_BATTERY_SYMBOL_DISCHARGING ⇣
5	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
36	72	----> __sf_util_set_default SPACEFISH_BATTERY_SYMBOL_FULL •
6	36	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
25	82	----> __sf_util_set_default SPACEFISH_BATTERY_THRESHOLD 10
10	57	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
27	27	------> set -g $var $argv[2..-1]
22	22	----> [ $SPACEFISH_BATTERY_SHOW = false ]
16	16	----> set -l battery_data
15	15	----> set -l battery_percent
13	13	----> set -l battery_status
12	12	----> set -l battery_color
13	13	----> set -l battery_symbol
42	41821	----> if type -q pmset
		set battery_data (pmset -g batt | grep "InternalBattery")

		# Return if no internal battery
		if test -z (echo $battery_data)
			return
		end

		set battery_percent (echo $battery_data | grep -oE "[0-9]{1,3}%")
		# spaceship has echo $battery_data | awk -F '; *' 'NR==2 { print $2 }', but NR==2 did not return anything.
		set battery_status (echo $battery_data | awk -F '; *' '{ print $2 }')

	# Linux machines
	else if type -q upower
		set -l battery (upower -e | grep battery | head -1)

		[ -z $battery ]; and return

		set -l IFS # Clear IFS to allow for multi-line variables
		set battery_data (upower -i $battery)
		set battery_percent (echo $battery_data | grep percentage | awk '{print $2}')
		set battery_status (echo $battery_data | grep state | awk '{print $2}')

	# Windows machines.
	else if type -q acpi
		set -l battery_data (acpi -b 2>/dev/null | head -1)

		# Return if no battery
		[ -z $battery_data ]; and return

		set battery_percent ( echo $battery_data | awk '{print $4}' )
		set battery_status ( echo $battery_data | awk '{print tolower($3)}' )
	else
		return
	...
90	2132	-----> type -q pmset
17	17	------> set -q argv[1]
26	26	------> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
59	59	------> argparse -n type -x t,p,P $options -- $argv
5	18	------> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
13	13	-------> set -q _flag_help
18	18	------> set -l res 1
12	12	------> set -l mode normal
9	9	------> set -l multi no
9	9	------> set -l selection all
8	8	------> set -l short no
14	33	------> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	-------> set -q _flag_quiet
12	12	-------> set mode quiet
9	9	------> set -q _flag_all
8	8	------> set -q _flag_short
13	13	------> set -q _flag_no_functions
85	1791	------> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
42	42	-------> set -l found 0
45	994	-------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
51	51	--------> test $selection != files
8	191	--------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
183	183	---------> functions -q -- $i
17	707	--------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
577	690	---------> contains -- $i (builtin -n)
113	113	----------> builtin -n
59	59	-------> set -l paths
23	557	-------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
51	51	--------> test $multi != yes
382	483	--------> set paths (command -s -- $i)
101	101	---------> command -s -- $i
16	16	-------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
7	38	-------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
18	18	--------> test $found = 0
13	13	--------> test $mode != quiet
12	12	------> return $res
86	1144	-----> type -q upower
12	12	------> set -q argv[1]
25	25	------> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
52	52	------> argparse -n type -x t,p,P $options -- $argv
4	14	------> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
10	10	-------> set -q _flag_help
12	12	------> set -l res 1
56	56	------> set -l mode normal
15	15	------> set -l multi no
13	13	------> set -l selection all
15	15	------> set -l short no
9	31	------> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
11	11	-------> set -q _flag_quiet
11	11	-------> set mode quiet
7	7	------> set -q _flag_all
6	6	------> set -q _flag_short
6	6	------> set -q _flag_no_functions
35	780	------> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
11	11	-------> set -l found 0
10	418	-------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
13	13	--------> test $selection != files
3	97	--------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
94	94	---------> functions -q -- $i
6	298	--------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
247	292	---------> contains -- $i (builtin -n)
45	45	----------> builtin -n
25	25	-------> set -l paths
9	172	-------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
16	16	--------> test $multi != yes
110	147	--------> set paths (command -s -- $i)
37	37	---------> command -s -- $i
27	96	-------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
18	18	--------> set res 0
16	16	--------> set found 1
10	10	--------> switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            ...
7	25	--------> if test $multi != yes
                continue
            ...
12	12	---------> test $multi != yes
6	6	---------> continue
6	23	-------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
17	17	--------> test $found = 0
14	14	------> return $res
165	17973	-----> set -l battery (upower -e | grep battery | head -1)
777	17808	------> upower -e | grep battery | head -1
108	3471	-------> source /usr/share/fish/functions/grep.fish
15	3363	--------> if echo | command grep --color=auto "" >/dev/null 2>&1
    function grep
        command grep --color=auto $argv
    end
...
3333	3333	---------> echo | command grep --color=auto "" >/dev/null 2>&1
15	15	---------> function grep
        command grep --color=auto $argv
    ...
13560	13560	-------> command grep --color=auto $argv
26	26	-----> [ -z $battery ]
13	13	-----> set -l IFS
322	10278	-----> set battery_data (upower -i $battery)
9956	9956	------> upower -i $battery
197	5516	-----> set battery_percent (echo $battery_data | grep percentage | awk '{print $2}')
1912	5319	------> echo $battery_data | grep percentage | awk '{print $2}'
3407	3407	-------> command grep --color=auto $argv
180	4697	-----> set battery_status (echo $battery_data | grep state | awk '{print $2}')
803	4517	------> echo $battery_data | grep state | awk '{print $2}'
3714	3714	-------> command grep --color=auto $argv
152	266	----> set battery_percent (echo $battery_percent | string trim --chars=%[,;])
114	114	-----> echo $battery_percent | string trim --chars=%[,;]
14	333	----> if test "$battery_percent" -eq 100 -o -n (echo (string match -r "(charged|full)" $battery_status))
		set battery_color green
	else if test "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD"
		set battery_color red
	else
		set battery_color yellow
	...
114	274	-----> test "$battery_percent" -eq 100 -o -n (echo (string match -r "(charged|full)" $battery_status))
102	160	------> echo (string match -r "(charged|full)" $battery_status)
58	58	-------> string match -r "(charged|full)" $battery_status
29	29	-----> test "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD"
16	16	-----> set battery_color yellow
12	55	----> if test "$battery_status" = "charging"
		set battery_symbol $SPACEFISH_BATTERY_SYMBOL_CHARGING
	else if test -n (echo (string match -r "^[dD]ischarg.*" $battery_status))
		set battery_symbol $SPACEFISH_BATTERY_SYMBOL_DISCHARGING
	else
		set battery_symbol $SPACEFISH_BATTERY_SYMBOL_FULL
	...
24	24	-----> test "$battery_status" = "charging"
19	19	-----> set battery_symbol $SPACEFISH_BATTERY_SYMBOL_CHARGING
7	298	----> if test "$SPACEFISH_BATTERY_SHOW" = "always" \
	-o "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD" \
	-o "$SPACEFISH_BATTERY_SHOW" = "charged" \
	-a -n (echo (string match -r "(charged|full)" $battery_status))
		__sf_lib_section \
			$battery_color \
			$SPACEFISH_BATTERY_PREFIX \
			"$battery_symbol$battery_percent%" \
			$SPACEFISH_BATTERY_SUFFIX
	...
148	291	-----> test "$SPACEFISH_BATTERY_SHOW" = "always" \
	-o "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD" \
	-o "$SPACEFISH_BATTERY_SHOW" = "charged" \
	-a -n (echo (string match -r "(charged|full)" $battery_status))
102	143	------> echo (string match -r "(charged|full)" $battery_status)
41	41	-------> string match -r "(charged|full)" $battery_status
46	1361	--> eval __sf_section_$i
228	1315	---> __sf_section_vi_mode
317	332	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_vi_mode.fish
15	15	-----> function __sf_section_vi_mode -d "Display vi mode status"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_VI_MODE_SHOW true
	__sf_util_set_default SPACEFISH_VI_MODE_PREFIX " "
	__sf_util_set_default SPACEFISH_VI_MODE_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
	__sf_util_set_default SPACEFISH_VI_MODE_INSERT [I]
	__sf_util_set_default SPACEFISH_VI_MODE_NORMAL [N]
	__sf_util_set_default SPACEFISH_VI_MODE_VISUAL [V]
	__sf_util_set_default SPACEFISH_VI_MODE_REPLACE_ONE [R]
	__sf_util_set_default SPACEFISH_VI_MODE_COLOR white

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_VI_MODE_SHOW = false ]; and return

	# Ensure fish_vi_key_bindings or fish_hybrid_key_bindings are used
	# Here we are trying to be compatible with default fish_mode_prompt implementation,
	# wich handle both "fish_vi_key_bindings" and "fish_hybrid_key_bindings"
	[ "$fish_key_bindings" = "fish_vi_key_bindings" ]; or [ "$fish_key_bindings" = "fish_hybrid_key_bindings" ]; or return

	# Use `set -l` to define local variables to avoid populating
  	# the global namespace
	set -l vi_mode_symbol

	# Check current mode and set vi_mode_symbol based on it
	switch $fish_bind_mode
		case default
			set vi_mode_symbol $SPACEFISH_VI_MODE_NORMAL
		case insert
			set vi_mode_symbol $SPACEFISH_VI_MODE_INSERT
		case replace_one
			set vi_mode_symbol $SPACEFISH_VI_MODE_REPLACE_ONE
		case visual
			set vi_mode_symbol $SPACEFISH_VI_MODE_VISUAL
	end

	__sf_lib_section \
		$SPACEFISH_VI_MODE_COLOR \
		$SPACEFISH_VI_MODE_PREFIX \
		$vi_mode_symbol \
		$SPACEFISH_VI_MODE_SUFFIX
...
49	111	----> __sf_util_set_default SPACEFISH_VI_MODE_SHOW true
11	62	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
22	22	------> not set -q $var
29	29	------> set -g $var $argv[2..-1]
32	88	----> __sf_util_set_default SPACEFISH_VI_MODE_PREFIX " "
11	56	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
30	30	------> set -g $var $argv[2..-1]
33	85	----> __sf_util_set_default SPACEFISH_VI_MODE_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
11	52	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
22	22	------> set -g $var $argv[2..-1]
31	108	----> __sf_util_set_default SPACEFISH_VI_MODE_INSERT [I]
10	77	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
47	47	------> set -g $var $argv[2..-1]
29	81	----> __sf_util_set_default SPACEFISH_VI_MODE_NORMAL [N]
10	52	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
25	25	------> set -g $var $argv[2..-1]
23	73	----> __sf_util_set_default SPACEFISH_VI_MODE_VISUAL [V]
9	50	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
24	24	------> set -g $var $argv[2..-1]
24	75	----> __sf_util_set_default SPACEFISH_VI_MODE_REPLACE_ONE [R]
10	51	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
24	24	------> set -g $var $argv[2..-1]
23	74	----> __sf_util_set_default SPACEFISH_VI_MODE_COLOR white
11	51	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
22	22	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_VI_MODE_SHOW = false ]
21	21	----> [ "$fish_key_bindings" = "fish_vi_key_bindings" ]
18	18	----> [ "$fish_key_bindings" = "fish_hybrid_key_bindings" ]
7	7	----> return
41	3450	--> eval __sf_section_$i
261	3409	---> __sf_section_jobs
297	310	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_jobs.fish
13	13	-----> function __sf_section_jobs -d "Show icon, if there's a working jobs in the background."
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_JOBS_SHOW true
	__sf_util_set_default SPACEFISH_JOBS_PREFIX ""
	__sf_util_set_default SPACEFISH_JOBS_SUFFIX " "
	__sf_util_set_default SPACEFISH_JOBS_SYMBOL ✦
	__sf_util_set_default SPACEFISH_JOBS_COLOR blue
	__sf_util_set_default SPACEFISH_JOBS_AMOUNT_PREFIX ""
	__sf_util_set_default SPACEFISH_JOBS_AMOUNT_SUFFIX ""
	__sf_util_set_default SPACEFISH_JOBS_AMOUNT_THRESHOLD 1

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_JOBS_SHOW = false ]; and return

	set jobs_amount (jobs | wc -l | xargs) # Zsh had a much more complicated command.

	if test $jobs_amount -eq 0
		return
	end

	if test $jobs_amount -le $SPACEFISH_JOBS_AMOUNT_THRESHOLD
		set jobs_amount ''
		set SPACEFISH_JOBS_AMOUNT_PREFIX ''
		set SPACEFISH_JOBS_AMOUNT_SUFFIX ''
	end

	set SPACEFISH_JOBS_SECTION "$SPACEFISH_JOBS_SYMBOL$SPACEFISH_JOBS_AMOUNT_PREFIX$jobs_amount$SPACEFISH_JOBS_AMOUNT_SUFFIX"

	__sf_lib_section \
		$SPACEFISH_JOBS_COLOR \
		$SPACEFISH_JOBS_PREFIX \
		$SPACEFISH_JOBS_SECTION \
		$SPACEFISH_JOBS_SUFFIX
...
40	104	----> __sf_util_set_default SPACEFISH_JOBS_SHOW true
11	64	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
22	22	------> not set -q $var
31	31	------> set -g $var $argv[2..-1]
34	84	----> __sf_util_set_default SPACEFISH_JOBS_PREFIX ""
9	50	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	------> not set -q $var
20	20	------> set -g $var $argv[2..-1]
37	79	----> __sf_util_set_default SPACEFISH_JOBS_SUFFIX " "
9	42	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
36	74	----> __sf_util_set_default SPACEFISH_JOBS_SYMBOL ✦
8	38	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
36	74	----> __sf_util_set_default SPACEFISH_JOBS_COLOR blue
8	38	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
37	74	----> __sf_util_set_default SPACEFISH_JOBS_AMOUNT_PREFIX ""
8	37	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
37	72	----> __sf_util_set_default SPACEFISH_JOBS_AMOUNT_SUFFIX ""
6	35	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
35	72	----> __sf_util_set_default SPACEFISH_JOBS_AMOUNT_THRESHOLD 1
6	37	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
22	22	----> [ $SPACEFISH_JOBS_SHOW = false ]
128	2147	----> set jobs_amount (jobs | wc -l | xargs)
2019	2019	-----> jobs | wc -l | xargs
8	36	----> if test $jobs_amount -eq 0
		return
	...
21	21	-----> test $jobs_amount -eq 0
7	7	-----> return
34	747	--> eval __sf_section_$i
203	713	---> __sf_section_exit_code
167	181	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_exit_code.fish
14	14	-----> function __sf_section_exit_code -d "Shows the exit code from the previous command."
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_EXIT_CODE_SHOW false
	__sf_util_set_default SPACEFISH_EXIT_CODE_PREFIX ""
	__sf_util_set_default SPACEFISH_EXIT_CODE_SUFFIX " "
	__sf_util_set_default SPACEFISH_EXIT_CODE_SYMBOL ✘
	__sf_util_set_default SPACEFISH_EXIT_CODE_COLOR red

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	[ $SPACEFISH_EXIT_CODE_SHOW = false ]; or test $sf_exit_code -eq 0; and return

	__sf_lib_section \
		$SPACEFISH_EXIT_CODE_COLOR \
		$SPACEFISH_EXIT_CODE_PREFIX \
		"$SPACEFISH_EXIT_CODE_SYMBOL$sf_exit_code" \
		$SPACEFISH_EXIT_CODE_SUFFIX
...
31	81	----> __sf_util_set_default SPACEFISH_EXIT_CODE_SHOW false
9	50	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
22	22	------> set -g $var $argv[2..-1]
28	65	----> __sf_util_set_default SPACEFISH_EXIT_CODE_PREFIX ""
6	37	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
19	19	------> set -g $var $argv[2..-1]
25	59	----> __sf_util_set_default SPACEFISH_EXIT_CODE_SUFFIX " "
7	34	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
17	17	------> set -g $var $argv[2..-1]
21	52	----> __sf_util_set_default SPACEFISH_EXIT_CODE_SYMBOL ✘
6	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
21	52	----> __sf_util_set_default SPACEFISH_EXIT_CODE_COLOR red
5	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
14	14	----> [ $SPACEFISH_EXIT_CODE_SHOW = false ]
6	6	----> return
30	1197	--> eval __sf_section_$i
216	1167	---> __sf_section_char
178	191	----> source /home/micro-p/.local/share/omf/themes/spacefish/functions/__sf_section_char.fish
13	13	-----> function __sf_section_char -d "Display the prompt character"
	# ------------------------------------------------------------------------------
	# Configuration
	# ------------------------------------------------------------------------------

	__sf_util_set_default SPACEFISH_CHAR_PREFIX ""
	__sf_util_set_default SPACEFISH_CHAR_SUFFIX " "
	__sf_util_set_default SPACEFISH_CHAR_SYMBOL ➜
	__sf_util_set_default SPACEFISH_CHAR_COLOR_SUCCESS green
	__sf_util_set_default SPACEFISH_CHAR_COLOR_FAILURE red

	# ------------------------------------------------------------------------------
	# Section
	# ------------------------------------------------------------------------------

	# Color $SPACEFISH_CHAR_SYMBOL red if previous command failed and
	# color it in green if the command succeeded.
	set -l color

	if test $sf_exit_code -eq 0
		set color $SPACEFISH_CHAR_COLOR_SUCCESS
	else
		set color $SPACEFISH_CHAR_COLOR_FAILURE
	end

	__sf_lib_section \
		$color \
		$SPACEFISH_CHAR_PREFIX \
		$SPACEFISH_CHAR_SYMBOL \
		$SPACEFISH_CHAR_SUFFIX
...
34	82	----> __sf_util_set_default SPACEFISH_CHAR_PREFIX ""
9	48	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
21	21	------> set -g $var $argv[2..-1]
25	60	----> __sf_util_set_default SPACEFISH_CHAR_SUFFIX " "
8	35	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
22	52	----> __sf_util_set_default SPACEFISH_CHAR_SYMBOL ➜
5	30	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
15	15	------> set -g $var $argv[2..-1]
21	53	----> __sf_util_set_default SPACEFISH_CHAR_COLOR_SUCCESS green
6	32	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
22	52	----> __sf_util_set_default SPACEFISH_CHAR_COLOR_FAILURE red
5	30	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
16	16	------> set -g $var $argv[2..-1]
10	10	----> set -l color
7	34	----> if test $sf_exit_code -eq 0
		set color $SPACEFISH_CHAR_COLOR_SUCCESS
	else
		set color $SPACEFISH_CHAR_COLOR_FAILURE
	...
14	14	-----> test $sf_exit_code -eq 0
13	13	-----> set color $SPACEFISH_CHAR_COLOR_SUCCESS
60	417	----> __sf_lib_section \
		$color \
		$SPACEFISH_CHAR_PREFIX \
		$SPACEFISH_CHAR_SYMBOL \
		$SPACEFISH_CHAR_SUFFIX
5	132	-----> if test (count $argv) -eq 2
		set content $argv[2]
		set prefix
	...
94	127	------> test (count $argv) -eq 2
33	33	-------> count $argv
11	92	-----> if test "$sf_prompt_opened" = "true" -a "$SPACEFISH_PROMPT_PREFIXES_SHOW" = "true"
		# Echo prefixes in bold white
		set_color --bold
		echo -e -n -s $prefix
		set_color normal
	...
28	28	------> test "$sf_prompt_opened" = "true" -a "$SPACEFISH_PROMPT_PREFIXES_SHOW" = "true"
22	22	------> set_color --bold
17	17	------> echo -e -n -s $prefix
14	14	------> set_color normal
15	15	-----> set -g sf_prompt_opened true
25	25	-----> set_color --bold $color
16	16	-----> echo -e -n $content
13	13	-----> set_color normal
11	64	-----> if test "$SPACEFISH_PROMPT_SUFFIXES_SHOW" = "true"
		# Echo suffixes in bold white
		set_color --bold
		echo -e -n -s $suffix
		set_color normal
	...
14	14	------> test "$SPACEFISH_PROMPT_SUFFIXES_SHOW" = "true"
13	13	------> set_color --bold
14	14	------> echo -e -n -s $suffix
12	12	------> set_color normal
15	15	-> set_color normal
34	165	> fish_right_prompt
31	98	-> __sf_util_set_default SPACEFISH_RPROMPT_ORDER ""
12	67	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
29	29	---> not set -q $var
26	26	---> set -g $var $argv[2..-1]
24	24	-> [ -n "$SPACEFISH_RPROMPT_ORDER" ]
9	9	-> return
27	288	> fish_title
9	261	-> if not set -q INSIDE_EMACS
        echo (status current-command) (__fish_pwd)
    ...
13	13	--> not set -q INSIDE_EMACS
171	239	--> echo (status current-command) (__fish_pwd)
17	17	---> status current-command
31	51	---> __fish_pwd
20	20	----> pwd
560	591	> source /usr/share/fish/functions/cd.fish
31	31	-> function cd --description "Change directory"
    set -l MAX_DIR_HIST 25

    if test (count $argv) -gt (test "$argv[1]" = "--" && echo 2 || echo 1)
        printf "%s\n" (_ "Too many args for cd command")
        return 1
    end

    # Skip history in subshells.
    if status --is-command-substitution
        builtin cd $argv
        return $status
    end

    # Avoid set completions.
    set -l previous $PWD

    if test "$argv" = "-"
        if test "$__fish_cd_direction" = "next"
            nextd
        else
            prevd
        end
        return $status
    end

    # allow explicit "cd ." if the mount-point became stale in the meantime
    if test "$argv" = "."
        cd "$PWD"
        return $status
    end

    builtin cd $argv
    set -l cd_status $status

    if test $cd_status -eq 0 -a "$PWD" != "$previous"
        set -q dirprev
        or set -l dirprev
        set -q dirprev[$MAX_DIR_HIST]
        and set -e dirprev[1]

        # If dirprev, dirnext, __fish_cd_direction
        # are set as universal variables, honor their scope.

        set -U -q dirprev
        and set -U -a dirprev $previous
        or set -g -a dirprev $previous

        set -U -q dirnext
        and set -U -e dirnext
        or set -e dirnext

        set -U -q __fish_cd_direction
        and set -U __fish_cd_direction prev
        or set -g __fish_cd_direction prev
    end

    return $cd_status
...
117	184	> source /usr/share/fish/completions/cd.fish
44	44	-> complete -c cd -a "(__fish_complete_cd)"
23	23	-> complete -c cd -s h -l help -d 'Display help and exit'
322	735	> __fish_complete_cd
311	325	-> source /usr/share/fish/functions/__fish_complete_cd.fish
14	14	--> function __fish_complete_cd -d "Completions for the cd command"
    set -q CDPATH[1]
    or return 0 # no CDPATH so rely solely on the core file name completions

    set -l token (commandline -ct)
    if string match -qr '^\.{0,2}/.*' -- $token
        # Absolute path or explicitly relative to the current directory. Rely on the builtin file
        # name completions since we no longer exclude them from the `cd` argument completion.
        return
    end

    # Relative path. Check $CDPATH and use that as the description for any possible matches.
    # We deliberately exclude the `.` path because the core file name completion logic will include
    # it when presenting possible matches.
    set -l cdpath (string match -v '.' -- $CDPATH)

    # Remove the CWD if it is in CDPATH since, again, the core file name completion logic will
    # handle it.
    set -l cdpath (string match -v -- $PWD $cdpath)
    set -q cdpath[1]
    or return 0

    # TODO: There's a subtlety regarding descriptions - if $cdpath[1]/foo and $cdpath[2]/foo
    # exist, we print both but want the first description to win - this currently works, but
    # is not guaranteed.
    for cdpath in $cdpath
        # Replace $HOME with "~".
        set -l desc (string replace -r -- "^$HOME" "~" "$cdpath")
        # This assumes the CDPATH component itself is cd-able.
        for d in $cdpath/$token*/
            # Remove the cdpath component again.
            test -x $d
            and printf "%s\tCDPATH %s\n" (string replace -r "^$cdpath/" "" -- $d) $desc
        end
    end
...
60	60	-> set -q CDPATH[1]
28	28	-> return 0
70	126	> __fish_complete_cd
41	41	-> set -q CDPATH[1]
15	15	-> return 0
335	931	> up-or-search
227	251	-> source /usr/share/fish/functions/up-or-search.fish
24	24	--> function up-or-search -d "Depending on cursor position and current mode, either search backward or move up one line"
    # If we are already in search mode, continue
    if commandline --search-mode
        commandline -f history-search-backward
        return
    end

    # If we are navigating the pager, then up always navigates
    if commandline --paging-mode
        commandline -f up-line
        return
    end

    # We are not already in search mode.
    # If we are on the top line, start search mode,
    # otherwise move up
    set lineno (commandline -L)

    switch $lineno
        case 1
            commandline -f history-search-backward

        case '*'
            commandline -f up-line
    end
...
10	129	-> if commandline --search-mode
        commandline -f history-search-backward
        return
    ...
119	119	--> commandline --search-mode
5	25	-> if commandline --paging-mode
        commandline -f up-line
        return
    ...
20	20	--> commandline --paging-mode
128	156	-> set lineno (commandline -L)
28	28	--> commandline -L
16	35	-> switch $lineno
        case 1
            commandline -f history-search-backward

        case '*'
            commandline -f up-line
    ...
19	19	--> commandline -f history-search-backward
61	118	> __fish_disable_bracketed_paste 'fish --profile'
57	57	-> printf "\e[?2004l"
46	439	> fish_title fish\ --profile
13	393	-> if not set -q INSIDE_EMACS
        echo (status current-command) (__fish_pwd)
    ...
19	19	--> not set -q INSIDE_EMACS
267	361	--> echo (status current-command) (__fish_pwd)
21	21	---> status current-command
42	73	---> __fish_pwd
31	31	----> pwd
3424	3424	> fish --profile
54	106	> __fish_enable_bracketed_paste
52	52	-> printf "\e[?2004h"
36	36	> fish_mode_prompt
100	85962	> fish_prompt
61	61	-> set -g sf_exit_code $status
30	30	-> set -g SPACEFISH_VERSION 2.7.0
78	115	-> __sf_util_set_default SPACEFISH_PROMPT_ADD_NEWLINE false
7	37	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
30	30	---> not set -q $var
50	82	-> __sf_util_set_default SPACEFISH_PROMPT_FIRST_PREFIX_SHOW false
6	32	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
26	26	---> not set -q $var
32	51	-> __sf_util_set_default SPACEFISH_PROMPT_PREFIXES_SHOW true
4	19	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	---> not set -q $var
22	35	-> __sf_util_set_default SPACEFISH_PROMPT_SUFFIXES_SHOW true
3	13	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	---> not set -q $var
31	56	-> __sf_util_set_default SPACEFISH_PROMPT_DEFAULT_PREFIX "via "
5	25	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	---> not set -q $var
41	68	-> __sf_util_set_default SPACEFISH_PROMPT_DEFAULT_SUFFIX " "
6	27	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	---> not set -q $var
106	128	-> __sf_util_set_default SPACEFISH_PROMPT_ORDER time user dir host git package node ruby golang php rust haskell julia elixir docker aws venv conda pyenv dotnet kubecontext exec_time line_sep battery vi_mode jobs exit_code char
5	22	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	---> not set -q $var
26	26	-> set -g sf_prompt_opened $SPACEFISH_PROMPT_FIRST_PREFIX_SHOW
3	20	-> if test "$SPACEFISH_PROMPT_ADD_NEWLINE" = "true"
		echo
	...
17	17	--> test "$SPACEFISH_PROMPT_ADD_NEWLINE" = "true"
261	85163	-> for i in $SPACEFISH_PROMPT_ORDER
		eval __sf_section_$i
	...
37	372	--> eval __sf_section_$i
51	335	---> __sf_section_time
29	53	----> __sf_util_set_default SPACEFISH_TIME_SHOW false
5	24	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
22	36	----> __sf_util_set_default SPACEFISH_DATE_SHOW false
2	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
22	34	----> __sf_util_set_default SPACEFISH_TIME_PREFIX "at "
2	12	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
26	37	----> __sf_util_set_default SPACEFISH_TIME_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
2	11	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
19	30	----> __sf_util_set_default SPACEFISH_TIME_FORMAT false
2	11	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
20	32	----> __sf_util_set_default SPACEFISH_TIME_12HR false
3	12	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
20	33	----> __sf_util_set_default SPACEFISH_TIME_COLOR "yellow"
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
13	13	----> [ $SPACEFISH_TIME_SHOW = false ]
16	16	----> return
31	328	--> eval __sf_section_$i
38	297	---> __sf_section_user
31	46	----> __sf_util_set_default SPACEFISH_USER_SHOW true
3	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
22	36	----> __sf_util_set_default SPACEFISH_USER_PREFIX "with "
3	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
22	34	----> __sf_util_set_default SPACEFISH_USER_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
2	12	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
20	33	----> __sf_util_set_default SPACEFISH_USER_COLOR yellow
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
19	32	----> __sf_util_set_default SPACEFISH_USER_COLOR_ROOT red
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
13	13	----> [ $SPACEFISH_USER_SHOW = false ]
8	65	----> if test "$SPACEFISH_USER_SHOW" = "always" \
	-o "$LOGNAME" != "$USER" \
	-o "$UID" = "0" \
	-o \( "$SPACEFISH_USER_SHOW" = "true" -a -n "$SSH_CONNECTION" \)

		set -l user_color
		if test "$USER" = "root"
			set user_color $SPACEFISH_USER_COLOR_ROOT
		else
			set user_color $SPACEFISH_USER_COLOR
		end

		__sf_lib_section \
			$user_color \
			$SPACEFISH_USER_PREFIX \
			$USER \
			$SPACEFISH_USER_SUFFIX
	...
57	57	-----> test "$SPACEFISH_USER_SHOW" = "always" \
	-o "$LOGNAME" != "$USER" \
	-o "$UID" = "0" \
	-o \( "$SPACEFISH_USER_SHOW" = "true" -a -n "$SSH_CONNECTION" \)
35	7409	--> eval __sf_section_$i
117	7374	---> __sf_section_dir
33	49	----> __sf_util_set_default SPACEFISH_DIR_SHOW true
4	16	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
22	36	----> __sf_util_set_default SPACEFISH_DIR_PREFIX "in "
3	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
23	36	----> __sf_util_set_default SPACEFISH_DIR_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
4	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
19	30	----> __sf_util_set_default SPACEFISH_DIR_TRUNC 3
2	11	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
19	35	----> __sf_util_set_default SPACEFISH_DIR_TRUNC_REPO true
3	16	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
19	32	----> __sf_util_set_default SPACEFISH_DIR_COLOR cyan
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
18	30	----> __sf_util_set_default SPACEFISH_DIR_LOCK_SHOW true
3	12	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
22	35	----> __sf_util_set_default SPACEFISH_DIR_LOCK_SYMBOL ""
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
20	32	----> __sf_util_set_default SPACEFISH_DIR_LOCK_COLOR red
2	12	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
20	20	----> [ $SPACEFISH_DIR_SHOW = false ]
17	17	----> set -l dir
10	10	----> set -l tmp
193	2390	----> set -l git_root (command git rev-parse --show-toplevel 2>/dev/null)
2197	2197	-----> command git rev-parse --show-toplevel 2>/dev/null
42	644	----> if test "$SPACEFISH_DIR_TRUNC_REPO" = "true" -a -n "$git_root"
		# Resolve to physical PWD instead of logical
		set -l resolvedPWD (pwd -P 2>/dev/null; or pwd)
		# Treat repo root as top level directory
		set tmp (string replace $git_root (basename $git_root) $resolvedPWD)
	else
		set -l realhome ~
		set tmp (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD)
	...
44	44	-----> test "$SPACEFISH_DIR_TRUNC_REPO" = "true" -a -n "$git_root"
29	29	-----> set -l realhome ~
354	529	-----> set tmp (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD)
175	175	------> string replace -r '^'"$realhome"'($|/)' '~$1' $PWD
365	3109	----> set dir (__sf_util_truncate_dir $tmp $SPACEFISH_DIR_TRUNC)
195	2744	-----> __sf_util_truncate_dir $tmp $SPACEFISH_DIR_TRUNC
55	2549	------> if test "$truncate_to" -eq 0
		echo $path
	else
		set -l folders (string split / $path)

		if test (count $folders) -le "$truncate_to"
			echo $path
		else
			echo (string join / $folders[(math 0 - $truncate_to)..-1])
		end
	...
72	72	-------> test "$truncate_to" -eq 0
516	699	-------> set -l folders (string split / $path)
183	183	--------> string split / $path
48	1723	-------> if test (count $folders) -le "$truncate_to"
			echo $path
		else
			echo (string join / $folders[(math 0 - $truncate_to)..-1])
		...
532	735	--------> test (count $folders) -le "$truncate_to"
203	203	---------> count $folders
448	940	--------> echo (string join / $folders[(math 0 - $truncate_to)..-1])
417	492	---------> string join / $folders[(math 0 - $truncate_to)..-1]
75	75	----------> math 0 - $truncate_to
6	65	----> if [ $SPACEFISH_DIR_LOCK_SHOW = true -a ! -w . ]
		set DIR_LOCK_SYMBOL (set_color $SPACEFISH_DIR_LOCK_COLOR)" $SPACEFISH_DIR_LOCK_SYMBOL"(set_color --bold)
	...
59	59	-----> [ $SPACEFISH_DIR_LOCK_SHOW = true -a ! -w . ]
117	687	----> __sf_lib_section \
		$SPACEFISH_DIR_COLOR \
		$SPACEFISH_DIR_PREFIX \
		$dir \
		"$DIR_LOCK_SYMBOL""$SPACEFISH_DIR_SUFFIX"
7	188	-----> if test (count $argv) -eq 2
		set content $argv[2]
		set prefix
	...
144	181	------> test (count $argv) -eq 2
37	37	-------> count $argv
5	42	-----> if test "$sf_prompt_opened" = "true" -a "$SPACEFISH_PROMPT_PREFIXES_SHOW" = "true"
		# Echo prefixes in bold white
		set_color --bold
		echo -e -n -s $prefix
		set_color normal
	...
37	37	------> test "$sf_prompt_opened" = "true" -a "$SPACEFISH_PROMPT_PREFIXES_SHOW" = "true"
24	24	-----> set -g sf_prompt_opened true
143	143	-----> set_color --bold $color
60	60	-----> echo -e -n $content
22	22	-----> set_color normal
15	91	-----> if test "$SPACEFISH_PROMPT_SUFFIXES_SHOW" = "true"
		# Echo suffixes in bold white
		set_color --bold
		echo -e -n -s $suffix
		set_color normal
	...
16	16	------> test "$SPACEFISH_PROMPT_SUFFIXES_SHOW" = "true"
22	22	------> set_color --bold
23	23	------> echo -e -n -s $suffix
15	15	------> set_color normal
38	448	--> eval __sf_section_$i
55	410	---> __sf_section_host
39	62	----> __sf_util_set_default SPACEFISH_HOST_SHOW true
5	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
37	55	----> __sf_util_set_default SPACEFISH_HOST_PREFIX "at "
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
55	84	----> __sf_util_set_default SPACEFISH_HOST_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
6	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
23	23	------> not set -q $var
27	50	----> __sf_util_set_default SPACEFISH_HOST_COLOR blue
6	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
33	49	----> __sf_util_set_default SPACEFISH_HOST_COLOR_SSH green
4	16	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
25	25	----> [ "$SPACEFISH_HOST_SHOW" = false ]
5	30	----> if test "$SPACEFISH_HOST_SHOW" = "always"; or set -q SSH_CONNECTION;

		# Determination of what color should be used
		set -l host_color
		if set -q SSH_CONNECTION;
			set host_color $SPACEFISH_HOST_COLOR_SSH
		else
			set host_color $SPACEFISH_HOST_COLOR
		end

		__sf_lib_section \
			$host_color \
			$SPACEFISH_HOST_PREFIX \
			(hostname) \
			$SPACEFISH_HOST_SUFFIX
		...
16	16	-----> test "$SPACEFISH_HOST_SHOW" = "always"
9	9	-----> set -q SSH_CONNECTION
42	12036	--> eval __sf_section_$i
56	11994	---> __sf_section_git
35	59	----> __sf_util_set_default SPACEFISH_GIT_SHOW true
5	24	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
29	52	----> __sf_util_set_default SPACEFISH_GIT_PREFIX "on "
5	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
30	42	----> __sf_util_set_default SPACEFISH_GIT_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
2	12	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
38	53	----> __sf_util_set_default SPACEFISH_GIT_SYMBOL " "
3	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
14	14	----> [ $SPACEFISH_GIT_SHOW = false ]
133	2302	----> set -l git_branch (__sf_section_git_branch)
61	2169	-----> __sf_section_git_branch
34	58	------> __sf_util_set_default SPACEFISH_GIT_BRANCH_SHOW true
6	24	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	--------> not set -q $var
36	54	------> __sf_util_set_default SPACEFISH_GIT_BRANCH_PREFIX $SPACEFISH_GIT_SYMBOL
4	18	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	--------> not set -q $var
40	57	------> __sf_util_set_default SPACEFISH_GIT_BRANCH_SUFFIX ""
4	17	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	--------> not set -q $var
24	47	------> __sf_util_set_default SPACEFISH_GIT_BRANCH_COLOR magenta
5	23	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	--------> not set -q $var
15	15	------> [ $SPACEFISH_GIT_BRANCH_SHOW = false ]
147	1853	------> set -l git_branch (__sf_util_git_branch)
42	1706	-------> __sf_util_git_branch
167	1664	--------> echo (command git rev-parse --abbrev-ref HEAD 2>/dev/null)
1497	1497	---------> command git rev-parse --abbrev-ref HEAD 2>/dev/null
18	18	------> [ -z $git_branch ]
6	6	------> return
111	9389	----> set -l git_status (__sf_section_git_status)
140	9278	-----> __sf_section_git_status
41	70	------> __sf_util_set_default SPACEFISH_GIT_STATUS_SHOW true
7	29	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
22	22	--------> not set -q $var
37	55	------> __sf_util_set_default SPACEFISH_GIT_STATUS_PREFIX " ["
4	18	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	--------> not set -q $var
36	55	------> __sf_util_set_default SPACEFISH_GIT_STATUS_SUFFIX ]
3	19	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	--------> not set -q $var
27	51	------> __sf_util_set_default SPACEFISH_GIT_STATUS_COLOR red
4	24	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	--------> not set -q $var
29	47	------> __sf_util_set_default SPACEFISH_GIT_STATUS_UNTRACKED \?
4	18	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	--------> not set -q $var
30	46	------> __sf_util_set_default SPACEFISH_GIT_STATUS_ADDED +
4	16	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	--------> not set -q $var
33	57	------> __sf_util_set_default SPACEFISH_GIT_STATUS_MODIFIED !
4	24	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	--------> not set -q $var
23	38	------> __sf_util_set_default SPACEFISH_GIT_STATUS_RENAMED »
4	15	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	--------> not set -q $var
30	45	------> __sf_util_set_default SPACEFISH_GIT_STATUS_DELETED ✘
4	15	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	--------> not set -q $var
25	52	------> __sf_util_set_default SPACEFISH_GIT_STATUS_STASHED \$
9	27	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	--------> not set -q $var
23	37	------> __sf_util_set_default SPACEFISH_GIT_STATUS_UNMERGED =
3	14	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	--------> not set -q $var
33	51	------> __sf_util_set_default SPACEFISH_GIT_STATUS_AHEAD ⇡
3	18	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	--------> not set -q $var
27	49	------> __sf_util_set_default SPACEFISH_GIT_STATUS_BEHIND ⇣
5	22	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	--------> not set -q $var
23	39	------> __sf_util_set_default SPACEFISH_GIT_STATUS_DIVERGED ⇕
4	16	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	--------> not set -q $var
50	67	------> __sf_util_set_default SPACEFISH_GIT_PROMPT_ORDER untracked added modified renamed deleted stashed unmerged diverged ahead behind
4	17	-------> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	--------> not set -q $var
20	20	------> [ $SPACEFISH_GIT_STATUS_SHOW = false ]
18	18	------> set -l git_status
11	11	------> set -l is_ahead
8	8	------> set -l is_behind
124	1376	------> set -l index (command git status --porcelain 2>/dev/null -b)
1252	1252	-------> command git status --porcelain 2>/dev/null -b
118	198	------> set -l trimmed_index (string split \n $index | string sub --start 1 --length 2)
80	80	-------> string split \n $index | string sub --start 1 --length 2
16	16	------> for i in $trimmed_index
		if test (string match '\?\?' $i)
			set git_status untracked $git_status
		end
		if test (string match '*A*' $i)
			set git_status added $git_status
		end
		if test (string match '*M*' $i)
			set git_status modified $git_status
		end
		if test (string match '*R*' $i)
			set git_status renamed $git_status
		end
		if test (string match '*D*' $i)
			set git_status deleted $git_status
		end
		if test (string match '*U*' $i)
			set git_status unmerged $git_status
		end
	...
9	1774	------> if test -n (echo (command git rev-parse --verify refs/stash 2>/dev/null))
		set git_status stashed $git_status
	...
420	1765	-------> test -n (echo (command git rev-parse --verify refs/stash 2>/dev/null))
126	1345	--------> echo (command git rev-parse --verify refs/stash 2>/dev/null)
1219	1219	---------> command git rev-parse --verify refs/stash 2>/dev/null
10	214	------> if test (string match '*ahead*' $index)
		set is_ahead true
	...
156	204	-------> test (string match '*ahead*' $index)
48	48	--------> string match '*ahead*' $index
8	182	------> if test (string match '*behind*' $index)
		set is_behind true
	...
125	174	-------> test (string match '*behind*' $index)
49	49	--------> string match '*behind*' $index
15	93	------> if test "$is_ahead" = "true" -a "$is_behind" = "true"
		set git_status diverged $git_status
	else if test "$is_ahead" = "true"
		set git_status ahead $git_status
	else if test "$is_behind" = "true"
		set git_status behind $git_status
	...
41	41	-------> test "$is_ahead" = "true" -a "$is_behind" = "true"
21	21	-------> test "$is_ahead" = "true"
16	16	-------> test "$is_behind" = "true"
24	24	------> set -l full_git_status
264	4424	------> for i in $SPACEFISH_GIT_PROMPT_ORDER
		set i (string upper $i)
		set git_status (string upper $git_status)
		if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		end
	...
140	173	-------> set i (string upper $i)
33	33	--------> string upper $i
243	288	-------> set git_status (string upper $git_status)
45	45	--------> string upper $git_status
8	47	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
39	39	--------> contains $i in $git_status
141	193	-------> set i (string upper $i)
52	52	--------> string upper $i
98	128	-------> set git_status (string upper $git_status)
30	30	--------> string upper $git_status
5	30	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
25	25	--------> contains $i in $git_status
92	129	-------> set i (string upper $i)
37	37	--------> string upper $i
94	128	-------> set git_status (string upper $git_status)
34	34	--------> string upper $git_status
4	26	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
22	22	--------> contains $i in $git_status
130	158	-------> set i (string upper $i)
28	28	--------> string upper $i
157	182	-------> set git_status (string upper $git_status)
25	25	--------> string upper $git_status
16	90	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
74	74	--------> contains $i in $git_status
344	451	-------> set i (string upper $i)
107	107	--------> string upper $i
396	505	-------> set git_status (string upper $git_status)
109	109	--------> string upper $git_status
14	99	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
85	85	--------> contains $i in $git_status
215	242	-------> set i (string upper $i)
27	27	--------> string upper $i
86	116	-------> set git_status (string upper $git_status)
30	30	--------> string upper $git_status
4	34	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
30	30	--------> contains $i in $git_status
95	147	-------> set i (string upper $i)
52	52	--------> string upper $i
101	125	-------> set git_status (string upper $git_status)
24	24	--------> string upper $git_status
3	24	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
21	21	--------> contains $i in $git_status
97	124	-------> set i (string upper $i)
27	27	--------> string upper $i
101	124	-------> set git_status (string upper $git_status)
23	23	--------> string upper $git_status
4	27	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
23	23	--------> contains $i in $git_status
99	125	-------> set i (string upper $i)
26	26	--------> string upper $i
92	119	-------> set git_status (string upper $git_status)
27	27	--------> string upper $git_status
3	32	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
29	29	--------> contains $i in $git_status
95	123	-------> set i (string upper $i)
28	28	--------> string upper $i
120	145	-------> set git_status (string upper $git_status)
25	25	--------> string upper $git_status
3	26	-------> if contains $i in $git_status
			set -l status_symbol SPACEFISH_GIT_STATUS_$i
			set full_git_status "$$status_symbol$full_git_status"
		...
23	23	--------> contains $i in $git_status
4	21	------> if test -n "$full_git_status"
		__sf_lib_section \
			$SPACEFISH_GIT_STATUS_COLOR \
			"$SPACEFISH_GIT_STATUS_PREFIX$full_git_status$SPACEFISH_GIT_STATUS_SUFFIX"
	...
17	17	-------> test -n "$full_git_status"
17	17	----> [ -z $git_branch ]
10	10	----> return
36	433	--> eval __sf_section_$i
52	397	---> __sf_section_package
33	64	----> __sf_util_set_default SPACEFISH_PACKAGE_SHOW true
6	31	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
25	25	------> not set -q $var
33	53	----> __sf_util_set_default SPACEFISH_PACKAGE_PREFIX "is "
4	20	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
41	59	----> __sf_util_set_default SPACEFISH_PACKAGE_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
37	55	----> __sf_util_set_default SPACEFISH_PACKAGE_SYMBOL "📦 "
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
25	48	----> __sf_util_set_default SPACEFISH_PACKAGE_COLOR red
6	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
17	17	----> [ $SPACEFISH_PACKAGE_SHOW = false ]
13	49	----> if not test -e ./package.json; and not test -e ./Cargo.toml
		return
	...
13	13	-----> not test -e ./package.json
12	12	-----> not test -e ./Cargo.toml
11	11	-----> return
32	708	--> eval __sf_section_$i
47	676	---> __sf_section_node
30	48	----> __sf_util_set_default SPACEFISH_NODE_SHOW true
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
78	102	----> __sf_util_set_default SPACEFISH_NODE_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
5	24	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
39	55	----> __sf_util_set_default SPACEFISH_NODE_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
4	16	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
45	68	----> __sf_util_set_default SPACEFISH_NODE_SYMBOL "⬢ "
4	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
30	53	----> __sf_util_set_default SPACEFISH_NODE_DEFAULT_VERSION ""
5	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
25	42	----> __sf_util_set_default SPACEFISH_NODE_COLOR green
3	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
23	23	----> [ $SPACEFISH_NODE_SHOW = false ]
13	238	----> if not test -f ./package.json \
		-o -d ./node_modules \
		-o (count *.js) -gt 0
		return
	...
127	214	-----> not test -f ./package.json \
		-o -d ./node_modules \
		-o (count *.js) -gt 0
87	87	------> count *.js
11	11	-----> return
36	601	--> eval __sf_section_$i
55	565	---> __sf_section_ruby
30	55	----> __sf_util_set_default SPACEFISH_RUBY_SHOW true
5	25	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
34	49	----> __sf_util_set_default SPACEFISH_RUBY_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
3	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
40	59	----> __sf_util_set_default SPACEFISH_RUBY_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	19	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
33	57	----> __sf_util_set_default SPACEFISH_RUBY_SYMBOL "💎 "
4	24	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
24	41	----> __sf_util_set_default SPACEFISH_RUBY_COLOR red
5	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
22	22	----> [ $SPACEFISH_RUBY_SHOW = false ]
14	227	----> if not test -f Gemfile \
		-o -f Rakefile \
		-o (count *.rb) -gt 0
		return
	...
121	190	-----> not test -f Gemfile \
		-o -f Rakefile \
		-o (count *.rb) -gt 0
69	69	------> count *.rb
23	23	-----> return
38	2007	--> eval __sf_section_$i
57	1969	---> __sf_section_golang
30	52	----> __sf_util_set_default SPACEFISH_GOLANG_SHOW true
5	22	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
35	55	----> __sf_util_set_default SPACEFISH_GOLANG_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
4	20	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
40	59	----> __sf_util_set_default SPACEFISH_GOLANG_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
4	19	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
30	57	----> __sf_util_set_default SPACEFISH_GOLANG_SYMBOL "🐹 "
8	27	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
25	45	----> __sf_util_set_default SPACEFISH_GOLANG_COLOR cyan
5	20	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
21	21	----> [ $SPACEFISH_GOLANG_SHOW = false ]
93	1161	----> type -q go
13	13	-----> set -q argv[1]
40	40	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
66	66	-----> argparse -n type -x t,p,P $options -- $argv
3	15	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
12	12	------> set -q _flag_help
11	11	-----> set -l res 1
18	18	-----> set -l mode normal
14	14	-----> set -l multi no
10	10	-----> set -l selection all
8	8	-----> set -l short no
10	32	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	------> set -q _flag_quiet
16	16	------> set mode quiet
11	11	-----> set -q _flag_all
7	7	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
39	799	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
17	17	------> set -l found 0
24	395	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
17	17	-------> test $selection != files
4	30	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
26	26	--------> functions -q -- $i
6	324	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
264	318	--------> contains -- $i (builtin -n)
54	54	---------> builtin -n
22	22	------> set -l paths
12	204	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
15	15	-------> test $multi != yes
131	177	-------> set paths (command -s -- $i)
46	46	--------> command -s -- $i
33	109	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
16	16	-------> set res 0
9	9	-------> set found 1
16	16	-------> switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            ...
8	35	-------> if test $multi != yes
                continue
            ...
20	20	--------> test $multi != yes
7	7	--------> continue
2	13	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
11	11	-------> test $found = 0
18	18	-----> return $res
14	462	----> if not test -f go.mod \
		-o -d Godeps \
		-o -f glide.yaml \
		-o (count *.go) -gt 0 \
		-o -f Gopkg.yml \
		-o -f Gopkg.lock \
		-o ([ (count $GOPATH) -gt 0 ]; and string match $GOPATH $PWD)
		return
	...
223	437	-----> not test -f go.mod \
		-o -d Godeps \
		-o -f glide.yaml \
		-o (count *.go) -gt 0 \
		-o -f Gopkg.yml \
		-o -f Gopkg.lock \
		-o ([ (count $GOPATH) -gt 0 ]; and string match $GOPATH $PWD)
87	87	------> count *.go
99	127	------> [ (count $GOPATH) -gt 0 ]
28	28	-------> count $GOPATH
11	11	-----> return
45	1439	--> eval __sf_section_$i
41	1394	---> __sf_section_php
31	60	----> __sf_util_set_default SPACEFISH_PHP_SHOW true
5	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
24	24	------> not set -q $var
35	61	----> __sf_util_set_default SPACEFISH_PHP_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
6	26	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
34	52	----> __sf_util_set_default SPACEFISH_PHP_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
35	51	----> __sf_util_set_default SPACEFISH_PHP_SYMBOL "🐘 "
3	16	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
28	51	----> __sf_util_set_default SPACEFISH_PHP_COLOR blue
4	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
15	15	----> [ $SPACEFISH_PHP_SHOW = false ]
91	1056	----> type -q php
16	16	-----> set -q argv[1]
28	28	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
57	57	-----> argparse -n type -x t,p,P $options -- $argv
3	14	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
11	11	------> set -q _flag_help
10	10	-----> set -l res 1
17	17	-----> set -l mode normal
15	15	-----> set -l multi no
10	10	-----> set -l selection all
8	8	-----> set -l short no
9	30	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	------> set -q _flag_quiet
14	14	------> set mode quiet
13	13	-----> set -q _flag_all
7	7	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
34	716	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
17	17	------> set -l found 0
19	366	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
18	18	-------> test $selection != files
3	28	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
25	25	--------> functions -q -- $i
7	301	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
241	294	--------> contains -- $i (builtin -n)
53	53	---------> builtin -n
17	17	------> set -l paths
10	223	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
16	16	-------> test $multi != yes
101	197	-------> set paths (command -s -- $i)
96	96	--------> command -s -- $i
17	17	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
8	42	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
21	21	-------> test $found = 0
13	13	-------> test $mode != quiet
18	18	-----> return $res
7	7	----> return
38	1484	--> eval __sf_section_$i
59	1446	---> __sf_section_rust
30	59	----> __sf_util_set_default SPACEFISH_RUST_SHOW true
7	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
22	22	------> not set -q $var
35	55	----> __sf_util_set_default SPACEFISH_RUST_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
5	20	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
36	51	----> __sf_util_set_default SPACEFISH_RUST_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
4	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
37	54	----> __sf_util_set_default SPACEFISH_RUST_SYMBOL "𝗥 "
4	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
24	45	----> __sf_util_set_default SPACEFISH_RUST_COLOR red
5	21	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
26	41	----> __sf_util_set_default SPACEFISH_RUST_VERBOSE_VERSION false
3	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
19	19	----> [ $SPACEFISH_RUST_SHOW = false ]
90	1053	----> type -q rustc
10	10	-----> set -q argv[1]
30	30	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
72	72	-----> argparse -n type -x t,p,P $options -- $argv
2	14	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
12	12	------> set -q _flag_help
14	14	-----> set -l res 1
18	18	-----> set -l mode normal
10	10	-----> set -l multi no
9	9	-----> set -l selection all
8	8	-----> set -l short no
16	41	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	------> set -q _flag_quiet
18	18	------> set mode quiet
8	8	-----> set -q _flag_all
7	7	-----> set -q _flag_short
7	7	-----> set -q _flag_no_functions
41	701	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
19	19	------> set -l found 0
24	345	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
15	15	-------> test $selection != files
5	28	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
23	23	--------> functions -q -- $i
6	278	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
221	272	--------> contains -- $i (builtin -n)
51	51	---------> builtin -n
20	20	------> set -l paths
10	219	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	-------> test $multi != yes
102	195	-------> set paths (command -s -- $i)
93	93	--------> command -s -- $i
14	14	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
10	43	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
14	14	-------> test $found = 0
19	19	-------> test $mode != quiet
14	14	-----> return $res
10	10	----> return
56	1484	--> eval __sf_section_$i
48	1428	---> __sf_section_haskell
38	62	----> __sf_util_set_default SPACEFISH_HASKELL_SHOW true
4	24	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
34	61	----> __sf_util_set_default SPACEFISH_HASKELL_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
5	27	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
22	22	------> not set -q $var
33	55	----> __sf_util_set_default SPACEFISH_HASKELL_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	22	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
28	42	----> __sf_util_set_default SPACEFISH_HASKELL_SYMBOL "λ "
3	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
33	52	----> __sf_util_set_default SPACEFISH_HASKELL_COLOR red
4	19	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
14	14	----> [ $SPACEFISH_HASKELL_SHOW = false ]
89	1084	----> type -q stack
16	16	-----> set -q argv[1]
26	26	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
57	57	-----> argparse -n type -x t,p,P $options -- $argv
3	14	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
11	11	------> set -q _flag_help
12	12	-----> set -l res 1
19	19	-----> set -l mode normal
11	11	-----> set -l multi no
10	10	-----> set -l selection all
8	8	-----> set -l short no
11	35	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	------> set -q _flag_quiet
17	17	------> set mode quiet
9	9	-----> set -q _flag_all
7	7	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
36	752	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
18	18	------> set -l found 0
28	393	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
17	17	-------> test $selection != files
2	24	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
22	22	--------> functions -q -- $i
7	324	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
264	317	--------> contains -- $i (builtin -n)
53	53	---------> builtin -n
22	22	------> set -l paths
8	224	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
15	15	-------> test $multi != yes
109	201	-------> set paths (command -s -- $i)
92	92	--------> command -s -- $i
13	13	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
8	46	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
17	17	-------> test $found = 0
21	21	-------> test $mode != quiet
13	13	-----> return $res
10	10	----> return
36	1429	--> eval __sf_section_$i
54	1393	---> __sf_section_julia
33	53	----> __sf_util_set_default SPACEFISH_JULIA_SHOW true
4	20	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
36	57	----> __sf_util_set_default SPACEFISH_JULIA_PREFIX "is "
4	21	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
37	57	----> __sf_util_set_default SPACEFISH_JULIA_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	20	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
25	47	----> __sf_util_set_default SPACEFISH_JULIA_SYMBOL "ஃ "
6	22	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
26	41	----> __sf_util_set_default SPACEFISH_JULIA_COLOR green
3	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
20	20	----> [ $SPACEFISH_JULIA_SHOW = false ]
93	1055	----> type -q julia
11	11	-----> set -q argv[1]
31	31	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
54	54	-----> argparse -n type -x t,p,P $options -- $argv
5	20	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
15	15	------> set -q _flag_help
26	26	-----> set -l res 1
22	22	-----> set -l mode normal
11	11	-----> set -l multi no
9	9	-----> set -l selection all
9	9	-----> set -l short no
12	36	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	------> set -q _flag_quiet
17	17	------> set mode quiet
9	9	-----> set -q _flag_all
6	6	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
39	697	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
18	18	------> set -l found 0
21	349	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
17	17	-------> test $selection != files
3	25	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
22	22	--------> functions -q -- $i
6	286	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
228	280	--------> contains -- $i (builtin -n)
52	52	---------> builtin -n
22	22	------> set -l paths
9	217	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	-------> test $multi != yes
102	194	-------> set paths (command -s -- $i)
92	92	--------> command -s -- $i
13	13	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
7	39	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
15	15	-------> test $found = 0
17	17	-------> test $mode != quiet
15	15	-----> return $res
9	9	----> return
37	805	--> eval __sf_section_$i
42	768	---> __sf_section_elixir
54	80	----> __sf_util_set_default SPACEFISH_ELIXIR_SHOW true
5	26	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	------> not set -q $var
36	62	----> __sf_util_set_default SPACEFISH_ELIXIR_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
5	26	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	------> not set -q $var
31	52	----> __sf_util_set_default SPACEFISH_ELIXIR_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	21	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
28	42	----> __sf_util_set_default SPACEFISH_ELIXIR_SYMBOL "💧 "
3	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
38	57	----> __sf_util_set_default SPACEFISH_ELIXIR_DEFAULT_VERSION $SPACEFISH_ELIXIR_DEFAULT_VERSION
5	19	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
25	47	----> __sf_util_set_default SPACEFISH_ELIXIR_COLOR magenta
5	22	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
17	17	----> [ $SPACEFISH_ELIXIR_SHOW = false ]
13	369	----> if not test -f mix.exs \
		-o (count *.ex) -gt 0 \
		-o (count *.exs) -gt 0
		return
	...
205	345	-----> not test -f mix.exs \
		-o (count *.ex) -gt 0 \
		-o (count *.exs) -gt 0
74	74	------> count *.ex
66	66	------> count *.exs
11	11	-----> return
37	1518	--> eval __sf_section_$i
51	1481	---> __sf_section_docker
30	52	----> __sf_util_set_default SPACEFISH_DOCKER_SHOW true
5	22	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
34	50	----> __sf_util_set_default SPACEFISH_DOCKER_PREFIX "is "
4	16	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
40	58	----> __sf_util_set_default SPACEFISH_DOCKER_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
36	59	----> __sf_util_set_default SPACEFISH_DOCKER_SYMBOL "🐳 "
6	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
41	60	----> __sf_util_set_default SPACEFISH_DOCKER_COLOR cyan
4	19	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
34	51	----> __sf_util_set_default SPACEFISH_DOCKER_VERBOSE_VERSION false
3	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
14	14	----> [ $SPACEFISH_DOCKER_SHOW = false ]
95	1080	----> type -q docker
12	12	-----> set -q argv[1]
31	31	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
57	57	-----> argparse -n type -x t,p,P $options -- $argv
5	17	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
12	12	------> set -q _flag_help
18	18	-----> set -l res 1
12	12	-----> set -l mode normal
9	9	-----> set -l multi no
10	10	-----> set -l selection all
11	11	-----> set -l short no
10	35	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
13	13	------> set -q _flag_quiet
12	12	------> set mode quiet
8	8	-----> set -q _flag_all
6	6	-----> set -q _flag_short
8	8	-----> set -q _flag_no_functions
43	739	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
14	14	------> set -l found 0
25	371	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
22	22	-------> test $selection != files
4	40	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
36	36	--------> functions -q -- $i
6	284	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
230	278	--------> contains -- $i (builtin -n)
48	48	---------> builtin -n
16	16	------> set -l paths
11	239	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	-------> test $multi != yes
124	214	-------> set paths (command -s -- $i)
90	90	--------> command -s -- $i
17	17	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
7	39	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
18	18	-------> test $found = 0
14	14	-------> test $mode != quiet
12	12	-----> return $res
6	6	----> return
36	1257	--> eval __sf_section_$i
40	1221	---> __sf_section_aws
30	50	----> __sf_util_set_default SPACEFISH_AWS_SHOW true
4	20	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
26	41	----> __sf_util_set_default SPACEFISH_AWS_PREFIX "using "
4	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
26	39	----> __sf_util_set_default SPACEFISH_AWS_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
22	36	----> __sf_util_set_default SPACEFISH_AWS_SYMBOL "☁️ "
3	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
20	33	----> __sf_util_set_default SPACEFISH_AWS_COLOR ff8700
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
14	14	----> [ $SPACEFISH_AWS_SHOW = false ]
85	963	----> type -q aws
10	10	-----> set -q argv[1]
24	24	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
45	45	-----> argparse -n type -x t,p,P $options -- $argv
3	12	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
9	9	------> set -q _flag_help
11	11	-----> set -l res 1
10	10	-----> set -l mode normal
9	9	-----> set -l multi no
9	9	-----> set -l selection all
8	8	-----> set -l short no
7	22	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	------> set -q _flag_quiet
9	9	------> set mode quiet
7	7	-----> set -q _flag_all
6	6	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
29	683	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
11	11	------> set -l found 0
15	342	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
13	13	-------> test $selection != files
3	27	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
24	24	--------> functions -q -- $i
5	287	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
237	282	--------> contains -- $i (builtin -n)
45	45	---------> builtin -n
17	17	------> set -l paths
14	228	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
22	22	-------> test $multi != yes
94	192	-------> set paths (command -s -- $i)
98	98	--------> command -s -- $i
19	19	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
9	37	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
16	16	-------> test $found = 0
12	12	-------> test $mode != quiet
16	16	-----> return $res
5	5	----> return
45	452	--> eval __sf_section_$i
46	407	---> __sf_section_venv
38	61	----> __sf_util_set_default SPACEFISH_VENV_SHOW true
6	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
36	62	----> __sf_util_set_default SPACEFISH_VENV_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
4	26	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
22	22	------> not set -q $var
32	58	----> __sf_util_set_default SPACEFISH_VENV_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	26	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	------> not set -q $var
29	43	----> __sf_util_set_default SPACEFISH_VENV_SYMBOL "·"
3	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
38	56	----> __sf_util_set_default SPACEFISH_VENV_GENERIC_NAMES virtualenv venv .venv
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
25	49	----> __sf_util_set_default SPACEFISH_VENV_COLOR blue
6	24	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
15	15	----> test $SPACEFISH_VENV_SHOW = false
12	12	----> test -n "$VIRTUAL_ENV"
5	5	----> return
60	1492	--> eval __sf_section_$i
51	1432	---> __sf_section_conda
38	70	----> __sf_util_set_default SPACEFISH_CONDA_SHOW true
6	32	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
26	26	------> not set -q $var
33	56	----> __sf_util_set_default SPACEFISH_CONDA_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
5	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
30	44	----> __sf_util_set_default SPACEFISH_CONDA_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
2	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
35	52	----> __sf_util_set_default SPACEFISH_CONDA_SYMBOL "🅒 "
4	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
24	49	----> __sf_util_set_default SPACEFISH_CONDA_COLOR blue
5	25	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
17	17	----> [ $SPACEFISH_CONDA_SHOW = false ]
10	1093	----> if not type -q conda; \
		or test -z "$CONDA_DEFAULT_ENV";
		return
	...
83	1074	-----> not type -q conda
15	15	------> set -q argv[1]
31	31	------> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
57	57	------> argparse -n type -x t,p,P $options -- $argv
4	15	------> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
11	11	-------> set -q _flag_help
12	12	------> set -l res 1
18	18	------> set -l mode normal
14	14	------> set -l multi no
10	10	------> set -l selection all
8	8	------> set -l short no
10	32	------> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	-------> set -q _flag_quiet
16	16	-------> set mode quiet
11	11	------> set -q _flag_all
7	7	------> set -q _flag_short
6	6	------> set -q _flag_no_functions
33	741	------> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
16	16	-------> set -l found 0
22	385	-------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
20	20	--------> test $selection != files
3	27	--------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
24	24	---------> functions -q -- $i
8	316	--------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
253	308	---------> contains -- $i (builtin -n)
55	55	----------> builtin -n
23	23	-------> set -l paths
10	226	-------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
15	15	--------> test $multi != yes
109	201	--------> set paths (command -s -- $i)
92	92	---------> command -s -- $i
14	14	-------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
10	44	-------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
15	15	--------> test $found = 0
19	19	--------> test $mode != quiet
14	14	------> return $res
9	9	-----> return
36	1522	--> eval __sf_section_$i
53	1486	---> __sf_section_pyenv
36	57	----> __sf_util_set_default SPACEFISH_PYENV_SHOW true
5	21	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
42	61	----> __sf_util_set_default SPACEFISH_PYENV_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
4	19	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
37	58	----> __sf_util_set_default SPACEFISH_PYENV_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	21	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
27	52	----> __sf_util_set_default SPACEFISH_PYENV_SYMBOL "🐍 "
5	25	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
26	42	----> __sf_util_set_default SPACEFISH_PYENV_COLOR yellow
2	16	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
22	22	----> [ $SPACEFISH_PYENV_SHOW = false ]
88	1131	----> type -q pyenv
10	10	-----> set -q argv[1]
35	35	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
50	50	-----> argparse -n type -x t,p,P $options -- $argv
5	20	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
15	15	------> set -q _flag_help
30	30	-----> set -l res 1
23	23	-----> set -l mode normal
13	13	-----> set -l multi no
10	10	-----> set -l selection all
10	10	-----> set -l short no
11	37	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
12	12	------> set -q _flag_quiet
14	14	------> set mode quiet
7	7	-----> set -q _flag_all
7	7	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
38	771	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
18	18	------> set -l found 0
20	395	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
15	15	-------> test $selection != files
5	35	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
30	30	--------> functions -q -- $i
5	325	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
271	320	--------> contains -- $i (builtin -n)
49	49	---------> builtin -n
22	22	------> set -l paths
10	239	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
18	18	-------> test $multi != yes
118	211	-------> set paths (command -s -- $i)
93	93	--------> command -s -- $i
14	14	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
9	45	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
15	15	-------> test $found = 0
21	21	-------> test $mode != quiet
14	14	-----> return $res
10	10	----> return
44	2354	--> eval __sf_section_$i
49	2310	---> __sf_section_dotnet
39	60	----> __sf_util_set_default SPACEFISH_DOTNET_SHOW true
4	21	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
42	61	----> __sf_util_set_default SPACEFISH_DOTNET_PREFIX $SPACEFISH_PROMPT_DEFAULT_PREFIX
3	19	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
16	16	------> not set -q $var
33	56	----> __sf_util_set_default SPACEFISH_DOTNET_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
4	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
27	44	----> __sf_util_set_default SPACEFISH_DOTNET_SYMBOL ".NET "
5	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
32	50	----> __sf_util_set_default SPACEFISH_DOTNET_COLOR "af00d7"
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
19	19	----> [ $SPACEFISH_DOTNET_SHOW = false ]
87	1169	----> type -q dotnet
11	11	-----> set -q argv[1]
27	27	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
55	55	-----> argparse -n type -x t,p,P $options -- $argv
5	18	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
13	13	------> set -q _flag_help
17	17	-----> set -l res 1
12	12	-----> set -l mode normal
9	9	-----> set -l multi no
10	10	-----> set -l selection all
13	13	-----> set -l short no
9	32	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
12	12	------> set -q _flag_quiet
11	11	------> set mode quiet
7	7	-----> set -q _flag_all
6	6	-----> set -q _flag_short
7	7	-----> set -q _flag_no_functions
42	840	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
15	15	------> set -l found 0
22	428	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
14	14	-------> test $selection != files
5	35	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
30	30	--------> functions -q -- $i
5	357	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
300	352	--------> contains -- $i (builtin -n)
52	52	---------> builtin -n
22	22	------> set -l paths
11	214	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
19	19	-------> test $multi != yes
146	184	-------> set paths (command -s -- $i)
38	38	--------> command -s -- $i
31	104	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
16	16	-------> set res 0
10	10	-------> set found 1
12	12	-------> switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            ...
8	35	-------> if test $multi != yes
                continue
            ...
20	20	--------> test $multi != yes
7	7	--------> continue
4	15	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
11	11	-------> test $found = 0
18	18	-----> return $res
10	802	----> if not test -f project.json \
		-o -f global.json \
		-o -f paket.dependencies \
		-o (count *.csproj) -gt 0 \
		-o (count *.fsproj) -gt 0 \
		-o (count *.xproj) -gt 0 \
		-o (count *.sln) -gt 0
		return
	...
511	782	-----> not test -f project.json \
		-o -f global.json \
		-o -f paket.dependencies \
		-o (count *.csproj) -gt 0 \
		-o (count *.fsproj) -gt 0 \
		-o (count *.xproj) -gt 0 \
		-o (count *.sln) -gt 0
77	77	------> count *.csproj
64	64	------> count *.fsproj
61	61	------> count *.xproj
69	69	------> count *.sln
10	10	-----> return
33	1313	--> eval __sf_section_$i
44	1280	---> __sf_section_kubecontext
29	49	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_SHOW true
3	20	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
23	39	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_NAMESPACE_SHOW true
3	16	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
23	37	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_PREFIX "at "
3	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
24	38	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
4	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
23	35	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_SYMBOL "☸️  "
3	12	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
19	31	----> __sf_util_set_default SPACEFISH_KUBECONTEXT_COLOR cyan
3	12	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
14	14	----> [ $SPACEFISH_KUBECONTEXT_SHOW = false ]
75	983	----> type -q kubectl
9	9	-----> set -q argv[1]
25	25	-----> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
46	46	-----> argparse -n type -x t,p,P $options -- $argv
3	12	-----> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
9	9	------> set -q _flag_help
10	10	-----> set -l res 1
10	10	-----> set -l mode normal
9	9	-----> set -l multi no
10	10	-----> set -l selection all
9	9	-----> set -l short no
8	23	-----> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
6	6	------> set -q _flag_quiet
9	9	------> set mode quiet
7	7	-----> set -q _flag_all
7	7	-----> set -q _flag_short
6	6	-----> set -q _flag_no_functions
36	710	-----> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
11	11	------> set -l found 0
17	349	------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
14	14	-------> test $selection != files
3	25	-------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
22	22	--------> functions -q -- $i
6	293	-------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
241	287	--------> contains -- $i (builtin -n)
46	46	---------> builtin -n
24	24	------> set -l paths
9	232	------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
17	17	-------> test $multi != yes
113	206	-------> set paths (command -s -- $i)
93	93	--------> command -s -- $i
13	13	------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
9	45	------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
15	15	-------> test $found = 0
21	21	-------> test $mode != quiet
15	15	-----> return $res
10	10	----> return
37	644	--> eval __sf_section_$i
59	607	---> __sf_section_exec_time
32	58	----> __sf_util_set_default SPACEFISH_EXEC_TIME_SHOW true
6	26	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
34	51	----> __sf_util_set_default SPACEFISH_EXEC_TIME_PREFIX "took "
4	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
40	58	----> __sf_util_set_default SPACEFISH_EXEC_TIME_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
26	50	----> __sf_util_set_default SPACEFISH_EXEC_TIME_COLOR yellow
5	24	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
39	66	----> __sf_util_set_default SPACEFISH_EXEC_TIME_ELAPSED 5
6	27	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
21	21	------> not set -q $var
21	21	----> [ $SPACEFISH_EXEC_TIME_SHOW = false ]
28	28	----> set -l command_duration "$CMD_DURATION$cmd_duration"
8	216	----> if test -n "$command_duration" -a "$command_duration" -gt (math "$SPACEFISH_EXEC_TIME_ELAPSED * 1000")
		set -l human_command_duration (echo $command_duration | __sf_util_human_time)
		__sf_lib_section \
			$SPACEFISH_EXEC_TIME_COLOR \
			$SPACEFISH_EXEC_TIME_PREFIX \
			$human_command_duration \
			$SPACEFISH_EXEC_TIME_SUFFIX
	...
157	208	-----> test -n "$command_duration" -a "$command_duration" -gt (math "$SPACEFISH_EXEC_TIME_ELAPSED * 1000")
51	51	------> math "$SPACEFISH_EXEC_TIME_ELAPSED * 1000"
36	177	--> eval __sf_section_$i
32	141	---> __sf_section_line_sep
35	54	----> __sf_util_set_default SPACEFISH_PROMPT_SEPARATE_LINE true
4	19	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
10	55	----> if test "$SPACEFISH_PROMPT_SEPARATE_LINE" = "true"
		echo -e -n \n
	...
25	25	-----> test "$SPACEFISH_PROMPT_SEPARATE_LINE" = "true"
20	20	-----> echo -e -n \n
44	38821	--> eval __sf_section_$i
94	38777	---> __sf_section_battery
28	57	----> __sf_util_set_default SPACEFISH_BATTERY_SHOW true
6	29	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
23	23	------> not set -q $var
30	47	----> __sf_util_set_default SPACEFISH_BATTERY_PREFIX ""
4	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
34	52	----> __sf_util_set_default SPACEFISH_BATTERY_SUFFIX " "
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
31	53	----> __sf_util_set_default SPACEFISH_BATTERY_SYMBOL_CHARGING ⇡
4	22	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
24	42	----> __sf_util_set_default SPACEFISH_BATTERY_SYMBOL_DISCHARGING ⇣
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
34	49	----> __sf_util_set_default SPACEFISH_BATTERY_SYMBOL_FULL •
3	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
31	52	----> __sf_util_set_default SPACEFISH_BATTERY_THRESHOLD 10
4	21	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
14	14	----> [ $SPACEFISH_BATTERY_SHOW = false ]
13	13	----> set -l battery_data
29	29	----> set -l battery_percent
15	15	----> set -l battery_status
8	8	----> set -l battery_color
7	7	----> set -l battery_symbol
38	37310	----> if type -q pmset
		set battery_data (pmset -g batt | grep "InternalBattery")

		# Return if no internal battery
		if test -z (echo $battery_data)
			return
		end

		set battery_percent (echo $battery_data | grep -oE "[0-9]{1,3}%")
		# spaceship has echo $battery_data | awk -F '; *' 'NR==2 { print $2 }', but NR==2 did not return anything.
		set battery_status (echo $battery_data | awk -F '; *' '{ print $2 }')

	# Linux machines
	else if type -q upower
		set -l battery (upower -e | grep battery | head -1)

		[ -z $battery ]; and return

		set -l IFS # Clear IFS to allow for multi-line variables
		set battery_data (upower -i $battery)
		set battery_percent (echo $battery_data | grep percentage | awk '{print $2}')
		set battery_status (echo $battery_data | grep state | awk '{print $2}')

	# Windows machines.
	else if type -q acpi
		set -l battery_data (acpi -b 2>/dev/null | head -1)

		# Return if no battery
		[ -z $battery_data ]; and return

		set battery_percent ( echo $battery_data | awk '{print $4}' )
		set battery_status ( echo $battery_data | awk '{print tolower($3)}' )
	else
		return
	...
99	1078	-----> type -q pmset
17	17	------> set -q argv[1]
31	31	------> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
58	58	------> argparse -n type -x t,p,P $options -- $argv
3	13	------> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
10	10	-------> set -q _flag_help
14	14	------> set -l res 1
18	18	------> set -l mode normal
10	10	------> set -l multi no
9	9	------> set -l selection all
8	8	------> set -l short no
11	35	------> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	-------> set -q _flag_quiet
17	17	-------> set mode quiet
9	9	------> set -q _flag_all
6	6	------> set -q _flag_short
6	6	------> set -q _flag_no_functions
39	728	------> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
19	19	-------> set -l found 0
23	362	-------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
15	15	--------> test $selection != files
4	26	--------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
22	22	---------> functions -q -- $i
10	298	--------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
239	288	---------> contains -- $i (builtin -n)
49	49	----------> builtin -n
18	18	-------> set -l paths
11	234	-------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
14	14	--------> test $multi != yes
119	209	--------> set paths (command -s -- $i)
90	90	---------> command -s -- $i
20	20	-------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
7	36	-------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
17	17	--------> test $found = 0
12	12	--------> test $mode != quiet
17	17	------> return $res
94	1075	-----> type -q upower
18	18	------> set -q argv[1]
29	29	------> set -l options 'h/help' 'a/all' 's/short' 'f/no-functions' 't/type' 'p/path' 'P/force-path' 'q/quiet'
57	57	------> argparse -n type -x t,p,P $options -- $argv
3	14	------> if set -q _flag_help
        __fish_print_help type
        return 0
    ...
11	11	-------> set -q _flag_help
11	11	------> set -l res 1
17	17	------> set -l mode normal
15	15	------> set -l multi no
10	10	------> set -l selection all
8	8	------> set -l short no
7	29	------> if set -q _flag_quiet
        set mode quiet
    else if set -q _flag_type
        set mode type
    else if set -q _flag_path
        set mode path
    else if set -q _flag_force_path
        set mode path
        set selection files
    ...
7	7	-------> set -q _flag_quiet
15	15	-------> set mode quiet
12	12	------> set -q _flag_all
7	7	------> set -q _flag_short
7	7	------> set -q _flag_no_functions
38	732	------> for i in $argv
        # Found will be set to 1 if a match is found.
        set -l found 0

        if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        end

        set -l paths
        if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        end
        for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        end

        if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        end
    ...
13	13	-------> set -l found 0
17	353	-------> if test $selection != files
            if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            end

            if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            end
        ...
21	21	--------> test $selection != files
4	27	--------> if functions -q -- $i
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a function') $i
                        if test $short != yes
                            echo (_ ' with definition')
                            functions $i
                        else
                            set -l func_path (functions --details $i)
                            if not contains -- $func_path - stdin
                                printf (_ ' (defined in %s)') $func_path
                            end
                            echo
                        end
                    case type
                        echo (_ 'function')
                    case path
                        set -l func_path (functions --details $i)
                        switch $func_path
                            case "-"
                            case "n/a"
                            case "stdin"
                                break
                            case "*"
                                echo $func_path
                        end
                end
                if test $multi != yes
                    continue
                end
            ...
23	23	---------> functions -q -- $i
5	288	--------> if contains -- $i (builtin -n)
                set res 0
                set found 1
                switch $mode
                    case normal
                        printf (_ '%s is a builtin\n') $i

                    case type
                        echo (_ 'builtin')
                end
                if test $multi != yes
                    continue
                end
            ...
228	283	---------> contains -- $i (builtin -n)
55	55	----------> builtin -n
22	22	-------> set -l paths
9	191	-------> if test $multi != yes
            set paths (command -s -- $i)
        else
            set paths (command -sa -- $i)
        ...
16	16	--------> test $multi != yes
129	166	--------> set paths (command -s -- $i)
37	37	---------> command -s -- $i
26	97	-------> for path in $paths
            set res 0
            set found 1
            switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            end
            if test $multi != yes
                continue
            end
        ...
14	14	--------> set res 0
17	17	--------> set found 1
13	13	--------> switch $mode
                case normal
                    printf (_ '%s is %s\n') $i $path
                case type
                    echo (_ 'file')
                case path
                    echo $path
            ...
7	27	--------> if test $multi != yes
                continue
            ...
14	14	---------> test $multi != yes
6	6	---------> continue
6	18	-------> if test $found = 0
            and test $mode != quiet
            and test $mode != path
            printf (_ "%s: Could not find '%s'\n") type $i >&2
        ...
12	12	--------> test $found = 0
15	15	------> return $res
166	14942	-----> set -l battery (upower -e | grep battery | head -1)
586	14776	------> upower -e | grep battery | head -1
14190	14190	-------> command grep --color=auto $argv
29	29	-----> [ -z $battery ]
15	15	-----> set -l IFS
310	9904	-----> set battery_data (upower -i $battery)
9594	9594	------> upower -i $battery
174	4904	-----> set battery_percent (echo $battery_data | grep percentage | awk '{print $2}')
843	4730	------> echo $battery_data | grep percentage | awk '{print $2}'
3887	3887	-------> command grep --color=auto $argv
154	5325	-----> set battery_status (echo $battery_data | grep state | awk '{print $2}')
1994	5171	------> echo $battery_data | grep state | awk '{print $2}'
3177	3177	-------> command grep --color=auto $argv
152	265	----> set battery_percent (echo $battery_percent | string trim --chars=%[,;])
113	113	-----> echo $battery_percent | string trim --chars=%[,;]
13	321	----> if test "$battery_percent" -eq 100 -o -n (echo (string match -r "(charged|full)" $battery_status))
		set battery_color green
	else if test "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD"
		set battery_color red
	else
		set battery_color yellow
	...
122	269	-----> test "$battery_percent" -eq 100 -o -n (echo (string match -r "(charged|full)" $battery_status))
96	147	------> echo (string match -r "(charged|full)" $battery_status)
51	51	-------> string match -r "(charged|full)" $battery_status
24	24	-----> test "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD"
15	15	-----> set battery_color yellow
6	54	----> if test "$battery_status" = "charging"
		set battery_symbol $SPACEFISH_BATTERY_SYMBOL_CHARGING
	else if test -n (echo (string match -r "^[dD]ischarg.*" $battery_status))
		set battery_symbol $SPACEFISH_BATTERY_SYMBOL_DISCHARGING
	else
		set battery_symbol $SPACEFISH_BATTERY_SYMBOL_FULL
	...
16	16	-----> test "$battery_status" = "charging"
32	32	-----> set battery_symbol $SPACEFISH_BATTERY_SYMBOL_CHARGING
7	295	----> if test "$SPACEFISH_BATTERY_SHOW" = "always" \
	-o "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD" \
	-o "$SPACEFISH_BATTERY_SHOW" = "charged" \
	-a -n (echo (string match -r "(charged|full)" $battery_status))
		__sf_lib_section \
			$battery_color \
			$SPACEFISH_BATTERY_PREFIX \
			"$battery_symbol$battery_percent%" \
			$SPACEFISH_BATTERY_SUFFIX
	...
134	288	-----> test "$SPACEFISH_BATTERY_SHOW" = "always" \
	-o "$battery_percent" -lt "$SPACEFISH_BATTERY_THRESHOLD" \
	-o "$SPACEFISH_BATTERY_SHOW" = "charged" \
	-a -n (echo (string match -r "(charged|full)" $battery_status))
116	154	------> echo (string match -r "(charged|full)" $battery_status)
38	38	-------> string match -r "(charged|full)" $battery_status
44	597	--> eval __sf_section_$i
72	553	---> __sf_section_vi_mode
35	70	----> __sf_util_set_default SPACEFISH_VI_MODE_SHOW true
6	35	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
29	29	------> not set -q $var
34	59	----> __sf_util_set_default SPACEFISH_VI_MODE_PREFIX " "
6	25	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
34	53	----> __sf_util_set_default SPACEFISH_VI_MODE_SUFFIX $SPACEFISH_PROMPT_DEFAULT_SUFFIX
5	19	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
32	50	----> __sf_util_set_default SPACEFISH_VI_MODE_INSERT [I]
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
27	50	----> __sf_util_set_default SPACEFISH_VI_MODE_NORMAL [N]
4	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
19	19	------> not set -q $var
24	38	----> __sf_util_set_default SPACEFISH_VI_MODE_VISUAL [V]
3	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
34	51	----> __sf_util_set_default SPACEFISH_VI_MODE_REPLACE_ONE [R]
4	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
24	47	----> __sf_util_set_default SPACEFISH_VI_MODE_COLOR white
5	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
17	17	----> [ $SPACEFISH_VI_MODE_SHOW = false ]
17	17	----> [ "$fish_key_bindings" = "fish_vi_key_bindings" ]
18	18	----> [ "$fish_key_bindings" = "fish_hybrid_key_bindings" ]
11	11	----> return
44	2593	--> eval __sf_section_$i
63	2549	---> __sf_section_jobs
32	55	----> __sf_util_set_default SPACEFISH_JOBS_SHOW true
6	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
34	50	----> __sf_util_set_default SPACEFISH_JOBS_PREFIX ""
4	16	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
41	59	----> __sf_util_set_default SPACEFISH_JOBS_SUFFIX " "
4	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
14	14	------> not set -q $var
25	51	----> __sf_util_set_default SPACEFISH_JOBS_SYMBOL ✦
6	26	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
20	20	------> not set -q $var
25	40	----> __sf_util_set_default SPACEFISH_JOBS_COLOR blue
3	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
39	57	----> __sf_util_set_default SPACEFISH_JOBS_AMOUNT_PREFIX ""
3	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
29	52	----> __sf_util_set_default SPACEFISH_JOBS_AMOUNT_SUFFIX ""
5	23	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
18	18	------> not set -q $var
22	35	----> __sf_util_set_default SPACEFISH_JOBS_AMOUNT_THRESHOLD 1
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
26	26	----> [ $SPACEFISH_JOBS_SHOW = false ]
139	2025	----> set jobs_amount (jobs | wc -l | xargs)
1886	1886	-----> jobs | wc -l | xargs
8	36	----> if test $jobs_amount -eq 0
		return
	...
21	21	-----> test $jobs_amount -eq 0
7	7	-----> return
35	297	--> eval __sf_section_$i
36	262	---> __sf_section_exit_code
30	51	----> __sf_util_set_default SPACEFISH_EXIT_CODE_SHOW false
4	21	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
17	17	------> not set -q $var
27	45	----> __sf_util_set_default SPACEFISH_EXIT_CODE_PREFIX ""
3	18	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
15	15	------> not set -q $var
24	37	----> __sf_util_set_default SPACEFISH_EXIT_CODE_SUFFIX " "
2	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
11	11	------> not set -q $var
24	37	----> __sf_util_set_default SPACEFISH_EXIT_CODE_SYMBOL ✘
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
21	36	----> __sf_util_set_default SPACEFISH_EXIT_CODE_COLOR red
3	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
14	14	----> [ $SPACEFISH_EXIT_CODE_SHOW = false ]
6	6	----> return
28	882	--> eval __sf_section_$i
63	854	---> __sf_section_char
26	41	----> __sf_util_set_default SPACEFISH_CHAR_PREFIX ""
3	15	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
27	40	----> __sf_util_set_default SPACEFISH_CHAR_SUFFIX " "
3	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
10	10	------> not set -q $var
29	46	----> __sf_util_set_default SPACEFISH_CHAR_SYMBOL ➜
4	17	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
13	13	------> not set -q $var
23	37	----> __sf_util_set_default SPACEFISH_CHAR_COLOR_SUCCESS green
2	14	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
12	12	------> not set -q $var
23	36	----> __sf_util_set_default SPACEFISH_CHAR_COLOR_FAILURE red
4	13	-----> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
9	9	------> not set -q $var
12	12	----> set -l color
7	38	----> if test $sf_exit_code -eq 0
		set color $SPACEFISH_CHAR_COLOR_SUCCESS
	else
		set color $SPACEFISH_CHAR_COLOR_FAILURE
	...
16	16	-----> test $sf_exit_code -eq 0
15	15	-----> set color $SPACEFISH_CHAR_COLOR_FAILURE
67	541	----> __sf_lib_section \
		$color \
		$SPACEFISH_CHAR_PREFIX \
		$SPACEFISH_CHAR_SYMBOL \
		$SPACEFISH_CHAR_SUFFIX
7	166	-----> if test (count $argv) -eq 2
		set content $argv[2]
		set prefix
	...
121	159	------> test (count $argv) -eq 2
38	38	-------> count $argv
17	120	-----> if test "$sf_prompt_opened" = "true" -a "$SPACEFISH_PROMPT_PREFIXES_SHOW" = "true"
		# Echo prefixes in bold white
		set_color --bold
		echo -e -n -s $prefix
		set_color normal
	...
34	34	------> test "$sf_prompt_opened" = "true" -a "$SPACEFISH_PROMPT_PREFIXES_SHOW" = "true"
32	32	------> set_color --bold
22	22	------> echo -e -n -s $prefix
15	15	------> set_color normal
23	23	-----> set -g sf_prompt_opened true
33	33	-----> set_color --bold $color
20	20	-----> echo -e -n $content
24	24	-----> set_color normal
14	88	-----> if test "$SPACEFISH_PROMPT_SUFFIXES_SHOW" = "true"
		# Echo suffixes in bold white
		set_color --bold
		echo -e -n -s $suffix
		set_color normal
	...
18	18	------> test "$SPACEFISH_PROMPT_SUFFIXES_SHOW" = "true"
14	14	------> set_color --bold
27	27	------> echo -e -n -s $suffix
15	15	------> set_color normal
27	27	-> set_color normal
36	121	> fish_right_prompt
36	65	-> __sf_util_set_default SPACEFISH_RPROMPT_ORDER ""
6	29	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
23	23	---> not set -q $var
14	14	-> [ -n "$SPACEFISH_RPROMPT_ORDER" ]
6	6	-> return
32	327	> fish_title
13	295	-> if not set -q INSIDE_EMACS
        echo (status current-command) (__fish_pwd)
    ...
21	21	--> not set -q INSIDE_EMACS
178	261	--> echo (status current-command) (__fish_pwd)
22	22	---> status current-command
35	61	---> __fish_pwd
26	26	----> pwd
81	444	> up-or-search
13	51	-> if commandline --search-mode
        commandline -f history-search-backward
        return
    ...
38	38	--> commandline --search-mode
5	27	-> if commandline --paging-mode
        commandline -f up-line
        return
    ...
22	22	--> commandline --paging-mode
193	235	-> set lineno (commandline -L)
42	42	--> commandline -L
22	50	-> switch $lineno
        case 1
            commandline -f history-search-backward

        case '*'
            commandline -f up-line
    ...
28	28	--> commandline -f history-search-backward
634	1296	> source /usr/share/fish/completions/fish.fish
76	76	-> complete -c fish -s c -l command -d "Run specified command instead of interactive session" -x -a "(__fish_complete_command)"
53	53	-> complete -c fish -s C -l init-command -d "Run specified command before session" -x -a "(__fish_complete_command)"
33	33	-> complete -c fish -s h -l help -d "Display help and exit"
28	28	-> complete -c fish -s v -l version -d "Display version and exit"
29	29	-> complete -c fish -s n -l no-execute -d "Only parse input, do not execute"
29	29	-> complete -c fish -s i -l interactive -d "Run in interactive mode"
28	28	-> complete -c fish -s l -l login -d "Run as a login shell"
29	29	-> complete -c fish -s p -l profile -d "Output profiling information to specified file" -r
86	86	-> complete -c fish -s d -l debug -d "Specify verbosity level" -x -a "0\t'Warnings silenced'
1\t'Default'
2\t'Basic debug output'
3\t'More debug output'
4\t'Much more debug output'
5\t'Too much debug output'
(fish --print-debug-categories | string replace ' ' \t)"
56	56	-> complete -c fish -s D -l debug-stack-frames -d "Show specified # of frames with debug output" -x -a "(seq 128)\t\n"
40	40	-> complete -c fish -s P -l private -d "Do not persist history"
14	14	-> function __fish_complete_features
    set -l arg_comma (commandline -tc | string replace -rf '(.*,)[^,]*' '$1' | string replace -r -- '--.*=' '')
    set -l features (status features | string replace -rf '^([\w-]+).*\t(.*)$' '$1\t$2')
    printf "%s\n" "$arg_comma"$features #TODO: remove existing args
...
61	61	-> complete -c fish -s f -l features -d "Run with comma-separated feature flags enabled" -a "(__fish_complete_features)" -x
30	30	-> complete -c fish -l print-rusage-self -d "Print stats from getrusage at exit" -f
27	27	-> complete -c fish -l print-debug-categories -d "Print the debug categories fish knows" -f
43	43	-> complete -c fish -x -a "(__fish_complete_suffix .fish)"
325	2171	> __fish_complete_suffix .fish
591	607	-> source /usr/share/fish/functions/__fish_complete_suffix.fish
16	16	--> function __fish_complete_suffix -d "Complete using files"

    # Variable declarations

    set -l comp
    set -l suff
    set -l desc
    set -l files
    set -l prefix ""

    switch (count $argv)

        case 1
            set comp (commandline -ct)
            set suff $argv
            set desc ""

        case 2
            set comp $argv[1]
            set suff $argv[2]
            set desc ""

        case 3
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]

        case 4
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]
            set prefix $argv[4]

            # Only directories are supported as prefixes, and to use the same logic
            # for both absolute prefixed paths and relative non-prefixed paths, $prefix
            # must terminate in a `/` if it is present, so it can be unconditionally
            # prefixed to any path to get the desired result.
            if not string match -qr '/$' $prefix
                set prefix $prefix/
            end
    end

    # Strip leading ./ as it confuses the detection of base and suffix
    # It is conditionally re-added below.
    set base $prefix(string replace -r '^("\')?\\./' '' -- $comp | string trim -c '\'"') # " make emacs syntax highlighting happy

    set -l all
    set -l dirs
    # If $comp is "./ma" and the file is "main.py", we'll catch that case here,
    # but complete.cpp will not consider it a match, so we have to output the
    # correct form.

    # Also do directory completion, since there might be files with the correct
    # suffix in a subdirectory.
    set all $base*$suff
    if not string match -qr '/$' -- $suff
        set dirs $base*/

        # The problem is that we now have each directory included twice in the output,
        # once as `dir` and once as `dir/`. The runtime here is O(n) for n directories
        # in the output, but hopefully since we have only one level (no nested results)
        # it should be fast. The alternative is to shell out to `sort` and remove any
        # duplicate results, but it would have to be a huge `n` to make up for the fork
        # overhead.
        for dir in $dirs
            set all (string match -v (string match -r '(.*)/$' -- $dir)[2] -- $all)
        end
    end

    set files $all $dirs
    if string match -qr '^\\./' -- $comp
        set files ./$files
    else
        # "Escape" files starting with a literal dash `-` with a `./`
        set files (string replace -r -- "^-" "./-" $files)
    end

    if set -q files[1]
        if string match -qr -- . "$desc"
            set desc "\t$desc"
        end
        if string match -qr -- . "$prefix"
            # Ideally, only replace in the beginning of the string, but we have no
            # way of doing a pcre2 escape so we can use a regex replace instead
            set files (string replace $prefix "" $files)
        end
        printf "%s$desc\n" $files
    end

...
29	29	-> set -l comp
17	17	-> set -l suff
16	16	-> set -l desc
16	16	-> set -l files
15	15	-> set -l prefix ""
130	374	-> switch (count $argv)

        case 1
            set comp (commandline -ct)
            set suff $argv
            set desc ""

        case 2
            set comp $argv[1]
            set suff $argv[2]
            set desc ""

        case 3
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]

        case 4
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]
            set prefix $argv[4]

            # Only directories are supported as prefixes, and to use the same logic
            # for both absolute prefixed paths and relative non-prefixed paths, $prefix
            # must terminate in a `/` if it is present, so it can be unconditionally
            # prefixed to any path to get the desired result.
            if not string match -qr '/$' $prefix
                set prefix $prefix/
            end
    ...
44	44	--> count $argv
129	161	--> set comp (commandline -ct)
32	32	---> commandline -ct
26	26	--> set suff $argv
13	13	--> set desc ""
133	279	-> set base $prefix(string replace -r '^("\')?\\./' '' -- $comp | string trim -c '\'"')
146	146	--> string replace -r '^("\')?\\./' '' -- $comp | string trim -c '\'"'
27	27	-> set -l all
17	17	-> set -l dirs
86	86	-> set all $base*$suff
16	130	-> if not string match -qr '/$' -- $suff
        set dirs $base*/

        # The problem is that we now have each directory included twice in the output,
        # once as `dir` and once as `dir/`. The runtime here is O(n) for n directories
        # in the output, but hopefully since we have only one level (no nested results)
        # it should be fast. The alternative is to shell out to `sort` and remove any
        # duplicate results, but it would have to be a huge `n` to make up for the fork
        # overhead.
        for dir in $dirs
            set all (string match -v (string match -r '(.*)/$' -- $dir)[2] -- $all)
        end
    ...
40	40	--> not string match -qr '/$' -- $suff
54	54	--> set dirs $base*/
20	20	--> for dir in $dirs
            set all (string match -v (string match -r '(.*)/$' -- $dir)[2] -- $all)
        ...
19	19	-> set files $all $dirs
14	195	-> if string match -qr '^\\./' -- $comp
        set files ./$files
    else
        # "Escape" files starting with a literal dash `-` with a `./`
        set files (string replace -r -- "^-" "./-" $files)
    ...
31	31	--> string match -qr '^\\./' -- $comp
115	150	--> set files (string replace -r -- "^-" "./-" $files)
35	35	---> string replace -r -- "^-" "./-" $files
5	19	-> if set -q files[1]
        if string match -qr -- . "$desc"
            set desc "\t$desc"
        end
        if string match -qr -- . "$prefix"
            # Ideally, only replace in the beginning of the string, but we have no
            # way of doing a pcre2 escape so we can use a regex replace instead
            set files (string replace $prefix "" $files)
        end
        printf "%s$desc\n" $files
    ...
14	14	--> set -q files[1]
139	1443	> __fish_complete_suffix .fish
33	33	-> set -l comp
17	17	-> set -l suff
13	13	-> set -l desc
13	13	-> set -l files
22	22	-> set -l prefix ""
143	402	-> switch (count $argv)

        case 1
            set comp (commandline -ct)
            set suff $argv
            set desc ""

        case 2
            set comp $argv[1]
            set suff $argv[2]
            set desc ""

        case 3
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]

        case 4
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]
            set prefix $argv[4]

            # Only directories are supported as prefixes, and to use the same logic
            # for both absolute prefixed paths and relative non-prefixed paths, $prefix
            # must terminate in a `/` if it is present, so it can be unconditionally
            # prefixed to any path to get the desired result.
            if not string match -qr '/$' $prefix
                set prefix $prefix/
            end
    ...
47	47	--> count $argv
142	177	--> set comp (commandline -ct)
35	35	---> commandline -ct
22	22	--> set suff $argv
13	13	--> set desc ""
134	264	-> set base $prefix(string replace -r '^("\')?\\./' '' -- $comp | string trim -c '\'"')
130	130	--> string replace -r '^("\')?\\./' '' -- $comp | string trim -c '\'"'
14	14	-> set -l all
10	10	-> set -l dirs
68	68	-> set all $base*$suff
13	138	-> if not string match -qr '/$' -- $suff
        set dirs $base*/

        # The problem is that we now have each directory included twice in the output,
        # once as `dir` and once as `dir/`. The runtime here is O(n) for n directories
        # in the output, but hopefully since we have only one level (no nested results)
        # it should be fast. The alternative is to shell out to `sort` and remove any
        # duplicate results, but it would have to be a huge `n` to make up for the fork
        # overhead.
        for dir in $dirs
            set all (string match -v (string match -r '(.*)/$' -- $dir)[2] -- $all)
        end
    ...
32	32	--> not string match -qr '/$' -- $suff
78	78	--> set dirs $base*/
15	15	--> for dir in $dirs
            set all (string match -v (string match -r '(.*)/$' -- $dir)[2] -- $all)
        ...
18	18	-> set files $all $dirs
9	192	-> if string match -qr '^\\./' -- $comp
        set files ./$files
    else
        # "Escape" files starting with a literal dash `-` with a `./`
        set files (string replace -r -- "^-" "./-" $files)
    ...
27	27	--> string match -qr '^\\./' -- $comp
120	156	--> set files (string replace -r -- "^-" "./-" $files)
36	36	---> string replace -r -- "^-" "./-" $files
9	100	-> if set -q files[1]
        if string match -qr -- . "$desc"
            set desc "\t$desc"
        end
        if string match -qr -- . "$prefix"
            # Ideally, only replace in the beginning of the string, but we have no
            # way of doing a pcre2 escape so we can use a regex replace instead
            set files (string replace $prefix "" $files)
        end
        printf "%s$desc\n" $files
    ...
12	12	--> set -q files[1]
3	28	--> if string match -qr -- . "$desc"
            set desc "\t$desc"
        ...
25	25	---> string match -qr -- . "$desc"
3	23	--> if string match -qr -- . "$prefix"
            # Ideally, only replace in the beginning of the string, but we have no
            # way of doing a pcre2 escape so we can use a regex replace instead
            set files (string replace $prefix "" $files)
        ...
20	20	---> string match -qr -- . "$prefix"
28	28	--> printf "%s$desc\n" $files
127	1481	> __fish_complete_suffix .fish
29	29	-> set -l comp
12	12	-> set -l suff
11	11	-> set -l desc
16	16	-> set -l files
19	19	-> set -l prefix ""
110	334	-> switch (count $argv)

        case 1
            set comp (commandline -ct)
            set suff $argv
            set desc ""

        case 2
            set comp $argv[1]
            set suff $argv[2]
            set desc ""

        case 3
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]

        case 4
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]
            set prefix $argv[4]

            # Only directories are supported as prefixes, and to use the same logic
            # for both absolute prefixed paths and relative non-prefixed paths, $prefix
            # must terminate in a `/` if it is present, so it can be unconditionally
            # prefixed to any path to get the desired result.
            if not string match -qr '/$' $prefix
                set prefix $prefix/
            end
    ...
39	39	--> count $argv
125	152	--> set comp (commandline -ct)
27	27	---> commandline -ct
16	16	--> set suff $argv
17	17	--> set desc ""
141	303	-> set base $prefix(string replace -r '^("\')?\\./' '' -- $comp | string trim -c '\'"')
162	162	--> string replace -r '^("\')?\\./' '' -- $comp | string trim -c '\'"'
15	15	-> set -l all
17	17	-> set -l dirs
82	82	-> set all $base*$suff
14	127	-> if not string match -qr '/$' -- $suff
        set dirs $base*/

        # The problem is that we now have each directory included twice in the output,
        # once as `dir` and once as `dir/`. The runtime here is O(n) for n directories
        # in the output, but hopefully since we have only one level (no nested results)
        # it should be fast. The alternative is to shell out to `sort` and remove any
        # duplicate results, but it would have to be a huge `n` to make up for the fork
        # overhead.
        for dir in $dirs
            set all (string match -v (string match -r '(.*)/$' -- $dir)[2] -- $all)
        end
    ...
32	32	--> not string match -qr '/$' -- $suff
63	63	--> set dirs $base*/
18	18	--> for dir in $dirs
            set all (string match -v (string match -r '(.*)/$' -- $dir)[2] -- $all)
        ...
25	25	-> set files $all $dirs
12	230	-> if string match -qr '^\\./' -- $comp
        set files ./$files
    else
        # "Escape" files starting with a literal dash `-` with a `./`
        set files (string replace -r -- "^-" "./-" $files)
    ...
29	29	--> string match -qr '^\\./' -- $comp
140	189	--> set files (string replace -r -- "^-" "./-" $files)
49	49	---> string replace -r -- "^-" "./-" $files
17	134	-> if set -q files[1]
        if string match -qr -- . "$desc"
            set desc "\t$desc"
        end
        if string match -qr -- . "$prefix"
            # Ideally, only replace in the beginning of the string, but we have no
            # way of doing a pcre2 escape so we can use a regex replace instead
            set files (string replace $prefix "" $files)
        end
        printf "%s$desc\n" $files
    ...
13	13	--> set -q files[1]
4	38	--> if string match -qr -- . "$desc"
            set desc "\t$desc"
        ...
34	34	---> string match -qr -- . "$desc"
5	27	--> if string match -qr -- . "$prefix"
            # Ideally, only replace in the beginning of the string, but we have no
            # way of doing a pcre2 escape so we can use a regex replace instead
            set files (string replace $prefix "" $files)
        ...
22	22	---> string match -qr -- . "$prefix"
39	39	--> printf "%s$desc\n" $files
160	1717	> __fish_complete_suffix .fish
35	35	-> set -l comp
15	15	-> set -l suff
11	11	-> set -l desc
17	17	-> set -l files
19	19	-> set -l prefix ""
137	392	-> switch (count $argv)

        case 1
            set comp (commandline -ct)
            set suff $argv
            set desc ""

        case 2
            set comp $argv[1]
            set suff $argv[2]
            set desc ""

        case 3
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]

        case 4
            set comp $argv[1]
            set suff $argv[2]
            set desc $argv[3]
            set prefix $argv[4]

            # Only directories are supported as prefixes, and to use the same logic
            # for both absolute prefixed paths and relative non-prefixed paths, $prefix
            # must terminate in a `/` if it is present, so it can be unconditionally
            # prefixed to any path to get the desired result.
            if not string match -qr '/$' $prefix
                set prefix $prefix/
            end
    ...
40	40	--> count $argv
141	172	--> set comp (commandline -ct)
31	31	---> commandline -ct
24	24	--> set suff $argv
19	19	--> set desc ""
138	328	-> set base $prefix(string replace -r '^("\')?\\./' '' -- $comp | string trim -c '\'"')
190	190	--> string replace -r '^("\')?\\./' '' -- $comp | string trim -c '\'"'
20	20	-> set -l all
12	12	-> set -l dirs
78	78	-> set all $base*$suff
19	132	-> if not string match -qr '/$' -- $suff
        set dirs $base*/

        # The problem is that we now have each directory included twice in the output,
        # once as `dir` and once as `dir/`. The runtime here is O(n) for n directories
        # in the output, but hopefully since we have only one level (no nested results)
        # it should be fast. The alternative is to shell out to `sort` and remove any
        # duplicate results, but it would have to be a huge `n` to make up for the fork
        # overhead.
        for dir in $dirs
            set all (string match -v (string match -r '(.*)/$' -- $dir)[2] -- $all)
        end
    ...
38	38	--> not string match -qr '/$' -- $suff
55	55	--> set dirs $base*/
20	20	--> for dir in $dirs
            set all (string match -v (string match -r '(.*)/$' -- $dir)[2] -- $all)
        ...
20	20	-> set files $all $dirs
11	225	-> if string match -qr '^\\./' -- $comp
        set files ./$files
    else
        # "Escape" files starting with a literal dash `-` with a `./`
        set files (string replace -r -- "^-" "./-" $files)
    ...
33	33	--> string match -qr '^\\./' -- $comp
138	181	--> set files (string replace -r -- "^-" "./-" $files)
43	43	---> string replace -r -- "^-" "./-" $files
48	253	-> if set -q files[1]
        if string match -qr -- . "$desc"
            set desc "\t$desc"
        end
        if string match -qr -- . "$prefix"
            # Ideally, only replace in the beginning of the string, but we have no
            # way of doing a pcre2 escape so we can use a regex replace instead
            set files (string replace $prefix "" $files)
        end
        printf "%s$desc\n" $files
    ...
25	25	--> set -q files[1]
5	38	--> if string match -qr -- . "$desc"
            set desc "\t$desc"
        ...
33	33	---> string match -qr -- . "$desc"
11	38	--> if string match -qr -- . "$prefix"
            # Ideally, only replace in the beginning of the string, but we have no
            # way of doing a pcre2 escape so we can use a regex replace instead
            set files (string replace $prefix "" $files)
        ...
27	27	---> string match -qr -- . "$prefix"
104	104	--> printf "%s$desc\n" $files
61	254	> __fish_disable_bracketed_paste 'fish --profile x.x'
193	193	-> printf "\e[?2004l"
48	789	> fish_title fish\ --profile\ x.x
17	741	-> if not set -q INSIDE_EMACS
        echo (status current-command) (__fish_pwd)
    ...
21	21	--> not set -q INSIDE_EMACS
416	703	--> echo (status current-command) (__fish_pwd)
25	25	---> status current-command
170	262	---> __fish_pwd
92	92	----> pwd
299126346	299126346	> fish --profile x.x
461	484	> source /home/micro-p/.config/fish/functions/fish_prompt.fish
23	23	-> function fish_prompt
    if test -n "$SSH_TTY"
        echo -n (set_color brred)"$USER"(set_color white)'@'(set_color yellow)(prompt_hostname)' '
    end

    echo -n (set_color blue)(prompt_pwd)' '

    set_color -o
    if test "$USER" = 'root'
        echo -n (set_color red)'# '
    end
    echo -n (set_color red)'❯'(set_color yellow)'❯'(set_color green)'❯ '
    set_color normal
...
43	268	> __fish_enable_bracketed_paste
225	225	-> printf "\e[?2004h"
49	49	> fish_mode_prompt
68	2705	> fish_prompt
7	45	-> if test -n "$SSH_TTY"
        echo -n (set_color brred)"$USER"(set_color white)'@'(set_color yellow)(prompt_hostname)' '
    ...
38	38	--> test -n "$SSH_TTY"
455	1992	-> echo -n (set_color blue)(prompt_pwd)' '
221	221	--> set_color blue
410	1316	--> prompt_pwd
330	356	---> source /usr/share/fish/functions/prompt_pwd.fish
26	26	----> function prompt_pwd --description "Print the current working directory, shortened to fit the prompt"
    set -l options 'h/help'
    argparse -n prompt_pwd --max-args=0 $options -- $argv
    or return

    if set -q _flag_help
        __fish_print_help prompt_pwd
        return 0
    end

    # This allows overriding fish_prompt_pwd_dir_length from the outside (global or universal) without leaking it
    set -q fish_prompt_pwd_dir_length
    or set -l fish_prompt_pwd_dir_length 1

    # Replace $HOME with "~"
    set realhome ~
    set -l tmp (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD)

    if [ $fish_prompt_pwd_dir_length -eq 0 ]
        echo $tmp
    else
        # Shorten to at most $fish_prompt_pwd_dir_length characters per directory
        string replace -ar '(\.?[^/]{'"$fish_prompt_pwd_dir_length"'})[^/]*/' '$1/' $tmp
    end
...
31	31	---> set -l options 'h/help'
28	28	---> argparse -n prompt_pwd --max-args=0 $options -- $argv
4	14	---> if set -q _flag_help
        __fish_print_help prompt_pwd
        return 0
    ...
10	10	----> set -q _flag_help
8	8	---> set -q fish_prompt_pwd_dir_length
13	13	---> set -l fish_prompt_pwd_dir_length 1
22	22	---> set realhome ~
151	298	---> set -l tmp (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD)
147	147	----> string replace -r '^'"$realhome"'($|/)' '~$1' $PWD
17	136	---> if [ $fish_prompt_pwd_dir_length -eq 0 ]
        echo $tmp
    else
        # Shorten to at most $fish_prompt_pwd_dir_length characters per directory
        string replace -ar '(\.?[^/]{'"$fish_prompt_pwd_dir_length"'})[^/]*/' '$1/' $tmp
    ...
37	37	----> [ $fish_prompt_pwd_dir_length -eq 0 ]
82	82	----> string replace -ar '(\.?[^/]{'"$fish_prompt_pwd_dir_length"'})[^/]*/' '$1/' $tmp
34	34	-> set_color -o
4	36	-> if test "$USER" = 'root'
        echo -n (set_color red)'# '
    ...
32	32	--> test "$USER" = 'root'
336	510	-> echo -n (set_color red)'❯'(set_color yellow)'❯'(set_color green)'❯ '
43	43	--> set_color red
89	89	--> set_color yellow
42	42	--> set_color green
20	20	-> set_color normal
51	226	> fish_right_prompt
122	152	-> __sf_util_set_default SPACEFISH_RPROMPT_ORDER ""
5	30	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
25	25	---> not set -q $var
17	17	-> [ -n "$SPACEFISH_RPROMPT_ORDER" ]
6	6	-> return
33	529	> fish_title
11	496	-> if not set -q INSIDE_EMACS
        echo (status current-command) (__fish_pwd)
    ...
19	19	--> not set -q INSIDE_EMACS
235	466	--> echo (status current-command) (__fish_pwd)
19	19	---> status current-command
119	212	---> __fish_pwd
93	93	----> pwd
33	33	> fish_mode_prompt
44	1298	> fish_prompt
5	32	-> if test -n "$SSH_TTY"
        echo -n (set_color brred)"$USER"(set_color white)'@'(set_color yellow)(prompt_hostname)' '
    ...
27	27	--> test -n "$SSH_TTY"
214	737	-> echo -n (set_color blue)(prompt_pwd)' '
32	32	--> set_color blue
90	491	--> prompt_pwd
42	42	---> set -l options 'h/help'
36	36	---> argparse -n prompt_pwd --max-args=0 $options -- $argv
5	16	---> if set -q _flag_help
        __fish_print_help prompt_pwd
        return 0
    ...
11	11	----> set -q _flag_help
8	8	---> set -q fish_prompt_pwd_dir_length
13	13	---> set -l fish_prompt_pwd_dir_length 1
23	23	---> set realhome ~
132	190	---> set -l tmp (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD)
58	58	----> string replace -r '^'"$realhome"'($|/)' '~$1' $PWD
10	73	---> if [ $fish_prompt_pwd_dir_length -eq 0 ]
        echo $tmp
    else
        # Shorten to at most $fish_prompt_pwd_dir_length characters per directory
        string replace -ar '(\.?[^/]{'"$fish_prompt_pwd_dir_length"'})[^/]*/' '$1/' $tmp
    ...
20	20	----> [ $fish_prompt_pwd_dir_length -eq 0 ]
43	43	----> string replace -ar '(\.?[^/]{'"$fish_prompt_pwd_dir_length"'})[^/]*/' '$1/' $tmp
20	20	-> set_color -o
5	22	-> if test "$USER" = 'root'
        echo -n (set_color red)'# '
    ...
17	17	--> test "$USER" = 'root'
299	398	-> echo -n (set_color red)'❯'(set_color yellow)'❯'(set_color green)'❯ '
31	31	--> set_color red
35	35	--> set_color yellow
33	33	--> set_color green
45	45	-> set_color normal
38	125	> fish_right_prompt
40	66	-> __sf_util_set_default SPACEFISH_RPROMPT_ORDER ""
4	26	--> if not set -q $var
		# Multiple arguments will become a list
		set -g $var $argv[2..-1]
	...
22	22	---> not set -q $var
14	14	-> [ -n "$SPACEFISH_RPROMPT_ORDER" ]
7	7	-> return
32	572	> fish_title
12	540	-> if not set -q INSIDE_EMACS
        echo (status current-command) (__fish_pwd)
    ...
18	18	--> not set -q INSIDE_EMACS
416	510	--> echo (status current-command) (__fish_pwd)
28	28	---> status current-command
43	66	---> __fish_pwd
23	23	----> pwd
55	108	> __fish_disable_bracketed_paste exit
53	53	-> printf "\e[?2004l"
38	509	> fish_title exit
18	471	-> if not set -q INSIDE_EMACS
        echo (status current-command) (__fish_pwd)
    ...
20	20	--> not set -q INSIDE_EMACS
288	433	--> echo (status current-command) (__fish_pwd)
68	68	---> status current-command
47	77	---> __fish_pwd
30	30	----> pwd
100	100	> exit
28	57	> __fish_disable_bracketed_paste 3
29	29	-> printf "\e[?2004l"
