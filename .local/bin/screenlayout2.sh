#!/bin/sh
xrandr --output eDP1 --primary --mode 1600x900 --pos 0x180 --rotate normal --gamma 1:0.9:1 --output DP1 --mode 1920x1080 --pos 1600x0 --rotate normal --output HDMI1 --off --output HDMI2 --off --output VIRTUAL1 --off
