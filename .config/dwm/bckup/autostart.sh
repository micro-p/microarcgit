#!/bin/zsh

xset r rate 300 50
setxkbmap -option caps:swapescape
picom -b &
feh --bg-fill --no-fehbg /wall.png &
