#aliases
# alias -g G=' | grep -i'
# alias -g W=' | wc'
alias config='/usr/bin/git --git-dir=$HOME/.mcrparcogit/ --work-tree=$HOME'
alias xfeh='feh -g 640x480 -d -S filename'
alias vi="sudo nvim"
alias vzconf="nvim ~/.zshrc"
alias vmpconf="nvim ~/.config/mpv/mpv.conf"
alias rm="rm -vI"
alias v="nvim"
alias vim="nvim"
alias ll="lsd -lhF --total-size --group-dirs first"
alias llf="lsd -lhF --group-dirs first"
alias lla="lsd -AlhF --total-size --group-dirs first"
alias la="lsd -AFh --group-dirs first"
alias ls="lsd -Fh --group-dirs first"
alias l="lsd -Fh --group-dirs first"
alias mv="mv -v"
alias weatherz="curl v2.wttr.in/zagazig"
alias map='telnet mapscii.me'
alias umatrix='unimatrix -a -l naASg -f -c cyan -s 98'
# alias sp="/usr/local/bin/sp"
alias cp="cp -v"
alias mpa="mpv --no-video"
alias m='mpvf'
alias syssusp='betterlockscreen -t "Micro-P Locked" -s -l'
# alias alacritty='WINIT_HIDPI_FACTOR=1 alacritty'
# neofetch | lolcat -a -s 5000 -S 9 -p 3
alias clneo='clneo && ~/.local/bin/COVID19'
alias clneol='~/.local/bin/clneo && ~/.local/bin/COVID19 && lsd -Fh --group-dirs first'
alias trm='trash-put -v'
# alias trl='trash-list && echo "Total of ... $(trash-list | wc -l)"'
alias trr='trash-restore'
alias trashe='trash-empty'
alias trashr="trash-rm"
alias dict='curl dict.org/d:'
alias fzfplay="fzf | sed 's/ /\\\ /g' | xargs mpv"
alias fzfplaya="fzf | sed 's/ /\\\ /g' | xargs mpv --no-video"
alias :q=exit
alias du="du -shP"
alias rc="rsync --progress --verbose --recursive --human-readable"

# arco default aliases
#readable output
alias df='df -h'
#pacman unlock
alias unlock="sudo rm /var/lib/pacman/db.lck"
#free
alias free="free -mt"
#use all cores
alias uac="sh ~/.bin/main/000*"
#continue download
alias wget="wget -c"
#userlist
alias userlist="cut -d: -f1 /etc/passwd"
#merge new settings
alias merge="xrdb -merge ~/.Xresources"
# Aliases for software managment
# pacman or pm
alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'
# yay as aur helper - updates everything
alias pksyua="yay -Syu --noconfirm"
alias upall="yay -Syu --noconfirm"
#ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
#add new fonts
alias update-fc='sudo fc-cache -fv'
#copy/paste all content of /etc/skel over to home folder - backup of config created - beware
# alias skel='cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S) && cp -rf /etc/skel/* ~'
#backup contents of /etc/skel to hidden backup folder in home/user
# alias bupskel='cp -Rf /etc/skel ~/.skel-backup-$(date +%Y.%m.%d-%H.%M.%S)'
#copy bashrc-latest over on bashrc - cb= copy bashrc
#alias cb='sudo cp /etc/skel/.bashrc ~/.bashrc && source ~/.bashrc'
#copy /etc/skel/.zshrc over on ~/.zshrc - cb= copy zshrc
alias cz='sudo cp /etc/skel/.zshrc ~/.zshrc && source ~/.zshrc'
#switch between bash and zsh
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
#quickly kill conkies
alias kc='killall conky'
#hardware info --short
alias hw="hwinfo --short"
#skip integrity check
alias yayskip='yay -S --mflags --skipinteg'
alias trizenskip='trizen -S --skipinteg'
#check vulnerabilities microcode
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'
#get fastest mirrors in your neighborhood
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"
#mounting the folder Public for exchange between host and guest on virtualbox
alias vbm="sudo mount -t vboxsf -o rw,uid=1000,gid=1000 Public /home/$USER/Public"
#shopt
#shopt -s autocd # change to named directory
#shopt -s cdspell # autocorrects cd misspellings
#shopt -s cmdhist # save multi-line commands in history as single line
#shopt -s dotglob
#shopt -s histappend # do not overwrite history
#shopt -s expand_aliases # expand aliases
#youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias ytv-best="youtube-dl -f bestvideo+bestaudio "
#Recent Installed Packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias riplong="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -3000 | nl"
#Cleanup orphaned packages
# alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'
#get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"
#gpg
#verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
#receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
#shutdown or reboot
alias ssn="sudo shutdown now"
alias sr="sudo reboot"

