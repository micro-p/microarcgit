#include "logos/arch4.h"
#define COLOR "\e[1;36m"

#define CONFIG \
{ \
   /* name            function                 cached */\
    { "",             get_title,               false }, \
    { "OS: ",         get_os,                  true  }, \
    { "Kernel: ",     get_kernel,              false  }, \
    { "Uptime: ",     get_uptime,              false }, \
    { "Packages: ",   get_packages_pacman,   false }, \
    { "Shell: ",      get_shell,             true }, \
}

#define CPU_CONFIG \
{ \
   REMOVE("(R)"), \
   REMOVE("(TM)"), \
   REMOVE("Dual-Core"), \
   REMOVE("Quad-Core"), \
   REMOVE("Six-Core"), \
   REMOVE("Eight-Core"), \
   REMOVE("Core"), \
   REMOVE("CPU"), \
}

#define GPU_CONFIG \
{ \
    REMOVE("Corporation"), \
}
