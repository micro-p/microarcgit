#!/bin/sh

setxkbmap -option caps:swapescape &
xset r rate 300 50 &
xsetroot -cursor_name left_ptr &
setxkbmap -option 'grp:sclk_toggle' &
setxkbmap -option 'grp_led:num' &
setxkbmap -layout us,dvorak,ara &
export QT_QPA_PLATFORMTHEME="qt5ct" &

xinput set-prop 12 "libinput Accel Speed" 0.5 &
xinput set-prop 12 "libinput Tapping Enabled" 1 &
xinput set-prop 12 "libinput Scroll Method Enabled" 0, 1, 0 &

xinput set-prop 13 "libinput Accel Speed" 0.5 &
xinput set-prop 13 "libinput Tapping Enabled" 1 &
xinput set-prop 13 "libinput Scroll Method Enabled" 0, 1, 0 &


killall dunst ;
dunst &
killall clipmenud ;
clipmenud &


killall picom ;
picom -b &
feh --bg-fill /home/micro-p/.config/wall.png &


killall udiskie ;
udiskie -t &
killall nm-applet ;
nm-applet &

killall kdeconnectd ;
/usr/lib/kdeconnectd &
killall kdeconnect-indicator ;
kdeconnect-indicator &

killall flameshot ;
flameshot &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# /home/micro-p/.local/bin/automultimonitor &
