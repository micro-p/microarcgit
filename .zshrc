#---------------------------------------------------#
#  __  __ _                      ____               #
# |  \/  (_) ___ _ __ ___       |  _ \              #
# | |\/| | |/ __| '__/ _ \ _____| |_) |             #
# | |  | | | (__| | | (_) |_____|  __/              #
# |_|  |_|_|\___|_|  \___/      |_|                 #
#                                                   #
# feel free to use just gimme credits xD            #
#---------------------------------------------------#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

fastfetch --logo small
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
export FPATH="~/.config/zshell/eza/completions/zsh:$FPATH"

#ZSH_THEME="powerlevel10k/powerlevel10k"


#plugins=(	        #git
#                    # common-aliases
#                    command-not-found
#                    #web-search
#                    # globalias
#                    zsh-autosuggestions
#                    z
#                    #universalarchive
#                    #vi-mode
#                    # zsh-vimode-visual
#                    zsh-completions
#                    you-should-use
#                    per-directory-history
#       	            # autojump
#                    )

#source $ZSH/oh-my-zsh.sh

# compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-"$ZSH_VERSION"
zmodload zsh/complist
autoload -Uz compinit && compinit
zstyle ':completion:*' menu select

# Auto complete with case insenstivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}'

zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS} "

setopt autocd extended_glob interactive_comments nomatch auto_pushd
# unsetopt nomatch
# _comp_options+=(globdots)		# Include hidden files.

autoload -Uz colors && colors

export EDITOR='nvim'
export EDITOR='nvim'
export HYPRSHOT_DIR="HOME/Pictures/screenshots"


source /home/micro-p/.config/zshell/functions
source /home/micro-p/.config/zshell/aliases
source /home/micro-p/.config/zshell/highlighting
source /home/micro-p/.config/zshell/bindkeys

source /home/micro-p/.config/zshell/zoxide

source /home/micro-p/.config/zshell/powerlevel10k/powerlevel10k.zsh-theme
source /home/micro-p/.config/zshell/zsh-you-should-use/you-should-use.plugin.zsh
# source /home/micro-p/.config/zshell/zsh-z/zsh-z.plugin.zsh
source /home/micro-p/.config/zshell/per-directory-history/per-directory-history.zsh
source /home/micro-p/.config/zshell/zsh-autosuggestions/zsh-autosuggestions.zsh
source /home/micro-p/.config/zshell/zsh-completions/zsh-completions.plugin.zsh
source /home/micro-p/.config/zshell/zsh-history-substring-search/zsh-history-substring-search.zsh
source /home/micro-p/.config/zshell/zsh-insulter/zsh.command-not-found
# source /home/micro-p/.config/zshell/fzf-zsh-plugin/fzf-zsh-plugin.plugin.zsh
source /home/micro-p/.config/zshell/fzf-tab/fzf-tab.plugin.zsh
source /home/micro-p/.config/zshell/fzf-tab-source/fzf-tab-source.plugin.zsh

# disable sort when completing `git checkout`
# zstyle ':completion:*:git-checkout:*' sort false
# set descriptions format to enable group support
# NOTE: don't use escape sequences here, fzf-tab will ignore them
# zstyle ':completion:*:descriptions' format '[%d]'
# set list-colors to enable filename colorizing
# zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
# force zsh not to show completion menu, which allows fzf-tab to capture the unambiguous prefix
# zstyle ':completion:*' menu no
# preview directory's content with eza when completing cd
# zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza -1 --color=always $realpath'
# zstyle ':fzf-tab:complete:z:*' fzf-preview 'eza -1 --color=always $realpath'
# zstyle ':fzf-tab:complete:z:*' fzf-preview 'eza -1 --color=always $realpath'
# switch group using `<` and `>`
# zstyle ':fzf-tab:*' switch-group '<' '>'



# General preview configuration
# zstyle ':fzf-tab:complete:*' fzf-preview 'eza -1 --color=always $realpath'
# Specific preview for file completion
# zstyle ':fzf-tab:complete:file:*' fzf-preview 'cat $realpath'
zstyle ':completion:*' list-colors '=(#b)(--[^ ]#)(*)=38;5;220;1=38;5;216'
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza -1 --color=always $realpath'
zstyle ':fzf-tab:complete:eza:*' fzf-preview 'eza -1 --color=always $realpath'
zstyle ':fzf-tab:complete:z:*' fzf-preview 'eza -1 --color=always $realpath'
zstyle ':fzf-tab:complete:git-(add|diff|restore):*' fzf-preview 'git diff $word | delta'
zstyle ':fzf-tab:complete:git-log:*' fzf-preview 'git log --color=always $word'
zstyle ':fzf-tab:complete:git-help:*' fzf-preview 'git help $word | bat -plman --color=always'
zstyle ':fzf-tab:complete:git-show:*' fzf-preview 'case "$group" in "commit tag") git show --color=always $word ;; *) git show --color=always $word | delta ;; esac'
zstyle ':fzf-tab:complete:git-checkout:*' fzf-preview 'case "$group" in "modified file") git diff $word | delta ;; "recent commit object name") git show --color=always $word | delta ;; *) git log --color=always $word ;; esac'
zstyle ':completion:*:git-checkout:*' sort false
zstyle ':completion:*:descriptions' format '[%d]'
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza -1 --color=always $realpath'
zstyle ':fzf-tab:*' switch-group '<' '>'

# [[ -r "/usr/share/z/z.sh" ]] && source /usr/share/z/z.sh


LF_ICONS=$(sed ~/.config/lf/icons \
            -e '/^[ \t]*#/d'       \
            -e '/^[ \t]*$/d'       \
            -e 's/[ \t]\+/=/g'     \
            -e 's/$/ /')
LF_ICONS=${LF_ICONS//$'\n'/:}
export LF_ICONS



HISTSIZE=999999999999999999
SAVEHIST=$HISTSIZE
HISTFILE=~/.zsh_history

# colorscript random
# && ~/.local/bin/COVID19

eval $(dircolors -b $HOME/.dircolors)

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
typeset -g POWERLEVEL9K_INSTANT_PROMPT=off

## autostart startx
# if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
#   exec startx
# fi
# eval "$(starship init zsh)"
# typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet

eval "$(atuin init zsh --disable-up-arrow)"
